
export type loginStateType = {
    username: string,
    token: string,
    roles: string[]
}


export const loginState: loginStateType = {
    username: '',
    token: '',
    roles:[],
}
