/* eslint-disable @typescript-eslint/no-unused-expressions */

import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import { loginState, loginStateType } from './login.state';


export const authSlice = createSlice({
    name: 'auth',
    initialState: loginState,
    reducers:{
        logout(state){
           state = {
            token: '',
            roles: [''],
            username: ''
           }

           return state;
        },
        setCredentials(state, action: PayloadAction<loginStateType>) {
            state = {
                token: action.payload.token,
                roles: [...action.payload.roles],
                username: action.payload.username
            };

            return state;
        }


    }
})

export const {logout, setCredentials} = authSlice.actions





