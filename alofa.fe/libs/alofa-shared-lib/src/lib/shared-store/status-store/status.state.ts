
export interface statusStateType {
    message: string;
    isError: boolean;
}

export const statusState: statusStateType = {
    message: '',
    isError: false
}