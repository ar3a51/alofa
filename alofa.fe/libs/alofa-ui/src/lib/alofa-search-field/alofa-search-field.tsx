import { Box, Input } from '@chakra-ui/react';
import styles from './alofa-search-field.module.scss';
import { SearchIcon } from '@chakra-ui/icons';
import { type } from 'os';

/* eslint-disable-next-line */
export interface AlofaSearchFieldProps {
  id: string;
  name: string;
}

export function AlofaSearchField(props: AlofaSearchFieldProps) {
  return (
   <Box as="div" className={styles['search-wrapper']}>
      <input className={styles['search-field']} type={'search'} {...props}/>
      <SearchIcon className={styles['search-icon']} fontSize={'1.6em'}/>
  </Box>
  );
}

export default AlofaSearchField;
