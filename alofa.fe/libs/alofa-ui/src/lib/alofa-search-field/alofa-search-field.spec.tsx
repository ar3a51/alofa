import { render } from '@testing-library/react';

import AlofaSearchField from './alofa-search-field';

describe('AlofaSearchField', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AlofaSearchField />);
    expect(baseElement).toBeTruthy();
  });
});
