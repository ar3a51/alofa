import styles from './alofa-header.module.scss';

/* eslint-disable-next-line */
export interface AlofaHeaderProps {}

export function AlofaHeader(props: AlofaHeaderProps) {
  return (
    <div className={styles['container']}>
      <h1>Welcome to AlofaHeader!</h1>
    </div>
  );
}

export default AlofaHeader;
