import { render } from '@testing-library/react';

import AlofaHeader from './alofa-header';

describe('AlofaHeader', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AlofaHeader />);
    expect(baseElement).toBeTruthy();
  });
});
