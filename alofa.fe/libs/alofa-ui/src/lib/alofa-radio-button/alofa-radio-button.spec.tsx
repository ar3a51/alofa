import { render } from '@testing-library/react';

import AlofaRadioButton from './alofa-radio-button';

describe('AlofaRadioButton', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AlofaRadioButton />);
    expect(baseElement).toBeTruthy();
  });
});
