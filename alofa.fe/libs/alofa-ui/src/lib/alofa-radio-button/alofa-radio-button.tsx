import { Box } from '@chakra-ui/react';
import styles from './alofa-radio-button.module.scss';

/* eslint-disable-next-line */
export interface AlofaRadioButtonProps {
  id: string,
  name: string,
  label: string,
  value?: any,
  checked?: boolean,
  onChange?: (e: any) => void
}

export function AlofaRadioButton(props: AlofaRadioButtonProps) {
  return (
    <Box as='div' 
      className={styles['radio-button']} 
      display={'flex'} alignItems={'center'}
      justifyContent={'center'}
      >
      <input type={'radio'} {...props} className={styles['radio-button__input']}/>
      <label
        className={styles['radio-button__field-label']} 
        htmlFor={props.id}>
        {props.label}
      </label>
    </Box>
  );
}

export default AlofaRadioButton;
