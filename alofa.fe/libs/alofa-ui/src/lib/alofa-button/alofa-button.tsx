import { MouseEventHandler, ReactNode } from 'react';
import styles from './alofa-button.module.scss';

/* eslint-disable-next-line */
export interface AlofaButtonProps {
  children: ReactNode;
  disabled?: boolean;
  isPrimary?: boolean;
  onClick?: MouseEventHandler<HTMLButtonElement>;
}

export function AlofaButton(props: AlofaButtonProps) {
  return (
    props.isPrimary || props.isPrimary === undefined || props.isPrimary === null ? 
    <button onClick={props.onClick}
      className={props.disabled ? styles['btn--primary--disabled'] : styles['btn--primary']} 
      disabled={props.disabled}>{props.children}
    </button> :
     <button onClick={props.onClick}
     className={props.disabled ? styles['btn--secondary--disabled'] : styles['btn--secondary']} 
     disabled={props.disabled}>{props.children}
   </button>
  );
}

export default AlofaButton;
