import { Box } from '@chakra-ui/react';
import { ChangeEventHandler } from 'react';
import styles from './alofa-dropdown.module.scss';

/* eslint-disable-next-line */
export interface AlofaDropdownProps {
  id: string,
  name: string,
  listInfo: {
    data: Array<any>,
    idField: string,
    labelField: string,
    valueField: string
  },
  onChange?: ChangeEventHandler<HTMLSelectElement>,
  value?: any
  

}

export function AlofaDropdown(props: AlofaDropdownProps) {
  return (
    <Box as='div' className={styles['dropdown-wrapper']}>
      <select className={styles['dropdown']} 
        id={props.id}
        name={props.name}
        onChange={props.onChange}
        value={props.value}
        >{
          props.listInfo && props.listInfo.data.map(val => <option
            key={val[props.listInfo.idField]} 
            value={val[props.listInfo.valueField]}>
              { val[props.listInfo.labelField] }
            </option>)
        }</select>
    </Box>
  );
}

export default AlofaDropdown;
