import { ChangeEvent, ChangeEventHandler, useEffect, useState } from 'react';
import styles from './alofa-pill-button.module.scss';

/* eslint-disable-next-line */
export interface AlofaPillButtonProps {
  id: string,
  name: string,
  /* eslint-disable-next-line */
  value?: any,
  label?: string,
  onChange?: ChangeEventHandler<HTMLInputElement>,
  checked?: boolean
}

export function AlofaPillButton(props: AlofaPillButtonProps) {


  return (
    <div className={styles['pill-button']}>
      <input type="checkbox"
        className={styles['pill-button__input']} 
        id={props.id} 
        name={props.name} 
        value={props.value}
        onChange={props.onChange}
      />
       <label 
        className={styles['pill-button__label']} 
        htmlFor={props.id}>{props.label}</label>
    </div>
  );
}

export default AlofaPillButton;
