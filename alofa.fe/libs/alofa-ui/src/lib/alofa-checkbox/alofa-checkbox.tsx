import React, { ChangeEventHandler} from 'react';
import style from './alofa-checkbox.module.scss'
/* eslint-disable-next-line */
export interface AlofaCheckboxProps {
  id: string,
  name: string,
  checked?: boolean,
  onChange?: ChangeEventHandler<HTMLInputElement>,
  /* eslint-disable-next-line */
  value?: any,
  label?: string,
  disabled?: boolean
}

export function AlofaCheckbox(props: AlofaCheckboxProps) {
  
   return (
    <div className={style['checkbox']}>
      <input disabled={props.disabled} type={'checkbox'}
      className={style['checkbox__input']}
      id={props.id}
      name={props.name}
      checked={props.checked} 
      onChange={props.onChange}
      value={props.value}/>
      <label
      className={style['checkbox__field-label']} 
      htmlFor={props.id}>
        {props.label}
      </label>
   </div>
  );
}


export default AlofaCheckbox;
