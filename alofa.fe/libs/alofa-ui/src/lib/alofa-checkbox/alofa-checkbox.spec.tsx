import { render } from '@testing-library/react';

import AlofaCheckbox from './alofa-checkbox';

describe('AlofaCheckbox', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AlofaCheckbox />);
    expect(baseElement).toBeTruthy();
  });
});
