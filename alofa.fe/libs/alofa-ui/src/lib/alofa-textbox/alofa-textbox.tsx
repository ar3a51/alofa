import { ChangeEventHandler } from 'react';
import styles from './alofa-textbox.module.scss';

/* eslint-disable-next-line */
export interface AlofaTextboxProps {
  name: string,
  id: string,
  type: string,
  value?: string,
  onChange?: ChangeEventHandler<HTMLInputElement>
}

export function AlofaTextbox(props: AlofaTextboxProps) {
  return (
    <input className={styles['txt']}
    {...props}
   ></input>
  );
}

export default AlofaTextbox;
