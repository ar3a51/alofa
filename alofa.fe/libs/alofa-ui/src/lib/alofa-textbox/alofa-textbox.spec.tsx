import { render } from '@testing-library/react';

import AlofaTextbox from './alofa-textbox';

describe('AlofaTextbox', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AlofaTextbox />);
    expect(baseElement).toBeTruthy();
  });
});
