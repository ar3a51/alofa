import { extendTheme } from "@chakra-ui/react"

const alofaTheme = extendTheme({
    fonts:{
      body: 'Open Sans',
      heading: 'Open Sans'
    }
});

export default alofaTheme;