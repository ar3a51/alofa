import { render } from '@testing-library/react';

import AlofaMobileLogin from './alofa-mobile-login';

describe('AlofaMobileLogin', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AlofaMobileLogin />);
    expect(baseElement).toBeTruthy();
  });
});
