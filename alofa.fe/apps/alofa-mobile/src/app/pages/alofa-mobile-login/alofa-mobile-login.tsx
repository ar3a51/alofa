import styles from './alofa-mobile-login.module.scss';
import { AlofaButton, AlofaTextbox } from '@alofa.fe/alofa-ui';
import { useAppDispatch } from '../../store.typed';
import { useState } from 'react';
import { clearStatus, loginModel, loginStateType, logout, setCredentials, statusStateType, updateStatus } from '@alofa.fe/alofa-shared-lib'
import { useSelector } from 'react-redux';
import { statusSelector } from '../../shared-selectors/status.selector';
import { useLoginMutation } from './loginApiSlice';
import { useNavigate } from 'react-router-dom';


/* eslint-disable-next-line */
export interface AlofaMobileLoginProps {}

export function AlofaMobileLogin(props: AlofaMobileLoginProps) {
  const dispatch = useAppDispatch();
  const [username, setUsername] = useState('');
  const [password, setPassword] = useState('');
   const status = useSelector(statusSelector);
  const [login, {isLoading, isError}] = useLoginMutation();
  const navigate = useNavigate();
   
  function handleInputChangeUser(e: any) {
    setUsername(e.target.value)
    dispatch(clearStatus());
  }

  function handleInputChangePassword(e: any) {
    setPassword(e.target.value)
    dispatch(clearStatus());
  }

  async function handleSubmit (e:any) {
    e.preventDefault();

    const loginCred: loginModel = {username, password}

    try {
      const result = await login(loginCred).unwrap() as loginStateType;
      dispatch(setCredentials({...result}));
      setUsername('');
      setPassword('');
      navigate('/')
    } catch (err: any) {
      console.log(err);
      if (!err?.data) {
        dispatch(updateStatus({message: 'No Server Response', isError}));
      } else if (err.status === 400) {
        dispatch(updateStatus({message: 'Missing username or password', isError}));
      } else if (err.status === 401) {
        dispatch(updateStatus({message: 'Unauthorised', isError}))
      } else {
        dispatch(updateStatus({message: err.data.ErrMessage, isError}))
      }
    }
  }

  function handleCancel(e:any): void{
    e.preventDefault();

    setUsername('');
    setPassword('');
    dispatch(logout());
  }


  
  return (
    <div className={styles['container']}>
      <span>{status.message}</span>
      <form className={styles['login_form']} onSubmit={handleSubmit}>
        <fieldset className={styles['fieldset']}>
            <label htmlFor='txt_username'>Username</label>
            <AlofaTextbox type='text' id='txt_username' name='username'
              onChange={handleInputChangeUser}
              value={username}
            ></AlofaTextbox>
        </fieldset>
        <fieldset className={styles['fieldset']}>
            <label htmlFor='txt_password'>Password</label>
            <AlofaTextbox type='password' id='txt_password' name='password'
              onChange={handleInputChangePassword}
              value={password}
            ></AlofaTextbox>
        </fieldset>
        <span className={styles['btn-submit']}>
          <AlofaButton>submit</AlofaButton>
        </span>
        <span className={styles['btn-cancel']}>
          <AlofaButton onClick={handleCancel} isPrimary={false}>Clear</AlofaButton>
        </span>
      </form>
    </div>
  );
}

export default AlofaMobileLogin;
