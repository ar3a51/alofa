import { AlofaButton } from '@alofa.fe/alofa-ui'
import styles from './alofa-mobile-welcome.module.scss'
import { useNavigate } from 'react-router-dom'



function AlofaMobileWelcome() {

    const navigate = useNavigate();

    function goSignIn(){
        navigate('/login',{replace: true});
    }

    function goRegister(){
        navigate('/register');
    }
    return(
        <main className={styles['welcome-container']}>
            <section className={styles['board']}>
                <div>
                    Logo here
                </div>
                <div className={styles['btn-group']}>
                    <span className={styles['btn-account']}>
                        <AlofaButton onClick={goRegister}>Create account</AlofaButton></span>
                    <span className={styles['btn-signin']}>
                        <AlofaButton isPrimary={false} onClick={goSignIn}>Sign In</AlofaButton>
                    </span>
                </div>
                <div className={styles['disclaimer']}>

                </div>
            </section>
        </main>
    )
}

export default AlofaMobileWelcome;