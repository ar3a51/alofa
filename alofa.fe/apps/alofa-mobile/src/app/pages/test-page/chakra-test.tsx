import { AlofaButton, AlofaPillButton, AlofaRadioButton, AlofaSearchField } from '@alofa.fe/alofa-ui'
import {
    Menu,
    MenuButton,
    MenuList,
    MenuItem,
    MenuItemOption,
    MenuGroup,
    MenuOptionGroup,
    MenuDivider,
    Button
  } from '@chakra-ui/react'

  import { ChevronDownIcon } from '@chakra-ui/icons';
export const ChakraTest = (()=>{


    return(
        <>
        <Menu>
            <MenuButton as={Button} rightIcon={<ChevronDownIcon />}>
                Actions
            </MenuButton>
            <MenuList>
                <MenuItem onClick={()=>alert('downloaded')}>Download</MenuItem>
                <MenuItem>Create a Copy</MenuItem>
                <MenuItem>Mark as Draft</MenuItem>
                <MenuItem>Delete</MenuItem>
                <MenuItem>Attend a Workshop</MenuItem>
            </MenuList>
        </Menu>

        <AlofaSearchField id="testsearch" name="testsearch"/>

        <AlofaPillButton id="pillbtntest" name="pillbtntest" label='test'/>

        <AlofaRadioButton id="radioTest" name="radioTest" label='radiotest'/>
        <AlofaRadioButton id="radioTest2" name="radioTest" label='radiotest'/>
        </>
    )
})
