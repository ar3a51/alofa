import React from 'react';
import { Route, Routes } from 'react-router-dom';
import LoadingFallback from '../../Loading-fallback';
import AlofaMobileLayout from './alofa-mobile-main.layout';

const ProfilePage = React.lazy(()=>import('./Profile/alofa-mobile-profile'));

export default function AlofaMobileMainRouting() {
    return (
        <Routes>
            <Route path="/" element={<AlofaMobileLayout/>}>
                <Route index element={
                    <React.Suspense fallback={<LoadingFallback/>}>
                        <ProfilePage/>
                    </React.Suspense>
                }/>
            </Route>
        </Routes>
    )
}