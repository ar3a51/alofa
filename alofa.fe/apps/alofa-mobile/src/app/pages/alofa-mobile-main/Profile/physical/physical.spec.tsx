import { render } from '@testing-library/react';

import Physical from './physical';

describe('Physical', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Physical />);
    expect(baseElement).toBeTruthy();
  });
});
