import { AlofaCheckbox, AlofaDropdown, AlofaTextbox } from "@alofa.fe/alofa-ui";
import { Box, Heading, HStack, Stack } from "@chakra-ui/react";
import { useField } from "@formiz/core";
import { useState } from "react";



function DayDropdown(props: any){
    const {value, isValid, setValue} = useField(props);

    return (
    <Box as={'fieldset'} width={'20%'}>
        <Box as={'label'} htmlFor={props.name}>Day</Box>
        <AlofaDropdown {...props} id={props.name} name={props.name}></AlofaDropdown>
    </Box>
    )
}

function MonthDropdown(props: any){
    const {value, isValid, setValue} = useField(props);

    return (
    <Box as={'fieldset'} width={'60%'}>
        <Box as={'label'} htmlFor={props.name}>Month</Box>
        <AlofaDropdown {...props} id={props.name} name={props.name}></AlofaDropdown>
    </Box>
    )
}

function YearDropdown(props: any){
    const {value, isValid, setValue} = useField(props);

    return (
    <Box as={'fieldset'} width={'20%'}>
        <Box as={'label'} htmlFor={props.name}>Year</Box>
        <AlofaDropdown {...props} id={props.name} name={props.name}></AlofaDropdown>
    </Box>
    )
}

function PlaceOfBirthField(props: any){
    const {value, setValue, isValid} = useField(props);
    return(
        <Box as={'fieldset'} marginTop={'1em'}>
            <Box as={'label'} htmlFor={props.name}>Place of Birth</Box>
            <AlofaTextbox type="text" id={props.name} name={props.name}
                value={value || ''}
                onChange={(e)=> setValue(e.target.value)}
            />
        </Box>
    )
}

function ShowDobDetailsInProfile(props: any) {
    const {value, setValue} = useField(props);
    return(
        <Box as={'fieldset'}>
            <AlofaCheckbox id={props.name} checked={value || false} name={props.name} label="Show in Profile"
                onChange={(e)=>setValue(e.target.checked)} 
            />
        </Box>
    )
}


export default function DateOfBirthForm() {

    return(
        <Box marginTop={'5em'} height={'70vh'}>
            <Heading>What is your date of birth?</Heading>
            <HStack justifyContent={'stretch'}>
                <DayDropdown name="dob.day" />
                <MonthDropdown name='dob.month' />
                <YearDropdown name="dob.year" />
            </HStack>
            <PlaceOfBirthField name='dob.placeOfBirth' />
            <Stack height={'33%'} justify='flex-end' alignItems={'center'}>
              <ShowDobDetailsInProfile name="dob.showDobDetails"/>
            </Stack>
        </Box>
    )
}

