import { Box } from "@chakra-ui/react";
import { useField } from "@formiz/core";
import { AlofaCheckbox } from '@alofa.fe/alofa-ui';

function ShowDobDetailsInProfile(props: any) {
    const {value, setValue} = useField(props);
    return(
        <Box as={'fieldset'}>
            <AlofaCheckbox id={props.name} checked={value || false} name={props.name} label="Show in Profile"
                onChange={(e)=>setValue(e.target.checked)} 
            />
        </Box>
    )
}

export default ShowDobDetailsInProfile;