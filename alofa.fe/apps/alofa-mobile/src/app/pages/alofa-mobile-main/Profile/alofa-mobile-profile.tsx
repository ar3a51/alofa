import { AlofaButton } from "@alofa.fe/alofa-ui";
import { Box, Flex, Stack } from "@chakra-ui/react";
import { Formiz, useForm, FormizStep } from "@formiz/core";
import AboutYouForm from "./About-You/alofa-mobile-aboutyou";
import DateOfBirthForm from "./Date-of-birth/alofa-mobile-dob";
import Ethnicity from "./ethnicity/ethnicity";
import GenderSexualOrientation from "./gender-sexual-orientation/gender-sexual-orientation";
import Physical from "./physical/physical";


export default function AlofaMobileProfile(){
    const profileForm = useForm();

    const init = {}

    function handleFormSubmit(e: any){
        console.log(e);
    }

    return (
        <Flex flexDirection={'column'} p="5" width={'100%'} height={'100vh'} position='relative'>
            <Formiz connect={profileForm} onSubmit={handleFormSubmit}
            initialValues={init}
            >
                <form noValidate onSubmit={profileForm.submitStep}>
                    <FormizStep name="abtYouStep">
                        <AboutYouForm/>
                    </FormizStep>
                    <FormizStep name="dobStep">
                        <DateOfBirthForm/>
                    </FormizStep>
                    <FormizStep name="genderStep">
                        <GenderSexualOrientation/>
                    </FormizStep>
                    <FormizStep name="ethnicityStep">
                        <Ethnicity/>
                    </FormizStep>
                    <FormizStep name="physicalStep">
                        <Physical/>
                    </FormizStep>
                    <Stack position={'fixed'} 
                    flexDirection={'column-reverse'} 
                    bottom='10' 
                    width={'90%'}
                    alignItems={'center'}
                    justify="center"
                    >
                        <Box p={'2'}><AlofaButton>Save & Continue</AlofaButton></Box>
                        <Box p={'2'}><AlofaButton isPrimary={false}>Skip</AlofaButton></Box>
                    </Stack>
                </form>
            </Formiz>
        </Flex>
    )
}