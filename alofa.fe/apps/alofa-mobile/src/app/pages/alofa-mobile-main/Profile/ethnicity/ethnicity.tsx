import { AlofaDropdown, AlofaTextbox } from '@alofa.fe/alofa-ui';
import { Box, Grid, GridItem, Heading } from '@chakra-ui/react';
import { useField } from '@formiz/core';
import ShowDobDetailsInProfile from '../show-details-in-profile';
import styles from './ethnicity.module.scss';

function EthnicityDropdown(props: any) {
  const {value, setValue} = useField(props);

  return (
    <Box as='fieldset'>
      <label htmlFor=''>What's your ethnicity</label>
      <AlofaDropdown {...props} id='ethnicity-selection' name='ethnicity-selection'></AlofaDropdown>
    </Box>
  )

}

function Other(props: any) {
  const {value, setValue} = useField(props);

  return (
    <Box as='fieldset'>
      <label htmlFor=''>Other</label>
      <AlofaTextbox {...props} 
        id='ethnicity-selection' 
        name='ethnicity-selection'
        onChange={(e)=> setValue(e.target.value)}
        value={value ?? ''}
        ></AlofaTextbox>
    </Box>
  )

}
/* eslint-disable-next-line */
export interface EthnicityProps {}

const paddingSize = '1.5em';

export function Ethnicity(props: EthnicityProps) {
  return (
    <Grid templateColumns={'1fr'} marginTop={'2em'}>
        <GridItem w={'100%'} paddingTop={paddingSize} paddingBottom={paddingSize}>
            <Heading>Ethnicity</Heading>
        </GridItem>
        <GridItem w={'100%'} paddingTop={paddingSize} paddingBottom={paddingSize}>
            <EthnicityDropdown name='ethnicity.selection'/>
        </GridItem>
        <GridItem w={'100%'} paddingTop={paddingSize} paddingBottom={paddingSize}>
            <Other name='ethnicity.other'/>
        </GridItem>
        <GridItem w={'100%'} position={'relative'} h={'15dvh'}>
          <Box position={'absolute'} bottom='0'>
            <ShowDobDetailsInProfile name='ethnicity.showDetailsInProfile'/>
          </Box>
        </GridItem>
    </Grid>
  );
}

export default Ethnicity;
