import { render } from '@testing-library/react';

import Ethnicity from './ethnicity';

describe('Ethnicity', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Ethnicity />);
    expect(baseElement).toBeTruthy();
  });
});
