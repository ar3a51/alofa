import { AlofaCheckbox, AlofaPillButton } from "@alofa.fe/alofa-ui";
import { Box, Divider, Flex, Heading, Spacer, Stack, Textarea, Checkbox, Grid, GridItem } from "@chakra-ui/react";
import { useField } from "@formiz/core";
import { useEffect, useState } from "react";



function AboutYouField(props: any){
    const {value, isValid, errorMessage, setValue} = useField(props);

    return(
        <Box as={'fieldset'} marginTop={'3em'}>
            <p>Share a little about yourself</p>
            <Textarea {...props}
                onChange={(e: any)=>setValue(e.target.value)}
                rows="8"
                value={value || ''}
            ></Textarea>
        </Box>
    )
}

function ShowAboutYouInProfile(props: any){
    const {value, isValid, errorMessage, setValue} = useField(props);

    return(
        <Box as={'fieldset'}>
            <AlofaCheckbox 
                    id={props.name} name={props.name} checked={value || false} label="Show in Profile" onChange={(e: any)=>{
                        setValue(e.target.checked);
                }}/>
        </Box>
    )
}

export default function AboutYouForm() {
    //const {value, setValue} = useField(props);
        
    return (

        <Grid templateColumns={'1fr'} width={'100%'} marginTop={'2em'}>
            <GridItem width={'100%'}>
                <Heading>About you</Heading>
            </GridItem>
            <GridItem width={'100%'}>
                <AboutYouField name="abtYou.abtYouField"/>
            </GridItem>
            <GridItem width={'100%'} position={'relative'} height={'19dvh'}>
                <Box position={'absolute'} bottom={'0'}>
                    <ShowAboutYouInProfile name="abtYou.showAbtYouInProfile" />
                </Box>
            </GridItem>
        </Grid>

        /*
        <Stack height={'70vh'} marginTop={'5em'}>
            <Box>
                <Heading>About you</Heading>
                <AboutYouField name="abtYou.abtYouField"/>
            </Box>
            <Stack height={'30%'} justify='flex-end' alignItems={'center'}>
                <ShowAboutYouInProfile name="abtYou.showAbtYouInProfile" />
            </Stack>
        </Stack>
        */
    )
} 
