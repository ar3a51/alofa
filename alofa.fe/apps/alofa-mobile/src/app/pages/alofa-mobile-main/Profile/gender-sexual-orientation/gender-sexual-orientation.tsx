import { Box, Heading } from '@chakra-ui/react';
import { AlofaDropdown, AlofaRadioButton, AlofaSearchField, AlofaCheckbox } from '@alofa.fe/alofa-ui';
import styles from './gender-sexual-orientation.module.scss';
import { useField } from '@formiz/core';


function GenderSelection(props: any) {
  const {value, setValue} = useField(props);

  return (
    <>
     <Box className={styles['radio']}>
        <AlofaRadioButton 
          id='gender-selection-male' 
          name='gender-selection'
          label='Male'
          value={'male'}
          checked={value === 'male'}
          onChange={(e)=> e.target.checked ? setValue(e.target.value) : setValue('')}
        />
      </Box>
      <Box className={styles['radio']}>
        <AlofaRadioButton 
          id='gender-selection-female' 
          name='gender-selection'
          label='Female'
          value={'female'}
          checked={value === 'female'}
          onChange={(e)=> e.target.checked ? setValue(e.target.value) : setValue('')}
        />
      </Box>
    </>
  )
}

function GenderIdentity(props: any) {
  const {value, setValue} = useField(props);

  return(
    <Box as='fieldset'>
      <label htmlFor='gender-identity'>What gender do you identity as?</label>
      <AlofaSearchField 
        id='gender-identity' 
        name='gender-identity'/>
    </Box>
  )
}

function SexualOrientation(props: any) {
  const {value, setValue} = useField(props);

  return(
    <Box as='fieldset'>
      <label htmlFor='sexual-orientation'>What gender do you identity as?</label>
      <AlofaDropdown
        {...props}
        id='sexual-orientation' 
        name='sexual-orientation'/>
    </Box>
  )
}

function ShowDobDetailsInProfile(props: any) {
  const {value, setValue} = useField(props);
  return(
      <Box as={'fieldset'}>
          <AlofaCheckbox id={props.name} checked={value || false} name={props.name} label="Show in Profile"
              onChange={(e)=>setValue(e.target.checked)} 
          />
      </Box>
  )
}

/* eslint-disable-next-line */
export interface GenderSexualOrientationProps {}

export function GenderSexualOrientation(props: GenderSexualOrientationProps) {
  return (
   <Box as={'div'} className={styles['container']}>
      <Box as={'div'} className={styles['field']}>
        <Heading>Gender/Sexual Orientation</Heading>
      </Box>
      <Box as={'div'} className={styles['field']}>
        <GenderSelection name='gender.physical' />
      </Box>
      <Box as={'div'} className={styles['field']}>
        <GenderIdentity name='gender.identity' />
      </Box>
      <Box as={'div'} className={styles['field']}>
        <SexualOrientation name='gender.sexualOrientation'/>
      </Box>
      <Box className={styles['field']}>
        <ShowDobDetailsInProfile name='gender.showInProfile' />
      </Box>
   </Box>
  );
}

export default GenderSexualOrientation;
