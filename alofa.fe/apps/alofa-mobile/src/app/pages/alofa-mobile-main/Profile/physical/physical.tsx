import { AlofaDropdown, AlofaTextbox } from '@alofa.fe/alofa-ui';
import { Box, Grid, GridItem, Heading } from '@chakra-ui/react';
import { useField } from '@formiz/core';
import styles from './physical.module.scss';

function PhysicalType(props: any){
  const {value, setValue} = useField(props);

  return(
    <Box as='fieldset'>
      <label htmlFor='body-type'>What's your Physical body type</label>
      <AlofaDropdown {...props} id='body-type' name='body-type'></AlofaDropdown>
    </Box>
  )
}

function Height(props: any){
  const {value, setValue} = useField(props);

  return(
    <Box as='fieldset'>
      <label htmlFor='height'>Enter Height</label>
      <AlofaTextbox {...props} id='height' name='height'
        onChange={(e)=> setValue(e.target.value)}
        value={value ?? ''}
      ></AlofaTextbox>
    </Box>
  )
}

function Weight(props: any){
  const {value, setValue} = useField(props);

  return(
    <Box as='fieldset'>
      <label htmlFor='height'>Enter Weight</label>
      <AlofaTextbox {...props} id='weight' name='weight'
        onChange={(e)=> setValue(e.target.value)}
        value={value ?? ''}
      ></AlofaTextbox>
    </Box>
  )
}
/* eslint-disable-next-line */
export interface PhysicalProps {}

const paddingSize = '1em'
export function Physical(props: PhysicalProps) {
  return (
   <Grid templateColumns={'1fr'} w={'100%'} marginTop={'2em'}>
      <GridItem w={'100%'}>
        <Heading>Physical Appearance</Heading>
      </GridItem>
      <GridItem w={'100%'} paddingTop={paddingSize} paddingBottom={paddingSize}>
        <PhysicalType name='physical.type'/>
      </GridItem>
      <GridItem w={'100%'} paddingTop={paddingSize} paddingBottom={paddingSize}>
        <Height name='physical.height' />
      </GridItem>
      <GridItem w={'100%'} paddingTop={paddingSize} paddingBottom={paddingSize}>
        <Weight name='physical.weight' />
      </GridItem>
   </Grid>
  );
}

export default Physical;
