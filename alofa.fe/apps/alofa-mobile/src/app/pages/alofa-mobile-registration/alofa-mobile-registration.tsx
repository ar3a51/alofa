import { AlofaButton, AlofaTextbox } from '@alofa.fe/alofa-ui';
import { Heading } from '@chakra-ui/react';
import { useForm, Formiz, FormizStep, useField } from '@formiz/core';
import { isEmail, isRequired } from '@formiz/validations';
import { useCallback } from 'react';
import styles from './alofa-mobile-registration.module.scss'



function TextField(props: any){
    const {value, isValid, errorMessage, setValue} = useField(props);

    return(
        <>
            <AlofaTextbox 
                id={props.id} 
                name={props.name}
                value={value ?? ''}
                onChange={(e)=>setValue(e.target.value)}
                type={props.type}
            ></AlofaTextbox>
            {
                !isValid
                &&<span>{errorMessage}</span> // Display error message
            }
        </>
    )

}

interface regModel {
    username: string;
    email: string;
    password: string;
}

function AlofaMobileRegistration(){
    const regForm = useForm();
   
    
    const handleValidSubmit = (values: regModel) => {
        console.log(values);
    } 

    const goPrevStep = (e: any)=>{
        e.preventDefault();
        regForm.prevStep();
    }

    const handleSubmitStep=(e:any)=>{
        e.preventDefault();
        regForm.submitStep();
    }

    return(
        <div className={styles['container-form']}>
            <Formiz connect={regForm} onValidSubmit={handleValidSubmit}>
                <form noValidate className={styles['reg-form']} onSubmit={handleSubmitStep}>
                    <FormizStep name='usernameForm'>
                        <Heading as='h1' className={styles['title']}>What is your username?</Heading>   
                        <TextField id='username' name='username' type='text'/>
                    </FormizStep>
                    <FormizStep name='emailForm'>
                        <Heading as='h1' className={styles['title']}>What is your email address?</Heading> 
                            <TextField id='email' name='email' type='email' validations={[
                                {
                                    rule: isRequired(),
                                    message: 'email is required'
                                },
                                {
                                    rule: isEmail(),
                                    message: 'invalid e-mail'
                                }
                            ]}/>                   
                    </FormizStep>
                    <FormizStep name='passwordForm'>
                        <Heading as='h1' className={styles['title']}>What is your Password?</Heading> 
                        <TextField id='password' name='password' type='password' validations={[
                            {
                                rule: isRequired(),
                                message: 'Password is required'
                            }
                        ]}
                         
                        />
                    </FormizStep>
                    <FormizStep name='retypePasswordForm'>
                        <Heading as='h1'>Type it again just in case...</Heading>
                        <TextField id='retype-password' name='retype-password' type='password'></TextField>
                    </FormizStep>
                    <div className={styles['btn-group']}>
                    
                    
                        {
                            regForm.isLastStep ? 
                                <AlofaButton>Submit</AlofaButton> :
                                <AlofaButton>Continue</AlofaButton>
                                
                        }
                    
                       
                        {!regForm.isFirstStep && 
                            <AlofaButton isPrimary={false} 
                                onClick={goPrevStep}>Prev</AlofaButton>
                            }
                      
                    </div>
                 </form>
           </Formiz>
        </div>
    )
}

export default AlofaMobileRegistration;