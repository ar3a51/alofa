import { RootState } from "../store.typed";


export const userLoginSelector = (state: RootState) => state.auth;