import { RootState } from "../store.typed";

export const statusSelector = (state: RootState) => state.status;