import { configureStore } from "@reduxjs/toolkit";
import { authSlice } from '@alofa.fe/alofa-shared-lib';
import { statusSlice } from '@alofa.fe/alofa-shared-lib'
import apiSlice from "./query";


export const store = configureStore({
    reducer: {
        auth: authSlice.reducer,
        status: statusSlice.reducer,
        [apiSlice.reducerPath]: apiSlice.reducer
    },
    middleware: (getDefaultMiddleWare)=> getDefaultMiddleWare().concat(apiSlice.middleware),
    devTools: true
})