import { useSelector } from "react-redux"
import { Outlet, Navigate, useLocation } from "react-router-dom"
import { userLoginSelector } from './shared-selectors/auth.selector'

type Props = {
    roles?: [string];
}

export const Protected = ({roles}:Props) =>{
    const loggedinUser = useSelector(userLoginSelector);
    const location = useLocation();
    console.log(location);

    return (
        loggedinUser.username !=='' && (roles?.find(role => loggedinUser.roles.includes(role))) ? <Outlet/> : <Navigate to={'login'} state={{from: location}} replace/>
    )
}