import { store } from './store';
import { useDispatch } from 'react-redux';

export type RootState = ReturnType<typeof store.getState>
export type AppDispatch = typeof store.dispatch;
export const useAppDispatch: ()=> AppDispatch = useDispatch

/*
export {
    RootState,
    AppDispatch,
    useAppDispatch
}*/