﻿using Alofa.SignalR.Service.Hubs;
using DotNetCore.CAP;
using Microsoft.AspNetCore.SignalR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alofa.Messaging.Contracts.Events;

namespace Alofa.SignalR.Service.EventHandler
{
    public class MessageEventHandler: ICapSubscribe
    {
        private IHubContext<MessageHub> _hubContext;
        public MessageEventHandler(IHubContext<MessageHub> hubContext)
        {
            _hubContext = hubContext;
        }

        [CapSubscribe(nameof(MessageSentEvent))]
        public void MessageSentEventHandler(MessageSentEvent messageSentEvent)
        {
            Console.WriteLine($"classname: {nameof(MessageEventHandler)}");
            _hubContext.Clients.Groups(messageSentEvent.RecipientUsername)
                .SendAsync("newMessage", messageSentEvent);

            _hubContext.Clients.Groups(messageSentEvent.SenderUsername)
                .SendAsync("messageSent", messageSentEvent);
            //_hubContext.Clients.us()
        }

    }
}
