﻿using Microsoft.Extensions.DependencyInjection;
using System;
using Microsoft.AspNetCore.SignalR;
using Alofa.SignalR.Service.EventHandler;

namespace Alofa.SignalR.Service
{
    public static class ConfigureService
    {
        public static void AddAlofaNotification(this IServiceCollection serviceCollection)
        {
            serviceCollection.AddSignalR();

            serviceCollection.AddTransient<MessageEventHandler>();
        }
    }
}
