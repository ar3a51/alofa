﻿using Microsoft.AspNetCore.Routing;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.SignalR;
using Microsoft.AspNetCore.Builder;
using Alofa.SignalR.Service.Hubs;

namespace Alofa.SignalR.Service
{
    public static class ConfigureEndpoint
    {
        public static void MapAlofaHub(this IEndpointRouteBuilder endpoint)
        {
            endpoint.MapHub<MessageHub>("/hub/message");
        }
    }
}
