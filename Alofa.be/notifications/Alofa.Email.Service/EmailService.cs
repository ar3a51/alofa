﻿using Alofa.Email.Service.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MailKit.Net.Smtp;
using MimeKit;

namespace Alofa.Email.Service
{
    public class EmailService: IDisposable
    {
        private EmailSettings _emailSettings;
        private SmtpClient _smtpClient;
        public EmailService(EmailSettings emailSettings)
        {
            _emailSettings = emailSettings;
            _smtpClient = new SmtpClient();
            _smtpClient.Connect(emailSettings.SmtpAddress, int.Parse(_emailSettings.Port), true);
            _smtpClient.Authenticate(emailSettings.AccountName, emailSettings.Password);
        }

        public void Dispose()
        {
            _smtpClient.Dispose();
        }

        public void SendEmail(string body, string subject, string name, string destinationAddress)
        {
            MimeMessage message = new MimeMessage();

            MailboxAddress from = new MailboxAddress(_emailSettings.From, _emailSettings.AccountName);
            message.From.Add(from);
            MailboxAddress to = new MailboxAddress(name, destinationAddress);
            message.To.Add(to);

            BodyBuilder builder = new BodyBuilder();
            builder.TextBody = body;

            message.Subject = subject;
            message.Body = builder.ToMessageBody();

            _smtpClient.Send(message);
            _smtpClient.Disconnect(true);

        }
    }
}
