﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alofa.Email.Service.Entities;

namespace Alofa.Email.Service.Repository
{
    public interface IEmailRepository
    {
        EmailEntity GetEmail(string username);
        void UpdateDetails(EmailEntity entity);
        void DeleteEmail(string username);

        void CreateEmail(EmailEntity entity);
    }
}
