﻿using Alofa.Email.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToDB;

namespace Alofa.Email.Service.Repository
{
    public class EmailRepository : IEmailRepository
    {
        private EmailDBConnection _dBConnection;
        public EmailRepository(EmailDBConnection emailDBConnection) 
        {
            _dBConnection = emailDBConnection;
        }

        public void CreateEmail(EmailEntity entity)
        {
            _dBConnection.Insert<EmailEntity>(entity);
        }

        public void DeleteEmail(string username)
        {
            _dBConnection
                .Email
                 .Delete(m => m.Username.ToLower() == username.ToLower());
        }

        public EmailEntity GetEmail(string username)
        {
            return _dBConnection
                        .Email
                        .Where(e => e.Username.ToLower() == username)
                        .FirstOrDefault();
        }

        public void UpdateDetails(EmailEntity entity)
        {
            _dBConnection
                        .Email
                        .Where(e => e.Username.ToLower() == entity.Username.ToLower() && e.UpdatedDate <= entity.UpdatedDate)
                        .Set(e => e.Firstname, entity.Firstname)
                        .Set(e => e.Lastname, entity.Lastname)
                        .Set(e => e.Email, entity.Email)
                        .Set(e => e.UpdatedDate, entity.UpdatedDate);
        }
    }
}
