﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetCore.CAP;
using Alofa.Messaging.Contracts.Events;

namespace Alofa.Email.Service.EventHandler
{
    public class MessagingHandler: ICapSubscribe
    {
        [CapSubscribe(nameof(MessageSentEvent))]
        public void HandleMessageReceived(MessageSentEvent messageSentEvent)
        {

        }
    }
}
