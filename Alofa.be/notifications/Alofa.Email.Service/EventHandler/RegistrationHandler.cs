﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetCore.CAP;
using Alofa.Auth.Contract.Events;
using Alofa.Email.Service.Repository;
using Alofa.Email.Service.Entities;

namespace Alofa.Email.Service.EventHandler
{
    public class RegistrationHandler: ICapSubscribe
    {
        private IEmailRepository _emailRepository;
        private EmailService _emailService;
        public RegistrationHandler(IEmailRepository emailRepository, EmailService emailService) 
        {
            _emailRepository = emailRepository;
            _emailService = emailService;
        }

        [CapSubscribe(nameof(AuthCreatedEvent))]
        public void HandleNewRegistration(AuthCreatedEvent authCreatedEvent)
        {
            var existingEmail = _emailRepository.GetEmail(authCreatedEvent.Username);
            if (existingEmail == null)
            {
                var emailEntity = new EmailEntity
                {
                    Username = authCreatedEvent.Username,
                    Firstname = authCreatedEvent.FirstName,
                    Lastname = authCreatedEvent.LastName,
                    Email = authCreatedEvent.EmailAddress,
                    CreatedDate = authCreatedEvent.CreatedDate,
                    UpdatedDate = authCreatedEvent.UpdatedDate
                };

                _emailRepository.CreateEmail(emailEntity);

                _emailService.SendEmail("You have successfully registered", "Registration", authCreatedEvent.Username, authCreatedEvent.EmailAddress);
            }

         
        }
    }
}
