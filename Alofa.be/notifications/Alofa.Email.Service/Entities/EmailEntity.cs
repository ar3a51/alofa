﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToDB.Mapping;

namespace Alofa.Email.Service.Entities
{
    [Table("Email")]
    public class EmailEntity
    {
        [Column(nameof(Username))]
        public string Username { set; get; }

        [Column(nameof(Firstname))]
        public string Firstname { set; get; }

        [Column(nameof(Lastname))]
        public string Lastname { set; get; }
        
        [Column(nameof(Email))]
        public string Email { set; get; }
        
        [Column(nameof(CreatedDate))]
        public DateTime CreatedDate { set; get; }

        [Column(nameof(UpdatedDate))]
        public DateTime UpdatedDate { set; get; }
    }
}
