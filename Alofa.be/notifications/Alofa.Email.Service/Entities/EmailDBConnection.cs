﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToDB;
using LinqToDB.Configuration;
using LinqToDB.Data;

namespace Alofa.Email.Service.Entities
{
    public class EmailDBConnection: DataConnection
    {
        public EmailDBConnection(LinqToDbConnectionOptions options): base(options)
        {

        }

        public ITable<EmailEntity> Email => GetTable<EmailEntity>();
    }
}
