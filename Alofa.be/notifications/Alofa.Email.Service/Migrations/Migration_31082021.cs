﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Alofa.Email.Service.Migrations
{
    [Tags("Alofa.Email")]
    [Migration(31082021, "Initialisation")]
    public class Migration_31082021 : MigrationBase
    {
        public override void Down()
        {
            throw new NotImplementedException();
        }

        public override void Up()
        {
            Create.Table("Email")
                    .WithColumn("Username").AsString().NotNullable()
                    .WithColumn("Firstname").AsString().NotNullable()
                    .WithColumn("Lastname").AsString().NotNullable()
                    .WithColumn("Email").AsString().NotNullable()
                    .WithColumn("CreatedDate").AsDateTime().NotNullable()
                    .WithColumn("UpdatedDate").AsDateTime().NotNullable();
        }
    }
}
