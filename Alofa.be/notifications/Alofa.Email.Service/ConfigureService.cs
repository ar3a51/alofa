﻿using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System;
using FluentMigrator;
using FluentMigrator.Runner;
using System.Reflection;
using FluentMigrator.Runner.Initialization;
using Alofa.Email.Service.EventHandler;
using LinqToDB.AspNet;
using LinqToDB.Configuration;
using Alofa.Email.Service.Entities;
using LinqToDB.AspNet.Logging;
using Alofa.Email.Service.Repository;

namespace Alofa.Email.Service
{
    public static class ConfigureService
    {
        public static void RegisterHandler(this IServiceCollection service)
        {
            service.AddTransient<RegistrationHandler>();
            service.AddTransient<MessagingHandler>();
        }
        public static void AddAlofaEmailHandler(this IServiceCollection service, IConfiguration configuration)
        {
            MigrateDb(configuration);
            service.AddLinqToDbContext<EmailDBConnection>((provider, options) => {
                options
                  .UsePostgreSQL(configuration.GetConnectionString("EmailDB"))
                   .UseDefaultLogging(provider);
            });

            service.AddScoped<IEmailRepository, EmailRepository>();
            service.AddTransient<EmailService>(s => new EmailService(
                    new Models.EmailSettings
                    {
                        AccountName = configuration["SMTP:AccountName"].Trim(),
                        From = configuration.GetSection("SMTP:From").Value,
                        SmtpAddress = configuration.GetSection("SMTP:Address").Value,
                        Password = configuration.GetSection("SMTP:password").Value
                    }    
                )
            );

            RegisterHandler(service);
        }

        private static void MigrateDb(IConfiguration configuration)
        {
            var migrationMemberSvc = new ServiceCollection()
               .AddFluentMigratorCore()
                  .ConfigureRunner(rb => rb
                  .AddPostgres()
                  .WithGlobalConnectionString(configuration.GetConnectionString("EmailDB"))
                  .WithMigrationsIn(Assembly.Load("Alofa.Email.Service")))
               .AddLogging(lb => lb.AddFluentMigratorConsole())
                .Configure<RunnerOptions>(opt => {
                    opt.Tags = new[] { "Alofa.Email" };
                })
               .BuildServiceProvider(false);

            using (var scope = migrationMemberSvc.CreateScope())
            {

                var runner = scope.ServiceProvider.GetRequiredService<IMigrationRunner>();
                runner.MigrateUp();
            }
        }
    }
}
