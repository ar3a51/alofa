﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Email.Service.Models
{
   public class EmailSettings
    {
        public string SmtpAddress { set; get; }
        public string From { set; get; }
        public string AccountName { set; get; }
        public string Password { set; get; }
        public string Port { set; get; }
    }
}
