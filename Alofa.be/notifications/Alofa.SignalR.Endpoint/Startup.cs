using Alofa.SignalR.Service;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.SignalR.Endpoint
{
    public class Startup
    {
        private IConfiguration Configuration;
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var key = Encoding.ASCII.GetBytes(Configuration["Authentication:Secret"]);
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(opt =>
                    {
#if DEBUG
                        opt.RequireHttpsMetadata = false;
                        opt.IncludeErrorDetails = true;
#else
                        opt.RequireHttpsMetadata = true;
                        opt.IncludeErrorDetails = true;
#endif
                        opt.Audience = "http://localhost:5000";
                        opt.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = new SymmetricSecurityKey(key),
                            ValidateIssuer = false,
                            ValidateAudience = false,
                            ClockSkew = TimeSpan.Zero,
                            ValidateLifetime = true
                            //ValidIssuer = "http://localhost:5000"
                        };

                        opt.Events = new JwtBearerEvents()
                        {
                            OnMessageReceived = context =>
                            {
                                var accessToken = context.Request.Query["access_token"];

                                // If the request is for our hub...
                                var path = context.HttpContext.Request.Path;
                                if (!string.IsNullOrEmpty(accessToken))
                                {
                                    // Read the token out of the query string
                                    context.Token = accessToken;
                                }
                                return System.Threading.Tasks.Task.CompletedTask;
                            }
                        };

                    });
            services.AddAuthorization();
            services.AddAlofaNotification();

            Console.WriteLine($"RabbitMQ: {Configuration.GetSection("RabbitMQURL").Value}");
            services.AddCap(c =>
            {

                c.UsePostgreSql(Configuration.GetConnectionString("CapDB"));
                c.UseRabbitMQ(Configuration.GetSection("RabbitMQURL").Value);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseRouting();
            app.UseAuthentication();

            app.UseAuthorization();
            app.UseEndpoints(endpoints =>
            {
                endpoints.MapAlofaHub();
            });
        }
    }
}
