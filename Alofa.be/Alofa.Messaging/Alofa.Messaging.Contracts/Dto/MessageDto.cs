﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Messaging.Contracts.Dto
{
    public class MessageDto
    {
        public string FromUsername { set; get; }
        public string ToUsername { set; get; }

        public string Message { set; get; } 
        public Guid MessageId { set; get; }
        public DateTime DateSent { set; get; }
    }
}
