﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Messaging.Contracts.Events
{
    public class MessageReadEvent
    {
        public Guid MessageId { set; get; }
        public string username { set; get; }
    }
}
