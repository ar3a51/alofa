﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Messaging.Contracts.Events
{
    public class MessageSentEvent
    {
        public Guid MessageId { set; get; }
        public string SenderUsername { set; get; }
        public string RecipientUsername { set; get; }
        public DateTime DateSent { set; get; }
    }
}
