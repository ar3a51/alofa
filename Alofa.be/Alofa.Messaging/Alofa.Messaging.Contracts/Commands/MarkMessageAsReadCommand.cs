﻿using Alofa.Common.BaseCommand;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Messaging.Contracts.Commands
{
    public class MarkMessageAsReadCommand: BaseUpdateCommand, IRequest
    {
        public Guid MessageId { set; get; }
        public string Username { set; get; }
    }
}
