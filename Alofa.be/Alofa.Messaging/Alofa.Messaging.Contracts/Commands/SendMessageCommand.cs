﻿using Alofa.Common.BaseCommand;
using Alofa.Messaging.Contracts.Dto;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Messaging.Contracts.Commands
{
    public class SendMessageCommand: BaseCreateCommand, IRequest
    {
       public MessageDto MessageDto { set; get; }
    }
    
}
