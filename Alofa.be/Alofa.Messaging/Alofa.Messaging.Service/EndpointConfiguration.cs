﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Text.Json;
using System.Threading.Tasks;
using Alofa.Messaging.Contracts.Commands;
using Alofa.Messaging.Service.Services;
using DotNetCore.CAP;
using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using Microsoft.Extensions.DependencyInjection;

namespace Alofa.Messaging.Service
{
    public static class EndpointConfiguration
    {
        public static void MapMessaging(this IEndpointRouteBuilder endpoint)
        {
            endpoint.MapGet("/api/received", async context => {
                var username = context.User.Identity.Name;
                var service = context.RequestServices.GetRequiredService<IMessageService>();
                var pageSize = int.Parse(context.Request.Query["pagesize"]);
                var pageNumber = int.Parse(context.Request.Query["pagenumber"]);

                var options = new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                };


                var result = service.GetReceivedMessages(username, pageSize, pageNumber);
                await context.Response.WriteAsync(JsonSerializer.Serialize(result, options));

            }).RequireAuthorization(new AuthorizeAttribute());

            endpoint.MapGet("/api/sent", async context => {
                var username = context.User.Identity.Name;
                var service = context.RequestServices.GetRequiredService<IMessageService>();
                var pageSize = int.Parse(context.Request.Query["pagesize"]);
                var pageNumber = int.Parse(context.Request.Query["pagenumber"]);

                var options = new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                };

                var result = service.GetSentMessages(username, pageSize, pageNumber);
                await context.Response.WriteAsync(JsonSerializer.Serialize(result, options));
            }).RequireAuthorization(new AuthorizeAttribute());

            endpoint.MapGet("/api/newmessagecount", async context => {
                var username = context.User.Identity.Name;
                var service = context.RequestServices.GetRequiredService<IMessageService>();
                var pageSize = int.Parse(context.Request.Query["messageid"]);

                var result = service.GetNumberNewMessages(username);
                await context.Response.WriteAsync(JsonSerializer.Serialize(new { newMessageCount = result }));

            }).RequireAuthorization(new AuthorizeAttribute());

            endpoint.MapGet("/api/detail", async context => {
                var username = context.User.Identity.Name;
                var service = context.RequestServices.GetRequiredService<IMessageService>();
                var messageId = context.Request.Query["messageId"];

                var result = service.GetMessage(username, Guid.Parse(messageId));
                var options = new JsonSerializerOptions
                {
                    PropertyNamingPolicy = JsonNamingPolicy.CamelCase,
                };
                await context.Response.WriteAsync(JsonSerializer.Serialize(result, options));

            }).RequireAuthorization(new AuthorizeAttribute());

            endpoint.MapPost("/api/send", async context => {

                var publish = context.RequestServices.GetService<IMediator>();
                var data = context.Items["body"] as dynamic;

                await publish.Send(new SendMessageCommand
                {    MessageDto = new Contracts.Dto.MessageDto
                    {
                        FromUsername = context.User.Identity.Name,
                        ToUsername = data.receiver,
                        MessageId = Guid.NewGuid(),
                        Message = data.message,
                        DateSent = DateTime.Now.ToUniversalTime()
                    }
                });

                context.Response.StatusCode = 200;
                await context.Response.WriteAsync("Ok");
                
            }).RequireAuthorization(new AuthorizeAttribute());

            endpoint.MapPost("/api/markasread", async context =>
            {
                var publish = context.RequestServices.GetService<IMediator>();
                var data = context.Items["body"] as dynamic;

                await publish.Send(new MarkMessageAsReadCommand { 
                    UpdatedDate = DateTime.Now.ToUniversalTime(),
                    MessageId = data.messageId,
                    Username = context.User.Identity.Name
                });

                context.Response.StatusCode = 200;
                await context.Response.WriteAsync("Ok");
            }).RequireAuthorization(new AuthorizeAttribute());
        }
    }
}
