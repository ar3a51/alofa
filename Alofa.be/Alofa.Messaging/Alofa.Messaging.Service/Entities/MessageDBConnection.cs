﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToDB;
using LinqToDB.Configuration;
using LinqToDB.Data;

namespace Alofa.Messaging.Service.Entities
{
    public class MessageDBConnection : DataConnection
    {
        public MessageDBConnection(LinqToDbConnectionOptions options) : base(options)
        {

        }

        public ITable<MessageEntity> Message => GetTable<MessageEntity>();
    }
}
