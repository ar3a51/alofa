﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToDB;
using LinqToDB.Mapping;

namespace Alofa.Messaging.Service.Entities
{
    [Table("Message")]
    public class MessageEntity
    {
        [Column("MessageId")]
        public Guid MessageID { set; get; }

        [Column("FromUsername")]
        public string FromUsername { set; get; }

        [Column("ToUsername")]
        public string ToUsername { set; get; }

        [Column("Message")]
        public string Message { set; get; }
        
        [Column("DateSent")]
        public DateTime DateSent { set; get; }

        [Column("IsRead")]
        public bool IsRead { set; get; }

        [Column("UpdatedDate")]
        public DateTime UpdatedDate { set; get; }
    }
}
