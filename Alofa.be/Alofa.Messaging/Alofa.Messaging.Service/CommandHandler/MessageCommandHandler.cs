﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Alofa.Messaging.Contracts.Commands;
using Alofa.Messaging.Contracts.Events;
using Alofa.Messaging.Service.Entities;
using Alofa.Messaging.Service.Repository;
using DotNetCore.CAP;
using LinqToDB;
using MediatR;

namespace Alofa.Messaging.Service.CommandHandler
{
    public class MessageCommandHandler: 
        IRequestHandler<MarkMessageAsReadCommand>,
        IRequestHandler<SendMessageCommand>
    {
        private MessageDBConnection _dbConnection;
        private ICapPublisher _capPublisher;
        private IMessageRepository _messageRepository;
        public MessageCommandHandler(
            IMessageRepository messageRepository,
            ICapPublisher capPublisher,
            MessageDBConnection dbConnection)
        {
            _messageRepository = messageRepository;
            _capPublisher = capPublisher;
            _dbConnection = dbConnection;
        }

        public Task<Unit> Handle(MarkMessageAsReadCommand markMessageAsReadCommand, CancellationToken cancellationToken)
        {
            try
            {
                _dbConnection.BeginTransaction();

                _messageRepository.MarkMessageAsRead(_dbConnection,
                    markMessageAsReadCommand.MessageId, 
                    markMessageAsReadCommand.UpdatedDate);

                _capPublisher.Publish(nameof(MessageReadEvent), new MessageReadEvent
                {
                    MessageId = markMessageAsReadCommand.MessageId,
                    username = markMessageAsReadCommand.Username
                });

                _dbConnection.CommitTransaction();
            }
            catch
            {
                _dbConnection.RollbackTransaction();
            }

            return Task.FromResult(Unit.Value);
        }

        public Task<Unit> Handle(SendMessageCommand request, CancellationToken cancellationToken)
        {
            var sendMessageCommand = request.MessageDto;
            try 
            { 
                _dbConnection.BeginTransaction();

                var message = new MessageEntity
                {
                    FromUsername = sendMessageCommand.FromUsername,
                    ToUsername = sendMessageCommand.ToUsername,
                    MessageID = sendMessageCommand.MessageId,
                    DateSent = sendMessageCommand.DateSent,
                    IsRead = false,
                    UpdatedDate = request.UpdatedDate,
                    Message = sendMessageCommand.Message
                };

                _messageRepository.SendMessage(_dbConnection, message);

                _capPublisher.Publish(nameof(MessageSentEvent), new MessageSentEvent { 
                    MessageId = message.MessageID,
                    DateSent = message.DateSent,
                    RecipientUsername = message.ToUsername,
                    SenderUsername = message.FromUsername
                });

                _dbConnection.CommitTransaction();
            }
            catch 
            {
                _dbConnection.RollbackTransaction();
            }

            return Task.FromResult(Unit.Value);
        }
    }
}
