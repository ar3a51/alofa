﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Alofa.Messaging.Service.Migrations
{
    [Tags("Alofa.Messaging")]
    [Migration(25072021,"Initialisation")]
    public class Migration_25072021 : MigrationBase
    {
        public override void Down()
        {
            throw new NotImplementedException();
        }

        public override void Up()
        {
            Create.Table("Message")
                .WithColumn("MessageId").AsGuid()
                .WithColumn("FromUsername").AsString()
                .WithColumn("ToUsername").AsString()
                .WithColumn("Message").AsString()
                .WithColumn("IsRead").AsBoolean()
                .WithColumn("DateSent").AsDateTime()
                .WithColumn("UpdatedDate").AsDateTime();
        }
    }
}
