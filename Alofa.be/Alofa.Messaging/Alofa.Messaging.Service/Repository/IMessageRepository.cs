﻿using Alofa.Messaging.Service.Entities;
using System;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Alofa.Messaging.Service.Repository
{
    public interface IMessageRepository
    {
        public Task<IEnumerable<MessageEntity>> GetReceivedMessages(string userName, int recordSize, int offset);
        public Task<IEnumerable<MessageEntity>> GetSentMessages(string username, int recordSize, int offset);
        public Task<MessageEntity> GetMessage(Guid messageId, string username);
        public Task<int> GetNumberNewMessages(string username);

        public void SendMessage(MessageDBConnection dbConnection, MessageEntity message);
        public void MarkMessageAsRead(MessageDBConnection dbConnection, Guid messageId, DateTime updatedDate);
    }
}
