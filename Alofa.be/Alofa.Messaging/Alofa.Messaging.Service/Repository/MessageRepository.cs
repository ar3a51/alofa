﻿using Alofa.Messaging.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using LinqToDB;
using System.Threading.Tasks;

namespace Alofa.Messaging.Service.Repository
{
    public class MessageRepository : IMessageRepository
    {
        private MessageDBConnection _dbConnection;
        public MessageRepository(MessageDBConnection dBConnection)
        {
            _dbConnection = dBConnection;
        }
        public async Task<MessageEntity> GetMessage(Guid messageId, string username)
        {
            var message = await _dbConnection.Message.SingleAsync(m => m.MessageID == messageId && 
                                (m.FromUsername == username || m.ToUsername == username));
            return message;
        }

        public async Task<int> GetNumberNewMessages(string username)
        {
            return await _dbConnection.Message.Where(m => m.IsRead == false && m.ToUsername == username).CountAsync();
        }

        public async Task<IEnumerable<MessageEntity>> GetReceivedMessages(string userName, int recordSize, int offset)
        {
            var messages = await _dbConnection.Message
                .Where(m => m.ToUsername.ToLower() == userName.ToLower())
                .Skip(offset)
                .Take(recordSize)
                .ToListAsync();

            return messages;
        }

        public async Task<IEnumerable<MessageEntity>> GetSentMessages(string username, int recordSize, int offset)
        {
            var messages = await _dbConnection.Message
               .Where(m => m.FromUsername.ToLower() == username.ToLower())
               .Skip(offset)
               .Take(recordSize)
               .ToListAsync();

            return messages;
        }

        public void MarkMessageAsRead(MessageDBConnection dbConnection, Guid messageId, DateTime updatedDate)
        {
            dbConnection.Message
                     .Where(m => m.MessageID == messageId)
                     .Set(m => m.IsRead, true)
                     .Set(m => m.UpdatedDate, updatedDate);

        }

        public void SendMessage(MessageDBConnection dbConnection, MessageEntity message)
        {
            dbConnection.Insert<MessageEntity>(message);
        }
    }
}
