﻿using Alofa.Messaging.Contracts.Dto;
using Alofa.Messaging.Service.Repository;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Messaging.Service.Services
{
    public class MessageService : IMessageService
    {
        private IMessageRepository _messageReadRepository;
        public MessageService(IMessageRepository messageReadRepository)
        {
            _messageReadRepository = messageReadRepository;
        }

        public async Task<MessageDto> GetMessage(string username, Guid messageId)
        {
            var result = await _messageReadRepository.GetMessage(messageId, username);
            return new MessageDto
            {
                Message = result.Message,
                MessageId = result.MessageID,
                DateSent = result.DateSent,
                FromUsername = result.FromUsername,
                ToUsername = result.ToUsername
            };
        }

        public async Task<int> GetNumberNewMessages(string username)
        {
            return await _messageReadRepository.GetNumberNewMessages(username);
        }

        public async Task<IEnumerable<MessageDto>> GetReceivedMessages(string username, int pageSize, int pageNumber)
        {
           int offset = pageSize * (pageNumber - 1);
           var entities = await _messageReadRepository.GetReceivedMessages(username, pageSize, offset);
           var messages = entities.Select(m => {
               return new MessageDto
               {
                   Message = m.Message,
                   FromUsername = m.FromUsername,
                   MessageId = m.MessageID,
                   DateSent = m.DateSent,
               };

           });

        return messages;
        }

        public async Task<IEnumerable<MessageDto>> GetSentMessages(string username, int pageSize, int pageNumber)
        {
            int offset = pageSize * (pageNumber - 1);
            var entities = await _messageReadRepository.GetSentMessages(username, pageSize, offset);
            var messages = entities.Select(m => {
                return new MessageDto
                {
                    Message = m.Message,
                    FromUsername = m.FromUsername,
                    MessageId = m.MessageID,
                    DateSent = m.DateSent,
                };

            });

            return messages;
        }
    }
}
