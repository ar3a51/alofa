﻿using Alofa.Messaging.Contracts.Dto;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Messaging.Service.Services
{
    public interface IMessageService
    {
        Task<IEnumerable<MessageDto>> GetReceivedMessages(string username, int pageSize, int pageNumber);
        Task<IEnumerable<MessageDto>> GetSentMessages(string username, int pageSize, int pageNumber);
        Task<int> GetNumberNewMessages(string username);
        Task<MessageDto> GetMessage(string username, Guid messageId);
    }
}
