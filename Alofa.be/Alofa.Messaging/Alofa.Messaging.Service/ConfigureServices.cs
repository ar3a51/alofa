﻿using System;
using System.Reflection;
using Alofa.Messaging.Service.Entities;
using FluentMigrator;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Initialization;
using LinqToDB;
using LinqToDB.AspNet;
using LinqToDB.AspNet.Logging;
using LinqToDB.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Alofa.Messaging.Service.Repository;
using Alofa.Messaging.Service.Services;
using Alofa.Messaging.Service.CommandHandler;
using MediatR;

namespace Alofa.Messaging.Service
{
    public static class ConfigureServices
    {

        public static void ConfigureMessagingHandler(this IServiceCollection service)
        {
            service.AddMediatR(typeof(MessageCommandHandler).Assembly);
        }
        public static void AddAlofaMessagingCommandService(this IServiceCollection service, IConfiguration configuration)
        {
            service.AddLinqToDbContext<MessageDBConnection>((provider, options) => {
                options
                  .UsePostgreSQL(configuration.GetConnectionString("MessagingDB"))
                   .UseDefaultLogging(provider);
            });

            service.AddScoped<IMessageRepository, MessageRepository>();
            MigrateDb(configuration);
        }

        public static void AddAlofaMessagingQueryService(this IServiceCollection service, IConfiguration configuration)
        {
            service.AddLinqToDbContext<MessageDBConnection>((provider, options) => {
                options
                  .UsePostgreSQL(configuration.GetConnectionString("MessagingDB"))
                   .UseDefaultLogging(provider);
            });

            service.AddScoped<IMessageRepository, MessageRepository>();
            service.AddScoped<IMessageService, MessageService>();
        }

        private static void MigrateDb(IConfiguration configuration)
        {
            var migrationMemberSvc = new ServiceCollection()
               .AddFluentMigratorCore()
                  .ConfigureRunner(rb => rb
                  .AddPostgres()
                  .WithGlobalConnectionString(configuration.GetConnectionString("MessagingDB"))
                  .WithMigrationsIn(Assembly.Load("Alofa.Messaging.Service")))
               .AddLogging(lb => lb.AddFluentMigratorConsole())
                .Configure<RunnerOptions>(opt => {
                    opt.Tags = new[] { "Alofa.Messaging" };
                })
               .BuildServiceProvider(false);

            using (var scope = migrationMemberSvc.CreateScope())
            {

                var runner = scope.ServiceProvider.GetRequiredService<IMigrationRunner>();
                runner.MigrateUp();
            }
        }
    }
}
