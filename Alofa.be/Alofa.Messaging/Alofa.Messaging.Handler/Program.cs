using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Alofa.Messaging.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alofa.Messaging.Handler
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
          Host.CreateDefaultBuilder(args)
              .ConfigureServices((hostContext, services) =>
              {

                  services.AddAlofaMessagingCommandService(hostContext.Configuration);

                  services.ConfigureMessagingHandler();

                  services.AddCap(c => {
                      c.UsePostgreSql(hostContext.Configuration.GetConnectionString("CapDB"));
                      c.UseRabbitMQ(hostContext.Configuration.GetSection("RabbitMQURL").Value);
                  });


              });
    }
}
