﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

using Alofa.Auth.Contract.Events;

using Dapr;

namespace Alofa.Messaging.EventHandler.Controllers
{
    [ApiController]
    public class AuthenticationEventHandler : ControllerBase
    {
        [Topic("AlofaPubSub", nameof(AuthCreatedEvent))]
        [HttpPost(nameof(AuthCreatedEvent))]
        public void HandleAuthCreated([FromBody] AuthCreatedEvent authCreatedEvent)
        {
            Console.WriteLine($"hello {authCreatedEvent.FirstName} {authCreatedEvent.LastName} {authCreatedEvent.Username}");
        }
    }
}
