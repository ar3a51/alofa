using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alofa.Member.Service;
using Alofa.Web.Common.Middleware;
using Alofa.Member.Endpoint.Middleware;
using Alofa.Member.Endpoint.Services;

namespace Alofa.Member.Endpoint
{
    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
        }

        public IConfiguration Configuration { get; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
             var key = Encoding.ASCII.GetBytes(Configuration["Authentication:Secret"]);
             services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                     .AddJwtBearer(opt =>
                     {
 #if DEBUG
                         opt.RequireHttpsMetadata = false;
                         opt.IncludeErrorDetails = true;
 #else
                         opt.RequireHttpsMetadata = true;
                         opt.IncludeErrorDetails = true;
 #endif
                         opt.Audience = "http://localhost:5001";
                         opt.TokenValidationParameters = new TokenValidationParameters
                         {
                             ValidateIssuerSigningKey = true,
                             IssuerSigningKey = new SymmetricSecurityKey(key),
                             ValidateIssuer = true,
                             ValidateAudience = true,
                             ClockSkew = TimeSpan.Zero,
                             ValidateLifetime = false,
                             ValidIssuer = "http://localhost:5000"
                         };
                     });
            services.AddSingleton<IJwtValidator, JwtValidator>();

            services.AddAuthorization();

            services.AddAlofaMemberQuery(Configuration);
            services.AddAlofaMemberWrite(Configuration);

            Console.WriteLine($"RabbitMQ: {Configuration.GetSection("RabbitMQURL").Value}");
            services.AddCap(c =>
            {

                c.UsePostgreSql(Configuration.GetConnectionString("CapDB"));
                c.UseRabbitMQ(Configuration.GetSection("RabbitMQURL").Value);
            });
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
          
            app.UseRouting();

           

            app.UseAuthentication();

            //app.UseMiddleware<AuthenticationMiddleware>();

            app.UseMiddleware<BodyParserMiddleware>();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapMember();
            });
        }
    }
}
