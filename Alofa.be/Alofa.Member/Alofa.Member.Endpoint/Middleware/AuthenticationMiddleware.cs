﻿using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Alofa.Member.Service;
using Alofa.Member.Endpoint.Services;
using System.Security.Claims;

namespace Alofa.Member.Endpoint.Middleware
{
    public class AuthenticationMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IConfiguration _appSettings;
        private IJwtValidator _validator;

        public AuthenticationMiddleware(RequestDelegate next, IConfiguration appSettings, IJwtValidator validator)
        {
            _next = next;
            _validator = validator;
            _appSettings = appSettings;
        }

        public async Task Invoke(HttpContext context)
        {
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();
            var validatedToken = _validator.ValidateToken(token);

            if (validatedToken != null)
            {
                context.User = new System.Security.Claims.ClaimsPrincipal(new ClaimsIdentity(validatedToken.Claims));
                // attach user to context on successful jwt validation
               // context.Items["User"] = userId;
            }

            await _next(context);
        }
    }
}
