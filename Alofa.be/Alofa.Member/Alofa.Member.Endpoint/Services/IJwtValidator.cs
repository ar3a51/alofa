﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Threading.Tasks;

namespace Alofa.Member.Endpoint.Services
{
    public interface IJwtValidator
    {
        JwtSecurityToken ValidateToken(string token);
    }
}
