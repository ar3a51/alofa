using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Configuration;
using Alofa.Member.Service;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Alofa.Member.Handler
{
    public class Program
    {
        public static void Main(string[] args)
        {
            CreateHostBuilder(args).Build().Run();
        }

        public static IHostBuilder CreateHostBuilder(string[] args) =>
          Host.CreateDefaultBuilder(args)
              .ConfigureServices((hostContext, services) =>
              {

                  services.AddAlofaMemberWrite(hostContext.Configuration);

                 

                  services.AddCap(c => {
                      c.UsePostgreSql(hostContext.Configuration.GetConnectionString("CapDB"));
                      c.UseRabbitMQ(hostContext.Configuration.GetSection("RabbitMQURL").Value);
                  });


              });
    }
}
