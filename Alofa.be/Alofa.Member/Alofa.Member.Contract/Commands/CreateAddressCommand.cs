﻿using Alofa.Common.BaseCommand;
using Alofa.Member.Contract.DTOs;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Member.Contract.Commands
{
   public class CreateAddressCommand: BaseCreateCommand, IRequest
    {
        public AddressDTO Address { set; get; }
        public string Username { set; get; }
    }
}
