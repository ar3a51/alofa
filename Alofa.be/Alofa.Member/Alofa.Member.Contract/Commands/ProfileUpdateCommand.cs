﻿using Alofa.Common.BaseCommand;
using Alofa.Member.Contract.DTO;
using MediatR;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Member.Contract.Commands
{
    public class ProfileUpdateCommand: BaseUpdateCommand, IRequest
    {
        public ProfileDTO ProfileDTO { get; set; }
        public string Username { set; get; }
    }
}
