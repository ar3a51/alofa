﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Member.Contract.DTOs
{
    public class AddressDTO
    {
        public string Address1 { set; get; }
        public string Address2 { set; get; }
        public string Suburb { set; get; }
        public string Province { set; get; }
        public string Country { set; get; }
        public DateTime UpdatedDate { set; get; }
        public DateTime CreatedDate { set; get; }
        public Guid Id { set; get; }

        public string PostCode { set; get; }
    }
}
