﻿using System;

namespace Alofa.Member.Contract.DTO
{
    public class ProfileDTO
    {
        public Guid Id { set; get; }

        public string Username { set; get; }

        public string Gender { set; get; }

        public string Height { set; get; }

        public string Zodiac { set; get; }

        public string PreferredGender { set; get; }

        public string PreferredZodiac { set; get; }

        public string PreferredReligion { set; get; }

        public DateTime CreatedDate { set; get; }

        public DateTime UpdatedDate { set; get; }
    }
}
