﻿using Alofa.Common.BaseEvent;
using Alofa.Member.Contract.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Member.Contract.Events
{
    public record AddressCreatedEvent(AddressDTO Address, string Username, DateTime CreatedDate, DateTime UpdatedDate): 
        BaseCreateEvent(CreatedDate, UpdatedDate);

}
