﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Member.Contract.Events
{
    public class ProfileUpdatedEvent
    {
        public Guid Id { set; get; }

        public string Username { set; get; }

        public string Gender { set; get; }

        public string Height { set; get; }

        public string Zodiac { set; get; }

        public string PreferredGender { set; get; }

        public string PreferredZodiac { set; get; }

        public string PreferredReligion { set; get; }

        public DateTime CreatedDate { set; get; }

        public DateTime UpdatedDate { set; get; }
    }
}
