﻿using Alofa.Common.BaseEvent;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Member.Contract.Events
{
    public record AddressUpdatedEvent
        (DTOs.AddressDTO Address, string Username, DateTime UpdatedDate): BaseUpdateEvent(UpdatedDate);
}
