﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Member.Service.Migrations
{
    [Tags("Alofa.Member")]
    [Migration(06062021_2)]
    public class Migration_06062021_2_ProfileAddCreatedDate : MigrationBase
    {
        public override void Down()
        {
            throw new NotImplementedException();
        }

        public override void Up()
        {
            Alter.Table("Profile")
                .AddColumn("CreatedDate").AsDateTime();
        }
    }
}
