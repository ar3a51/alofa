﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Alofa.Member.Service.Migration
{
    [Tags("Alofa.Member")]
    [Migration(06062021)]
    public class Migration_06062021_CreateTables : MigrationBase
    {
        public override void Down()
        {
            throw new NotImplementedException();
        }

        public override void Up()
        {
            Create.Table("Address")
                .WithColumn("Id").AsGuid().NotNullable().PrimaryKey()
                .WithColumn("Address1").AsString()
                .WithColumn("Address2").AsString()
                .WithColumn("Suburb").AsString()
                 .WithColumn("Province").AsString()
                  .WithColumn("PostCode").AsString()
                 .WithColumn("Country").AsString()
                .WithColumn("CreatedDate").AsDateTime().NotNullable()
                .WithColumn("UpdatedDate").AsDateTime().NotNullable();
               

            Create.Table("Profile")
                .WithColumn("Id").AsGuid().NotNullable().PrimaryKey()
                .WithColumn("Gender").AsString()
                .WithColumn("Height").AsString()
                .WithColumn("Zodiac").AsString()
                .WithColumn("ProfilePhoto").AsBinary()
                .WithColumn("PreferredGender").AsString()
                .WithColumn("PreferredZodiac").AsString()
                .WithColumn("PreferredReligion").AsString()
                .WithColumn("UpdatedDate").AsDateTime();
        }
    }
}
