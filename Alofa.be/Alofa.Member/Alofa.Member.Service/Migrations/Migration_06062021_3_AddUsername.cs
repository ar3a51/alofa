﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Alofa.Auth.Service.Migration
{
    [Tags("Alofa.Member")]
    [Migration(06062021_3)]
    public class Migration_06062021_3_AddUsername : MigrationBase
    {
        public override void Down()
        {
            throw new NotImplementedException();
        }

        public override void Up()
        {
            Alter.Table("Address")
                .AddColumn("Username").AsString().NotNullable();

            Alter.Table("Profile")
                .AddColumn("Username").AsString().NotNullable();
        }
    }
}
