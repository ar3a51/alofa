﻿using Alofa.Member.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Member.Service.Reads
{
    public interface IAddressRead
    {
        Task<Address> GetAddress(string username);
    }
}
