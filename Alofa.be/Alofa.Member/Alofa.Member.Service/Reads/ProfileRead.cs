﻿using Alofa.Member.Service.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using LinqToDB;

namespace Alofa.Member.Service.Reads
{
    public class ProfileRead : IProfileRead
    {
        private MemberDBConnection _dbConnection;
        public ProfileRead(MemberDBConnection dbConnection)
        {
            _dbConnection = dbConnection;
        }
        public async Task<Profile> GetProfileByUsername(string username)
        {
            return await _dbConnection.Profile
                    .SingleAsync(p => p.Username == username);
        }
    }
}
