﻿using Alofa.Member.Service.Entities;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;
using LinqToDB;

namespace Alofa.Member.Service.Reads
{
    public class AddressRead : IAddressRead
    {
        private MemberDBConnection _db;
        public AddressRead(MemberDBConnection dBConnection)
        {
            _db = dBConnection;
        }
        public async Task<Address> GetAddress(string username)
        {
            return await _db.Address
                            .FirstOrDefaultAsync(a => a.Username == username);
        }
    }
}
