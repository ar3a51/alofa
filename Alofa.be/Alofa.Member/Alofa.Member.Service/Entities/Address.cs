﻿using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Member.Service.Entities
{
    [Table(Name ="Address")]
    public class Address
    {
        [Column("Id",DataType = LinqToDB.DataType.Guid)]
        public Guid Id { set; get; }

        [Column(nameof(Username),DataType = LinqToDB.DataType.VarChar)]
        public string Username { set; get; }

        [Column("Address1", DataType = LinqToDB.DataType.VarChar)]
        public string Address1 { set; get; }
        [Column("Address2", DataType = LinqToDB.DataType.VarChar)]
        public string Address2 { set; get; }
        [Column("Suburb", DataType = LinqToDB.DataType.VarChar)]
        public string Suburb { set; get; }
        [Column("Province", DataType = LinqToDB.DataType.VarChar)]
        public string Province { set; get; }
        [Column("Country", DataType = LinqToDB.DataType.VarChar)]
        public string Country { set; get; }

        [Column("CreatedDate", DataType = LinqToDB.DataType.DateTime)]
        public DateTime CreatedDate { set; get; }

        [Column("UpdatedDate", DataType = LinqToDB.DataType.DateTime)]
        public DateTime UpdatedDate { set; get; }
    }
}
