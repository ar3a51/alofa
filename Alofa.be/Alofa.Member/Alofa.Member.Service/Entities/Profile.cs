﻿using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Member.Service.Entities
{
    [Table(Name = nameof(Profile))]
    public class Profile
    {

        [Column(nameof(Id),DataType = LinqToDB.DataType.Guid)]
        public Guid Id { set; get; }

        [Column(nameof(Username), DataType = LinqToDB.DataType.VarChar)]
        public string Username { set; get; }

        [Column(nameof(Gender), DataType = LinqToDB.DataType.VarChar)]
        public string Gender { set; get; }

        [Column(nameof(Height), DataType = LinqToDB.DataType.VarChar)]
        public string Height { set; get; }

        [Column(nameof(Zodiac), DataType = LinqToDB.DataType.VarChar)]
        public string Zodiac { set; get; }

        [Column(nameof(PreferredGender), DataType = LinqToDB.DataType.VarChar)]
        public string PreferredGender { set; get; }

        [Column(nameof(PreferredZodiac), DataType = LinqToDB.DataType.VarChar)]
        public string PreferredZodiac { set; get; }

        [Column(nameof(PreferredReligion), DataType = LinqToDB.DataType.VarChar)]
        public string PreferredReligion { set; get; }

        [Column(nameof(CreatedDate), DataType = LinqToDB.DataType.DateTime)]
        public DateTime CreatedDate { set; get; }

        [Column(nameof(UpdatedDate), DataType = LinqToDB.DataType.DateTime)]
        public DateTime UpdatedDate { set; get; }

        [Column(nameof(ProfilePhoto), DataType = LinqToDB.DataType.Binary)]
        public byte[] ProfilePhoto { set; get; }
         
    }
}
