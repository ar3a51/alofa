﻿using LinqToDB;
using LinqToDB.Configuration;
using LinqToDB.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Member.Service.Entities
{
    public class MemberDBConnection : DataConnection
    {
        public MemberDBConnection(LinqToDbConnectionOptions options):base(options)
        {

        }

        public ITable<Address> Address => GetTable<Address>();
        public ITable<Profile> Profile => GetTable<Profile>();

    }
}
