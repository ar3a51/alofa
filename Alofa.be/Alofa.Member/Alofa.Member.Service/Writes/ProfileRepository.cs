﻿using Alofa.Member.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToDB;

namespace Alofa.Member.Service.Writes
{
    public class ProfileRepository : IProfileRespository
    {
        private MemberDBConnection _db;

        public ProfileRepository(MemberDBConnection dbConnection)
        {
            _db = dbConnection;
        }
        public void AddNewProfile(Profile profile)
        {
            _db.Insert(profile);
        }

        public int UpdateProfile(Profile profile)
        {
            return _db.Profile
                        .Where(p => p.Id == profile.Id && p.UpdatedDate < profile.UpdatedDate)
                        .Set(p => p.Gender, profile.Gender)
                        .Set(p => p.Height, profile.Height)
                        .Set(p => p.PreferredGender, profile.PreferredGender)
                        .Set(p => p.PreferredReligion, profile.PreferredReligion)
                        .Set(p => p.PreferredZodiac, profile.PreferredZodiac)
                        .Set(p => p.UpdatedDate, DateTime.Now.ToUniversalTime())
                        .Update();
                       
        }

        public int UpdateProfilePicture(Guid profileId, byte[] profilePicture)
        {
            return _db.Profile
                    .Where(p => p.Id == profileId)
                    .Set(p => p.ProfilePhoto, profilePicture)
                    .Set(p => p.UpdatedDate, DateTime.Now.ToUniversalTime())
                    .Update();
        }
    }
}
