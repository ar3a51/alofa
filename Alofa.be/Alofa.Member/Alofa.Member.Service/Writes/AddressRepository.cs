﻿using Alofa.Member.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToDB;

namespace Alofa.Member.Service.Writes
{
    public class AddressRepository : IAddressRepository
    {
        private MemberDBConnection _dbConnection;
        public AddressRepository(MemberDBConnection dBConnection)
        {
            _dbConnection = dBConnection;
        }
        public void DeleteAddress(Address address)
        {
            _dbConnection.Address
                .Where(a => a.Id == address.Id)
                .Delete();
        }

        public void InsertNewAddress(Address address)
        {
            _dbConnection.Insert(address);
        }

        public void UpdateAddress(Address address)
        {
            _dbConnection.Address
                .Where(a => a.Id == address.Id && a.UpdatedDate < address.UpdatedDate)
                .Set(a => a.Address1, address.Address1)
                .Set(a => a.Address2, address.Address2)
                .Set(a => a.Suburb, address.Suburb)
                .Set(a => a.Province, address.Province)
                .Set(a => a.UpdatedDate, DateTime.Now.ToUniversalTime())
            .Update();
        }
    }
}
