﻿using Alofa.Member.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Member.Service.Writes
{
    public interface IProfileRespository
    {
        void AddNewProfile(Profile profile);

        int UpdateProfilePicture(Guid profileId, byte[] profilePicture);

        int UpdateProfile(Profile profile);
    }
}
