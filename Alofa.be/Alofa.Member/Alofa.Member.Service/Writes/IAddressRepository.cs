﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alofa.Member.Service.Entities;

namespace Alofa.Member.Service.Writes
{
    public interface IAddressRepository
    {
        void InsertNewAddress(Address address);
        void DeleteAddress(Address address);
        void UpdateAddress(Address address);
    }
}
