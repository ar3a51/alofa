﻿using Alofa.Member.Service.Services.Query;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Routing;
using System;
using System.Threading.Tasks;
using Microsoft.Extensions.DependencyInjection;
using System.Text.Json;
using DotNetCore.CAP;
using Alofa.Member.Contract.Commands;
using MediatR;

namespace Alofa.Member.Service
{
    public static class EndpointConfiguration
    {
        public static void MapMember(this IEndpointRouteBuilder endpoint)
        {
            #region Read
            endpoint.MapGet("/member/test", async context =>
            {
                await context.Response.WriteAsync($"this is member name {context.User.Identity.Name}");
            }).RequireAuthorization(new AuthorizeAttribute());

            endpoint.MapGet("/member/address", async context =>
            {
                var address = await context
                      .RequestServices.GetService<IAddressQuery>()
                          .GetAddress(context.User.Identity.Name);

                await context.Response.WriteAsync(JsonSerializer.Serialize(address));

            }).RequireAuthorization(new AuthorizeAttribute());

            endpoint.MapGet("/member/profile", async context =>
            {
                var profile = await context
                      .RequestServices.GetService<IMemberQuery>()
                          .GetMemberProfile(context.User.Identity.Name);

                await context.Response.WriteAsync(JsonSerializer.Serialize(profile));

            }).RequireAuthorization(new AuthorizeAttribute());

            #endregion

            #region Write
            endpoint.MapPost("/api/createprofile", async context =>
            {
                var data = context.Items["body"] as dynamic;

                var service = context
                      .RequestServices.GetService<IMediator>();

                var userName = context.User.Identity.Name;

                var newProfileCommand = new CreateProfileCommand
                {
                   ProfileDTO = new Contract.DTO.ProfileDTO
                   {
                       Id = Guid.NewGuid(),
                       Gender = data.gender,
                       PreferredGender = data.preferredGender,
                       PreferredReligion = data.preferredReligion,
                       PreferredZodiac = data.preferredZodiac,
                       Zodiac = data.zodiac,
                       Username = userName,
                       Height = data.height,
                       CreatedDate = DateTime.Now.ToUniversalTime(),
                       UpdatedDate = DateTime.Now.ToUniversalTime()
                   },
                   Username = userName,
                    CreatedDate = DateTime.Now.ToUniversalTime(),
                    UpdatedDate = DateTime.Now.ToUniversalTime()
                };

                await service.Send(newProfileCommand);

                await context.Response.WriteAsync("Ok");

            }).RequireAuthorization(new AuthorizeAttribute());

            endpoint.MapPut("/api/updateprofile", async context =>
            {
                var data = context.Items["body"] as dynamic;
                var userName = context.User.Identity.Name;

                var service = context
                      .RequestServices.GetService<IMediator>();


                var updatedProfileCommand = new ProfileUpdateCommand
                {
                    ProfileDTO = new Contract.DTO.ProfileDTO
                    {
                        Id = data.id,
                        Gender = data.gender,
                        PreferredGender = data.preferredGender,
                        PreferredReligion = data.preferredReligion,
                        PreferredZodiac = data.preferredZodiac,
                        Zodiac = data.zodiac,
                        Username = userName,
                        Height = data.height,
                        UpdatedDate = DateTime.Now.ToUniversalTime()
                    },
                    Username = userName,
                    UpdatedDate = DateTime.Now.ToUniversalTime()
                };

                await service.Send(updatedProfileCommand);

                await context.Response.WriteAsync("ok");

            }).RequireAuthorization(new AuthorizeAttribute());

            endpoint.MapPost("/api/createaddress", async context =>
            {

                var data = context.Items["body"] as dynamic;
                var userName = context.User.Identity.Name;

                var publisher = context
                      .RequestServices.GetService<IMediator>();

                var address = new CreateAddressCommand {
                    Address = new Contract.DTOs.AddressDTO
                    {
                        Address1 = data.address1,
                        Address2 = data.address2,
                        Country = data.country,
                        CreatedDate = DateTime.Now.ToUniversalTime(),
                        UpdatedDate = DateTime.Now.ToUniversalTime(),
                        Id = Guid.NewGuid(),
                        Province = data.province,
                        Suburb = data.suburb,
                        PostCode = data.postcode
                    },
                    Username = userName,
                    CreatedDate = DateTime.Now.ToUniversalTime(),
                    UpdatedDate = DateTime.Now.ToUniversalTime(),
                };

                await publisher.Send(address);

                await context.Response.WriteAsync("Ok");

            }).RequireAuthorization(new AuthorizeAttribute());

            endpoint.MapPut("/api/updateaddress", async context =>
            {
                var data = context.Items["body"] as dynamic;
                var userName = context.User.Identity.Name;

                var publisher = context
                      .RequestServices.GetService<IMediator>();

                var newAddress = new AddressUpdateCommand
                {
                    Address = new Contract.DTOs.AddressDTO
                    {
                        Address1 = data.address1,
                        Address2 = data.address2,
                        Country = data.country,
                        UpdatedDate = DateTime.Now.ToUniversalTime(),
                        Id = data.id,
                        Province = data.province,
                        Suburb = data.suburb,
                        PostCode = data.postcode
                    },
                    Username = userName,
                    UpdatedDate = DateTime.Now.ToUniversalTime(),
                };

                await publisher.Send(newAddress);

                await context.Response.WriteAsync("Ok");

            }).RequireAuthorization(new AuthorizeAttribute());

            #endregion
            #region read

            #endregion
        }

    }

}
