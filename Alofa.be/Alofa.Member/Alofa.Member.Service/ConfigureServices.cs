﻿using Alofa.Member.Service.CommandHandlers;
using Alofa.Member.Service.Entities;
using Alofa.Member.Service.Reads;
using Alofa.Member.Service.Services.Query;
using Alofa.Member.Service.Writes;
using FluentMigrator.Runner;
using FluentMigrator.Runner.Initialization;
using LinqToDB.AspNet;
using LinqToDB.AspNet.Logging;
using LinqToDB.Configuration;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using System.Reflection;
using MediatR;

namespace Alofa.Member.Service
{
    public static class ConfigureServices
    {
        public static void AddAlofaMemberQuery(this IServiceCollection service, IConfiguration configuration)
        {

            MigrateDb(configuration);

            AddAlofaMemberDBConnection(service, configuration);

            service.AddScoped<IAddressRead, AddressRead>();
            service.AddScoped<IProfileRead, ProfileRead>();
            service.AddScoped<IMemberQuery, MemberQuery>();
            service.AddScoped<IAddressQuery, AddressQuery>();
         
        }

        public static void AddAlofaMemberWrite(this IServiceCollection service, IConfiguration configuration)
        {
            AddAlofaMemberDBConnection(service, configuration);

            service.AddScoped<IProfileRespository, ProfileRepository>();
            service.AddScoped<IAddressRepository, AddressRepository>();
            service.AddScoped<IAddressRead, AddressRead>();
            service.AddScoped<IProfileRead, ProfileRead>();
            service.AddMediatR(new Assembly[] { 
                typeof(AddressCommandHandler).Assembly,
                typeof(MemberCommandHandler).Assembly
            });
           
        }

        private static void AddAlofaMemberDBConnection(this IServiceCollection service, IConfiguration configuration)
        {
            service.AddLinqToDbContext<MemberDBConnection>((provider, options) => {
                options
                    .UsePostgreSQL(configuration.GetConnectionString("MemberDB"))
                     .UseDefaultLogging(provider);
            });
        }

        private static void MigrateDb(IConfiguration configuration)
        {
            var migrationMemberSvc = new ServiceCollection()
               .AddFluentMigratorCore()
                  .ConfigureRunner(rb => rb
                  .AddPostgres()
                  .WithGlobalConnectionString(configuration.GetConnectionString("MemberDB"))
                  .WithMigrationsIn(Assembly.Load("Alofa.Member.Service")))
               .AddLogging(lb => lb.AddFluentMigratorConsole())
                .Configure<RunnerOptions>(opt => {
                    opt.Tags = new[] { "Alofa.Member" };
                })
               .BuildServiceProvider(false);

            using (var scope = migrationMemberSvc.CreateScope())
            {

                var runner = scope.ServiceProvider.GetRequiredService<IMigrationRunner>();
                runner.MigrateUp();
            }
        }
    }
}
