﻿using Alofa.Member.Contract.DTO;
using Alofa.Member.Service.Reads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Member.Service.Services.Query
{
    public class MemberQuery : IMemberQuery
    {
        private IProfileRead _profileRead;
        public MemberQuery(IProfileRead profileRead)
        {
            _profileRead = profileRead;
        }
        public async Task<ProfileDTO> GetMemberProfile(string username)
        {
            var result = await _profileRead.GetProfileByUsername(username);

            return new ProfileDTO
            {
                UpdatedDate = result.UpdatedDate,
                Height = result.Height,
                Gender = result.Gender,
                PreferredGender = result.PreferredGender,
                PreferredReligion = result.PreferredReligion,
                PreferredZodiac = result.PreferredZodiac,
                Zodiac = result.Zodiac
            };
        }

        public async Task<byte[]> GetProfilePhoto(string username)
        {
            var result = await _profileRead.GetProfileByUsername(username);

            return result.ProfilePhoto;
        }
    }
}
