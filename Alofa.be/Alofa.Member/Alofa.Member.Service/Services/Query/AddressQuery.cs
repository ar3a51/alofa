﻿using Alofa.Member.Contract.DTOs;
using Alofa.Member.Service.Reads;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Member.Service.Services.Query
{
    public class AddressQuery : IAddressQuery
    {
        private IAddressRead _addressRead;
        public AddressQuery(IAddressRead addressRead) {
            _addressRead = addressRead;
        }
        public async Task<AddressDTO> GetAddress(string username)
        {
            var result = await _addressRead.GetAddress(username);
            return new AddressDTO
            {
                Address1 = result.Address1,
                Address2 = result.Address2,
                Country = result.Country,
                Province = result.Province,
                Suburb = result.Suburb,
                UpdatedDate = result.UpdatedDate
            };
        }
    }
}
