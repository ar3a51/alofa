﻿using Alofa.Member.Contract.DTO;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Member.Service.Services.Query
{
    public interface IMemberQuery
    {
        Task<ProfileDTO> GetMemberProfile(string username);
        Task<byte[]> GetProfilePhoto(string username);
    }
}
