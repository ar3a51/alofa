﻿using Alofa.Member.Contract.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Member.Service.Services.Query
{
    public interface IAddressQuery
    {
        public Task<AddressDTO> GetAddress(string username);
    }
}
