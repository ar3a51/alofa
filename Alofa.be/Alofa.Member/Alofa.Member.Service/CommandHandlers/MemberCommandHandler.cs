﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Alofa.Member.Contract.Commands;
using Alofa.Member.Contract.Events;
using Alofa.Member.Service.Reads;
using Alofa.Member.Service.Writes;
using DotNetCore.CAP;
using MediatR;

namespace Alofa.Member.Service.CommandHandlers
{
    public class MemberCommandHandler: 
        IRequestHandler<CreateProfileCommand>,
        IRequestHandler<ProfileUpdateCommand>
    {
        private IProfileRespository _profileRepository;
        private IProfileRead _profileRead;
        private ICapPublisher _publisher;

        public MemberCommandHandler(
            IProfileRespository profileRespository,
            IProfileRead profileRead,
            ICapPublisher publisher)
        {
            _profileRepository = profileRespository;
            _profileRead = profileRead;
            _publisher = publisher;
        }

        public Task<Unit> Handle(CreateProfileCommand request, CancellationToken cancellationToken)
        {
            var profileCommand = request.ProfileDTO;

            var profile = new Entities.Profile
            {
                Id = profileCommand.Id,
                Gender = profileCommand.Gender,
                PreferredGender = profileCommand.PreferredGender,
                PreferredReligion = profileCommand.PreferredReligion,
                PreferredZodiac = profileCommand.PreferredZodiac,
                Zodiac = profileCommand.Zodiac,
                Username = profileCommand.Username,
                Height = profileCommand.Height,
                CreatedDate = profileCommand.CreatedDate,
                UpdatedDate = profileCommand.UpdatedDate
            };

            _profileRepository.AddNewProfile(profile);

            _publisher.Publish(nameof(ProfileCreatedEvent), new ProfileCreatedEvent
            {
                Id = profileCommand.Id,
                Gender = profileCommand.Gender,
                PreferredGender = profileCommand.PreferredGender,
                PreferredReligion = profileCommand.PreferredReligion,
                PreferredZodiac = profileCommand.PreferredZodiac,
                Zodiac = profileCommand.Zodiac,
                Username = profileCommand.Username,
                Height = profileCommand.Height,
                CreatedDate = profileCommand.CreatedDate,
                UpdatedDate = profileCommand.UpdatedDate
            });

            return Task.FromResult(Unit.Value);
        }

        public Task<Unit> Handle(ProfileUpdateCommand request, CancellationToken cancellationToken)
        {
            var newProfile = request.ProfileDTO;

            var profile = new Entities.Profile
            {
                Id = newProfile.Id,
                Gender = newProfile.Gender,
                PreferredGender = newProfile.PreferredGender,
                PreferredReligion = newProfile.PreferredReligion,
                PreferredZodiac = newProfile.PreferredZodiac,
                Zodiac = newProfile.Zodiac,
                Username = newProfile.Username,
                Height = newProfile.Height,
                UpdatedDate = newProfile.UpdatedDate
            };

            _profileRepository.UpdateProfile(profile);

            _publisher.Publish(nameof(ProfileUpdatedEvent), new ProfileUpdatedEvent
            {
                Id = newProfile.Id,
                Gender = newProfile.Gender,
                PreferredGender = newProfile.PreferredGender,
                PreferredReligion = newProfile.PreferredReligion,
                PreferredZodiac = newProfile.PreferredZodiac,
                Zodiac = newProfile.Zodiac,
                Username = newProfile.Username,
                Height = newProfile.Height,
                UpdatedDate = newProfile.UpdatedDate
            });

            return Task.FromResult(Unit.Value);
        }
    }
}
