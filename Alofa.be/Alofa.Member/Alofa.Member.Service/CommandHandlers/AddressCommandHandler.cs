﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Alofa.Member.Contract.Commands;
using Alofa.Member.Contract.Events;
using Alofa.Member.Service.Writes;
using DotNetCore.CAP;
using MediatR;

namespace Alofa.Member.Service.CommandHandlers
{
    public class AddressCommandHandler:
        IRequestHandler<CreateAddressCommand>,
        IRequestHandler<AddressUpdateCommand>
    {
        private IAddressRepository _addressRepository;
        private ICapPublisher _publisher;
        public AddressCommandHandler(
            IAddressRepository addressRepository,
            ICapPublisher publisher)
        {
            _addressRepository = addressRepository;
            _publisher = publisher;
        }

        public Task<Unit> Handle(CreateAddressCommand addressCommand, CancellationToken cancellationToken)
        {
            var addressData = addressCommand.Address;

            _addressRepository.InsertNewAddress(new Entities.Address
            {
                Address1 = addressData.Address1,
                Address2 = addressData.Address2,
                Country = addressData.Country,
                Id = addressData.Id,
                Province = addressData.Province,
                Suburb = addressData.Suburb,
                CreatedDate = addressData.CreatedDate,
                UpdatedDate = addressData.UpdatedDate,
                Username = addressCommand.Username
            });

            _publisher.Publish(nameof(AddressCreatedEvent), 
                new AddressCreatedEvent(addressData, addressCommand.Username, addressCommand.CreatedDate, addressCommand.UpdatedDate)
               );

            return Task.FromResult(Unit.Value);
        }

        public Task<Unit> Handle(AddressUpdateCommand request, CancellationToken cancellationToken)
        {
            var addressData = request.Address;

            _addressRepository.UpdateAddress(new Entities.Address
            {
                Address1 = addressData.Address1,
                Address2 = addressData.Address2,
                Country = addressData.Country,
                Id = addressData.Id,
                Province = addressData.Province,
                Suburb = addressData.Suburb,
                CreatedDate = addressData.CreatedDate,
                UpdatedDate = addressData.UpdatedDate,
                Username = request.Username
            });

            _publisher.Publish(nameof(AddressUpdatedEvent), new AddressUpdatedEvent
            (
                addressData,
                request?.Username,
                request.UpdatedDate
            ));

            return Task.FromResult(Unit.Value);
        }
    }
}
