﻿using Microsoft.AspNetCore.Routing;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Http;

using Microsoft.Extensions.DependencyInjection;

using System;
using System.IO;
using System.Text.Json;

using Alofa.Common;
using Alofa.Auth.Contract.DTOs;
using System.Dynamic;
using Alofa.Auth.Service.Services.Query;
using System.Collections.Generic;
using Alofa.Auth.Service.Services.Command;
using Alofa.Auth.Contract.Commands;
using Alofa.Web.Common;
using MediatR;

namespace Alofa.Auth.Service
{
    public static class EndpointConfiguration
    {
        public static void MapAuthorisation(this IEndpointRouteBuilder endpoint)
        {
            //endpoint.MapDaprSubscriber();
            endpoint.MapGet("/api/test", async context => {
                await context.Response.WriteAsync($"Hello {context.Request.Query["name"]} from auth");
            });

            endpoint.MapPost("/api/create", async context => {
                var data = context.Items["body"] as dynamic;

                var auth = new AuthDTO
                {
                    Username = data.username,
                    FirstName = data.firstname,
                    LastName = data.lastname,
                    EmailAddress = data.emailAddress,
                    Password = data.password,
                    Role = new List<string> { "Member"}
                };

                

                var service = context.RequestServices.GetService<IMediator>();
                await service.Send(new CreateUserCommand { AuthDto = auth, 
                    CreatedDate = DateTime.Now.ToUniversalTime(), 
                    UpdatedDate = DateTime.Now.ToUniversalTime()
                });

                await context.Response.WriteAsync("User created Successfully");
            });

            endpoint.MapPost("/api/login", async context => {
                var data = context.Items["body"] as dynamic;

                var login = new LoginDTO
                {
                    Username = data.username,
                    Password = data.password
                };

                var service = context.RequestServices.GetService<IUserAuthentication>();
                var result = await service.VerifyUser(login);

                context.Response.ContentType = "application/json";

                if (result.Result == "Failed")
                    context.Response.StatusCode = 404;
                    

                await context.Response.WriteAsync(JsonSerializer.Serialize(result));
            });
        }
    }
}
