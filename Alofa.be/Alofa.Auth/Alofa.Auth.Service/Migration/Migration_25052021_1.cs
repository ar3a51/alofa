﻿using FluentMigrator;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Service.Migration
{
    [Tags("Alofa.Auth")]
    [Migration(25052021_1)]
    public class Migration_25052021_1 : MigrationBase
    {
        public override void Down()
        {
            throw new NotImplementedException();
        }

        public override void Up()
        {
            Insert.IntoTable("Roles")
                   .Row(new { Id = Guid.NewGuid(), RoleName = "Member", CreatedDate = DateTime.UtcNow, UpdatedDate = DateTime.UtcNow })
                   .Row(new { Id = Guid.NewGuid(), RoleName = "Admin", CreatedDate = DateTime.UtcNow, UpdatedDate = DateTime.UtcNow });
        }
    }
}
