﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using FluentMigrator;

namespace Alofa.Auth.Service.Migration
{
    [Tags("Alofa.Auth")]
    [Migration(25052020)]
    public class Migration_25052020_CreateTables : MigrationBase
    {
        public override void Down()
        {
            throw new NotImplementedException();
        }

        public override void Up()
        {
            Create.Table("User")
                .WithColumn("Id").AsGuid().NotNullable().PrimaryKey()
                .WithColumn("Username").AsString().NotNullable()
                .WithColumn("Password").AsString().NotNullable()
                .WithColumn("FirstName").AsString().NotNullable()
                 .WithColumn("LastName").AsString().NotNullable()
                  .WithColumn("EmailAddress").AsString().NotNullable()
                .WithColumn("CreatedDate").AsDateTime().NotNullable()
                .WithColumn("UpdatedDate").AsDateTime().NotNullable();

            Create.Table("Roles")
                .WithColumn("Id").AsGuid().NotNullable().PrimaryKey()
                .WithColumn("RoleName").AsString()
                .WithColumn("CreatedDate").AsDateTime()
                .WithColumn("UpdatedDate").AsDateTime();

            Create.Table("UserRoles")
                .WithColumn("Id").AsGuid().PrimaryKey()
                .WithColumn("UserId").AsGuid().NotNullable().ForeignKey("User", "Id")
                .WithColumn("RoleId").AsGuid().NotNullable().ForeignKey("Roles", "Id")
                .WithColumn("CreatedDate").AsDateTime()
                .WithColumn("UpdatedDate").AsDateTime();    
        }
    }
}
