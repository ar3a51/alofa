﻿using Microsoft.AspNetCore.Http;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Service.Middleware
{
   public class JwtTokenMiddleware
    {
        private readonly RequestDelegate _next;
        
        public JwtTokenMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            var token = context.Request.Headers["Authorization"].FirstOrDefault()?.Split(" ").Last();

            if (token != null)
            {
                Validate(token, context);
            }

            await _next(context);
        }

        private void Validate(string token, HttpContext context)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            try { 
            var key = Encoding.ASCII.GetBytes("RWE%^EWRGSEQAF{%}^$%#%$");
            var claims = tokenHandler.ValidateToken(token, new Microsoft.IdentityModel.Tokens.TokenValidationParameters
            {
                ValidateIssuerSigningKey = true,
                IssuerSigningKey = new SymmetricSecurityKey(key),
                ValidateIssuer = false,
                ValidateAudience = false,
                ClockSkew = TimeSpan.Zero,
                ValidateLifetime = true
                //ValidIssuer = "http://localhost:5000"


            }, out SecurityToken validatedToken);
            
            context.User = claims;
            } 
            catch(Exception ex)
            {
               
            }


        }
    }
}
