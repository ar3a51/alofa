﻿using Alofa.Auth.Contract.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Service.Services.Query
{
    public interface IUserQueryService
    {
        Task<UserDetailsDTO> GetUserDetailsAsync(string username);
    }
}
