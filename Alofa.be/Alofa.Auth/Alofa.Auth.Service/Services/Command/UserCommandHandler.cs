﻿using Alofa.Auth.Contract.DTOs;
using Alofa.Auth.Contract.Domains;
using Alofa.Auth.Service.Repositories.Read;
using Alofa.Auth.Service.Repositories.Write;
using Alofa.Auth.Service.Entities;
using Alofa.Web.Common.Services;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using BCrypt;
using DotNetCore.CAP;
using Alofa.Auth.Contract.Commands;
using Alofa.Auth.Contract.Events;
using MediatR;
using System.Threading;


namespace Alofa.Auth.Service.Services.Command
{
    public class UserCommandHandler : 
        IRequestHandler<CreateUserCommand, bool>,
        IRequestHandler<UpdateUserEmailCommand>,
        IRequestHandler<UpdateFirstNameCommand>,
        IRequestHandler<UpdateLastNameCommand>,
        IRequestHandler<UpdatePasswordCommand>
    {
        private IUnitOfWork _userUnitOfWork;
        private IRoleQueryRepo _roleRepo;
        private IUserQueryRepo _userQueryRepo;
        private DaprService _daprClient;
        public UserCommandHandler(
            IUnitOfWork unitOfWork, 
            IUserQueryRepo userQueryRepo, 
            IRoleQueryRepo roleQueryRepo, 
            DaprService daprClient)
        {
            _userUnitOfWork = unitOfWork;
            _roleRepo = roleQueryRepo;
            _userQueryRepo = userQueryRepo;
            _daprClient = daprClient;
           
        }
        public async Task<bool> Handle(CreateUserCommand userCommand, CancellationToken cancellation)
        {
            var existingUser = await _userQueryRepo.GetUserByUsernameAsync(userCommand.AuthDto.Username);
            if (existingUser != null) throw new ArgumentException("User already exists");

            var newUser = Authentication.Create(userCommand.AuthDto);
            var roles = _roleRepo.GetRolesByName(userCommand.AuthDto.Role);

            foreach (var role in roles)
            {
                newUser.AddRole(role);
            }

            await _userUnitOfWork.userRepo.AddUserAsync(newUser.ToEntity());

            await _userUnitOfWork.userRepo.AddUserRolesAsync(newUser.Id,
                newUser.Roles.Select(r => r.RoleId).ToList());

           

            AuthCreatedEvent authCreated = new AuthCreatedEvent
            {
                EmailAddress = newUser.User.EmailAddress,
                Username = newUser.User.UserName,
                FirstName = newUser.User.FirstName,
                LastName = newUser.User.LastName
            };

            //await _capPublisher.PublishAsync(nameof(AuthCreatedEvent), authCreated);
            await _daprClient.Publish<AuthCreatedEvent>(
                "AlofaPubSub", 
                nameof(AuthCreatedEvent), authCreated);

            await _userUnitOfWork.SaveAsync();

            return true;

        }

        public async Task<Unit> Handle(UpdateUserEmailCommand command, CancellationToken cancellationToken)
        {
            await _userUnitOfWork.userRepo.UpdateEmailAsync(command.Username, command.Email);
            //await _capPublisher.PublishAsync(nameof(AuthEmailUpdatedEvent), new AuthEmailUpdatedEvent(command.Username, command.Email, command.UpdatedDate));
            
            await _daprClient.Publish<AuthEmailUpdatedEvent>(
               "AlofaPubSub",
               nameof(AuthEmailUpdatedEvent), new AuthEmailUpdatedEvent(command.Username, command.Email, command.UpdatedDate));
            
            await _userUnitOfWork.SaveAsync();
            return Unit.Value;
            
        }

        public async Task<Unit> Handle(UpdateFirstNameCommand request, CancellationToken cancellationToken)
        {
            await _userUnitOfWork.userRepo.UpdateFirstNameAsync(request.Username, request.FirstName);
            //await _capPublisher.PublishAsync(nameof(AuthFirstNameUpdatedEvent), new AuthFirstNameUpdatedEvent(request.Username, request.FirstName, request.UpdatedDate));
            await _daprClient.Publish<AuthFirstNameUpdatedEvent>(
              "AlofaPubSub",
              nameof(AuthFirstNameUpdatedEvent), new AuthFirstNameUpdatedEvent(request.Username, request.FirstName, request.UpdatedDate));

            await _userUnitOfWork.SaveAsync();
            return Unit.Value;
        }

        public async Task<Unit> Handle(UpdateLastNameCommand request, CancellationToken cancellationToken)
        {
            await _userUnitOfWork.userRepo.UpdateLastNameAsync(request.Username, request.LastName);
            //await _capPublisher.PublishAsync(nameof(AuthLastNameUpdatedEvent), new AuthLastNameUpdatedEvent(request.Username, request.LastName, request.UpdatedDate));
            await _daprClient.Publish<AuthLastNameUpdatedEvent>(
             "AlofaPubSub",
             nameof(AuthFirstNameUpdatedEvent), new AuthLastNameUpdatedEvent(request.Username, request.LastName, request.UpdatedDate));

            await _userUnitOfWork.SaveAsync();
            return Unit.Value;
        }

        public async Task<Unit> Handle(UpdatePasswordCommand request, CancellationToken cancellationToken)
        {
            var hashedPassword = BCrypt.Net.BCrypt.HashPassword(request.Password);
            await _userUnitOfWork.userRepo.UpdatePasswordAsync(request.Username, hashedPassword);
            //await _capPublisher.PublishAsync(nameof(AuthPasswordUpdatedEvent), new AuthPasswordUpdatedEvent(request.Username, hashedPassword, request.UpdatedDate));
            await _daprClient.Publish<AuthPasswordUpdatedEvent>(
            "AlofaPubSub",
            nameof(AuthPasswordUpdatedEvent), new AuthPasswordUpdatedEvent(request.Username, hashedPassword, request.UpdatedDate));

            await _userUnitOfWork.SaveAsync();
            return Unit.Value;
        }
    }
}
