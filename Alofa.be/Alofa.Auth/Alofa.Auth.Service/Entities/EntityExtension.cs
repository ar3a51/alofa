﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alofa.Auth.Contract.Domains;

namespace Alofa.Auth.Service.Entities
{
    public static class UserDomainExtension
    {
        public static Entities.User ToEntity(this Contract.Domains.Authentication auth)
        {
            return new Entities.User
            {
                Id = auth.Id,
                EmailAddress = auth.User.EmailAddress,
                FirstName = auth.User.FirstName,
                LastName = auth.User.LastName,
                Password = auth.User.Password,
                Username = auth.User.UserName
            };
        }
    }

    public static class RoleDomainExtension
    {
        public static Entities.Role ToEntity(this Contract.Domains.Role role)
        {
            return new Entities.Role
            {
                Id = role.RoleId,
                RoleName = role.RoleName
            };
        }
    }
}
