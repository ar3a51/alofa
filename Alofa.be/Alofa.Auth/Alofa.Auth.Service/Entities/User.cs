﻿using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Service.Entities
{
    [Table(Name ="User")]
    public class User
    {
        [Column(Name ="Id", IsPrimaryKey = true, DataType = LinqToDB.DataType.Guid)]
        public Guid Id { set; get; }

        [Column(Name = "Username", DataType = LinqToDB.DataType.VarChar)]
        public string Username { set; get; }

        [Column(Name = "Password", DataType = LinqToDB.DataType.VarChar)]
        public string Password { set; get; }

        [Column(Name = "FirstName", DataType = LinqToDB.DataType.VarChar)]
        public string FirstName { set; get; }

        [Column(Name = "LastName", DataType = LinqToDB.DataType.VarChar)]
        public string LastName { set; get; }

        [Column(Name = "EmailAddress", DataType = LinqToDB.DataType.VarChar)]
        public string EmailAddress { set; get; }

        [Column(Name = "CreatedDate", DataType = LinqToDB.DataType.DateTime)]
        public DateTime CreatedDate { set; get; }

        [Column(Name = "UpdatedDate", DataType = LinqToDB.DataType.DateTime)]
        public DateTime UpdatedDate { set; get; }
    }
}
