﻿using LinqToDB;
using LinqToDB.Configuration;
using LinqToDB.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Service.Entities
{
    public class AuthDbConnection : DataConnection
    {
        public AuthDbConnection(LinqToDbConnectionOptions options):base(options)
        {

        }

        public ITable<User> User => GetTable<User>();
        public ITable<Role> Role => GetTable<Role>();
        public ITable<UserRole> UserRole => GetTable<UserRole>();


    }
}
