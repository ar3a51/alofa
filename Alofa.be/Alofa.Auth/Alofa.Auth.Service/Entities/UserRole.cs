﻿using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Service.Entities
{
    [Table(Name ="UserRoles")]
   public class UserRole
    {
        [Column(Name ="Id", DataType = LinqToDB.DataType.Guid)]
        public Guid Id { set; get; }

        [Column(Name ="UserId")]
        public Guid UserId { set; get; }
        
        [Column(Name = "RoleId")]
        public Guid RoleId { set; get; }

        [Column(Name = "CreatedDate")]
        public DateTime CreatedDate { set; get; }

        [Column(Name = "UpdatedDate")]
        public DateTime UpdatedDate { set; get; }
    }
}
