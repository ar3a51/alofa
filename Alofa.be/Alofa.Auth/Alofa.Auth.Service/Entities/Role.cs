﻿using LinqToDB.Mapping;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Service.Entities
{
    [Table(Name ="Roles")]
    public class Role
    {
        [Column(Name ="Id", IsPrimaryKey = true, DataType = LinqToDB.DataType.Guid)]
        public Guid Id { set; get; }

        [Column(Name = "RoleName", DataType = LinqToDB.DataType.VarChar)]
        public string RoleName { set; get; }

        [Column(Name = "CreatedDate", DataType = LinqToDB.DataType.DateTime)]
        public DateTime CreatedDate { set; get; }

        [Column(Name = "UpdatedDate", DataType = LinqToDB.DataType.DateTime)]
        public DateTime UpdatedDate { set; get; }
    }
}
