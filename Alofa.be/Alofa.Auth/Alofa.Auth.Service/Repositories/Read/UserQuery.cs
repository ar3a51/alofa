﻿using Alofa.Auth.Contract.Domains;
using Alofa.Auth.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToDB;

namespace Alofa.Auth.Service.Repositories.Read
{
    public class UserQuery : IUserQueryRepo
    {
        private AuthDbConnection _connection;
        public UserQuery(AuthDbConnection connection)
        {
            _connection = connection;
        }
        public async Task<Auth.Contract.Domains.User> GetUserByUsernameAsync(string username)
        {
            //LinqToDB.Common.Configuration.Linq.GenerateExpressionTest = true;
            var user = await _connection.User.SingleOrDefaultAsync(user => user.Username.ToLower().Equals(username.ToLower()));
            //LinqToDB.Common.Configuration.Linq.GenerateExpressionTest = false;
            if (user != null)
                return new Auth.Contract.Domains.User(user.Username, user.Password, user.FirstName, user.LastName, user.EmailAddress);
            else
                return null;

        }

        public async Task<IEnumerable<Auth.Contract.Domains.Role>> GetUserRolesAsync(string username)
        {
            var result = from u in _connection.User
                         join ur in _connection.UserRole on u.Id equals ur.UserId
                         join r in _connection.Role on ur.RoleId equals r.Id
                         where u.Username.Equals(username)
                         select r;

            return await result.Select(r => new Contract.Domains.Role(r.Id, r.RoleName)).ToListAsync();
        }
    }
}
