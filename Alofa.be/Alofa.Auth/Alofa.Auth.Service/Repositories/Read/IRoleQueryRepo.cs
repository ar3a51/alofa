﻿using Alofa.Auth.Contract.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Service.Repositories.Read
{
    public interface IRoleQueryRepo
    {
        Role GetRole(Guid RoleId);
        Role GetRoleByName(string name);

        IEnumerable<Role> GetRolesByName(IEnumerable<string> roleNames);
    }
}
