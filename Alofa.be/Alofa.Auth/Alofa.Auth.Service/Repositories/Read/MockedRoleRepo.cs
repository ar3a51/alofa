﻿using Alofa.Auth.Contract.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Service.Repositories.Read
{
    class MockedRoleRepo : IRoleQueryRepo
    {
        public Role GetRole(Guid RoleId)
        {
            throw new NotImplementedException();
        }

        public Role GetRoleByName(string name)
        {
            throw new NotImplementedException();
        }

        public IEnumerable<Role> GetRolesByName(IEnumerable<string> roleNames)
        {
           return new List<Role>{ new Role(Guid.NewGuid(),"member")};
        }
    }
}
