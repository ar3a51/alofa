﻿using Alofa.Auth.Contract.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Service.Repositories.Read
{
   public interface IUserQueryRepo
    {
        Task<User> GetUserByUsernameAsync(string username);
        Task<IEnumerable<Role>> GetUserRolesAsync(string username);
    }
}
