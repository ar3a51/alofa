﻿using Alofa.Auth.Contract.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToDB;
using Alofa.Auth.Service.Entities;
using Role = Alofa.Auth.Contract.Domains.Role;

namespace Alofa.Auth.Service.Repositories.Read
{
    public class RoleQuery : IRoleQueryRepo
    {
        private AuthDbConnection _connection;
        public RoleQuery(AuthDbConnection connection)
        {
            _connection = connection;
        }
        public Role GetRole(Guid RoleId)
        {
            return _connection.Role.Where(r => r.Id == RoleId)
                            .Select(r => new Role(r.Id, r.RoleName)).FirstOrDefault();
        }

        public Role GetRoleByName(string name)
        {
            return _connection.Role.Where(r => r.RoleName.Equals(name,StringComparison.OrdinalIgnoreCase))
                            .Select(r => new Role(r.Id, r.RoleName)).FirstOrDefault();
        }

        public IEnumerable<Role> GetRolesByName(IEnumerable<string> roleNames)
        {
            return _connection.Role.Where(r => roleNames.Contains(r.RoleName))
                .Select(r => new Role(r.Id, r.RoleName));
        }
    }
}
