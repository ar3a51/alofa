﻿using Alofa.Auth.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToDB;


namespace Alofa.Auth.Service.Repositories.Write
{
    public class UserWrite 
    {
        private AuthDbConnection _connection;
        public UserWrite(AuthDbConnection connection)
        {
            _connection = connection;
        }
        public Task AddUserAsync(Entities.User userEntity)
        {
            _connection.Insert<Entities.User>(userEntity);
            return Task.CompletedTask;
        }

        public Task AddUserRolesAsync(Guid userId, List<Guid> roleIds)
        {
           foreach(var roleId in roleIds)
            {
               _connection.UserRole
                        .Value(ur => ur.Id, Guid.NewGuid())
                        .Value(ur => ur.UserId, userId)
                        .Value(ur => ur.RoleId, roleId)
                        .Value(ur => ur.CreatedDate, DateTime.Now.ToUniversalTime())
                        .Value(ur => ur.UpdatedDate, DateTime.Now.ToUniversalTime())
                        .Insert();
            }

            return Task.CompletedTask;

           
        }

        public Task UpdateEmailAsync(string userName, string email)
        {
            _connection.User
                     .Where(u => u.Username.Equals(userName, StringComparison.OrdinalIgnoreCase))
                     .Set(u => u.EmailAddress, email)
                     .Set(u => u.UpdatedDate, DateTime.Now.ToUniversalTime())
                     .Update();

            return Task.CompletedTask;
        }

        public Task UpdateFirstNameAsync(string userName, string newFirstName)
        {
            _connection.User
                    .Where(u => u.Username.Equals(userName, StringComparison.OrdinalIgnoreCase))
                    .Set(u => u.FirstName, newFirstName)
                    .Set(u => u.UpdatedDate, DateTime.Now.ToUniversalTime())
                    .Update();

            return Task.CompletedTask;
        }

        public Task UpdateLastNameAsync(string userName, string newLastName)
        {
            _connection.User
                   .Where(u => u.Username.Equals(userName, StringComparison.OrdinalIgnoreCase))
                   .Set(u => u.LastName, newLastName)
                    .Set(u => u.UpdatedDate, DateTime.Now.ToUniversalTime())
                   .Update();

            return Task.CompletedTask;
        }

        public Task UpdatePasswordAsync(string userName, string newPassword)
        {
            _connection.User
                    .Where(u => u.Username.Equals(userName, StringComparison.OrdinalIgnoreCase))
                    .Set(u => u.Password, newPassword)
                     .Set(u => u.UpdatedDate, DateTime.Now.ToUniversalTime())
                    .Update();

            return Task.CompletedTask;
        }
    }
}
