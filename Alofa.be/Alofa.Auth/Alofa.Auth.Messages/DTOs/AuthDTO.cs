﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Contract.DTOs
{
    public class AuthDTO
    {
        public string Username { set; get; }
        public string Password { set; get; }
        public IEnumerable<string> Role { set; get; }
        public string FirstName { set; get; }
        public string LastName { set; get; }
        public string EmailAddress { set; get; }


    }
}
