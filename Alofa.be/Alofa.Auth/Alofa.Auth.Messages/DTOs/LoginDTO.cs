﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Contract.DTOs
{
    public class LoginDTO
    {
        public string Username { set; get; }
        public string Password { set; get; }
    }
}
