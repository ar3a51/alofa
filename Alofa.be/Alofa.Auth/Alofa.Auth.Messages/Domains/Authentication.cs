﻿using Alofa.Auth.Contract.DTOs;
using System;
using System.Collections.Generic;
using BCrypt.Net;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Contract.Domains
{
    public class Authentication
    {
        public Guid Id { private set; get; }
        public User User { private set; get; }
        public List<Role> Roles { private set; get; }

        public Authentication() {
            this.Roles = new List<Role>();
        }
        public static Authentication Create(AuthDTO auth)
        {
            if (auth == null)
            {
                throw new ArgumentNullException($"{nameof(auth)} is empty");
            }

            var authObject = new Authentication
            {
                User = User.Create(auth)
                
            };

            authObject.Id = Guid.NewGuid();

            return authObject;
        }

        public void AddRole(Role role)
        {
            this.Roles.Add(role);
        }
    }
}
