﻿using Alofa.Auth.Contract.DTOs;
using BCrypt.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Contract.Domains
{
    public class User
    {
        public string UserName { private set; get; }
        public string Password { private set; get; }
        public string FirstName { private set; get; }
        public string LastName { private set; get; }
        public string EmailAddress { private set; get; }

        public User(string username, string password, string firstName, string lastName, string emailAddress)
        {
            UserName = username;
            Password = password;
            FirstName = firstName;
            LastName = lastName;
            EmailAddress = emailAddress;
        }
        public User()
        { }
        public static User Create ( AuthDTO auth) 
        {
            return new User
            {
                UserName = auth.Username,
                Password = BCrypt.Net.BCrypt.HashPassword(auth.Password),
                FirstName = auth.FirstName,
                LastName = auth.LastName,
                EmailAddress = auth.EmailAddress
            };    
        }

        public bool ValidatePassword(string password)
        {
            return BCrypt.Net.BCrypt.Verify(password, Password);
            //return true;
        }

    }
}
