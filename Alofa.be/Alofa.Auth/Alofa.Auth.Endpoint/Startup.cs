using Alofa.Auth.Service;
using Alofa.Web.Common;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Hosting;
using Microsoft.IdentityModel.Tokens;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using DotNetCore.CAP;
using Alofa.Web.Common.Middleware;
using MediatR;
using System.Reflection;

namespace Alofa.Auth.Endpoint
{
    public class Startup
    {
        private IConfiguration _configuration;
        public Startup(IConfiguration configuration)
        {
            _configuration = configuration;
        }
        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {
            var key = Encoding.ASCII.GetBytes(_configuration["Authentication:Secret"]);

            services.AddCors(options => {
                options.AddDefaultPolicy(policy => {
                        policy.WithOrigins("http://localhost:4200");
                        policy.AllowCredentials();
                        policy.AllowAnyHeader();
                    }
                
                );
            });
            services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(opt =>
                    {
#if DEBUG
                        opt.RequireHttpsMetadata = false;
                        opt.IncludeErrorDetails = true;
#else
                        opt.RequireHttpsMetadata = true;
                        opt.IncludeErrorDetails = true;
#endif
                        opt.Audience = "http://localhost:5000";
                        opt.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = new SymmetricSecurityKey(key),
                            ValidateIssuer = false,
                            ValidateAudience = false,
                            ClockSkew = TimeSpan.Zero,
                            ValidateLifetime = true
                            //ValidIssuer = "http://localhost:5000"
                        };
                    });

            services.AddAuthorization();

            services.AddMediatR(Assembly.Load("Alofa.Auth.Service"));

            services.AddAlofaAuth(_configuration);
           
            Console.WriteLine($"RabbitMQ: {_configuration.GetSection("RabbitMQURL").Value}");
           /* services.AddCap(c =>
            {

                c.UsePostgreSql(_configuration.GetConnectionString("CapDB"));
                c.UseRabbitMQ(_configuration.GetSection("RabbitMQURL").Value);
            });*/
           
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IWebHostEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            //app.AddCloudEvents();

            app.UseRouting();

            app.UseAuthentication();

            app.UseMiddleware<BodyParserMiddleware>();

            app.UseCors();

            app.UseAuthorization();

            app.UseEndpoints(endpoints =>
            {
                endpoints.MapAuthorisation();
            });
        }
    }
}
