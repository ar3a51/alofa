﻿using Newtonsoft.Json;
using Newtonsoft.Json.Converters;
using System;
using System.Dynamic;
using System.IO;
using System.Threading.Tasks;

namespace Alofa.Common
{
    public class RequestDeserialiser
    {
        public async static Task<dynamic> ParseAsync(Stream requestBody)
        {
            var reader = new StreamReader(requestBody);
            return JsonConvert.DeserializeObject<ExpandoObject>(await reader.ReadToEndAsync(), 
                    new ExpandoObjectConverter()
                );
        }
    }
}
