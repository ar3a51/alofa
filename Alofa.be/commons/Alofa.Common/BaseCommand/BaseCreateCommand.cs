﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Common.BaseCommand
{
    public class BaseCreateCommand
    {
        [Required]
        public DateTime CreatedDate { set; get; }

        [Required]
        public DateTime UpdatedDate { set; get; }
    }
}
