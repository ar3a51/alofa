﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Common.BaseEvent
{
    public record BaseCreateEvent(DateTime CreateDate, DateTime UpdatedDate);
    public record BaseUpdateEvent(DateTime UpdateDate);
}
