﻿using Alofa.Common;
using Microsoft.AspNetCore.Http;
using System;
using System.Collections.Generic;
using System.Dynamic;
using System.Linq;
using System.Threading.Tasks;

namespace Alofa.Web.Common.Middleware
{
    public class BodyParserMiddleware
    {
        private RequestDelegate _next;
        public BodyParserMiddleware(RequestDelegate next)
        {
            _next = next;
        }

        public async Task Invoke(HttpContext context)
        {
            //context.Request.
            if (!context.Request.Method.Equals("GET"))
            {
               dynamic result = await RequestDeserialiser.ParseAsync(context.Request.Body);
                context.Items["body"] = result;
            }

            //await context.Response.WriteAsync("hitting middleware");
           await _next(context);
        }
    }
}
