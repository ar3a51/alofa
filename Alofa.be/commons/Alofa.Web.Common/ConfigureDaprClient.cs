﻿using Microsoft.Extensions.DependencyInjection;
using Dapr;
using Dapr.Client;
using Dapr.AspNetCore;
using Alofa.Web.Common.Services;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;

namespace Alofa.Web.Common
{
    public static class ConfigureDaprClient
    {
        public static void ConfigureDapr(this IServiceCollection service)
        {
            service.AddDaprClient();

            service.AddScoped<DaprService>((d) =>
                new DaprService(d.GetRequiredService<DaprClient>())
            );
        }

        public static void ConfigureCloudEvent(this IApplicationBuilder app)
        {
            app.UseCloudEvents();
        }
    }
}
