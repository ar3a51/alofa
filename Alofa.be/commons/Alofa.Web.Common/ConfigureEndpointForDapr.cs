﻿using Microsoft.AspNetCore.Routing;
using Dapr.AspNetCore;
using Microsoft.AspNetCore.Builder;

namespace Alofa.Web.Common
{
    public static class ConfigureEndpointForDapr
    {
        public static void MapDaprSubscriber(this IEndpointRouteBuilder endpoint)
        {
            endpoint.MapSubscribeHandler();
        }
    }
}
