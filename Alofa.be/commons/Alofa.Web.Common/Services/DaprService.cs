﻿using Dapr;
using Dapr.Client;
using System.Threading.Tasks;

namespace Alofa.Web.Common.Services
{
    public class DaprService
    {
        private DaprClient _client;
        public DaprService(DaprClient daprClient)
        {
            _client = daprClient;
        }

        public Task Publish<T>(string pubsubName, string topic, T data)
        {
            return _client.PublishEventAsync<T>(pubsubName, topic, data);
        }

        public Task InvokeService<TResponse>(string appId, string methodName)
        {
            return _client.InvokeMethodAsync<TResponse>(appId, methodName, new System.Threading.CancellationToken());
        }
    }
}
