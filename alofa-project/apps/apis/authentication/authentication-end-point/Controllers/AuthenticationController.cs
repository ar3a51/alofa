using Microsoft.AspNetCore.Mvc;
using Alofa.Auth.Service;
using Alofa.Auth.Service.Services.Query;
using Alofa.Auth.Contract.DTOs;
using System.Text.Json;

namespace Alofa.Apis.Authentication.AuthenticationEndPoint.Controllers;

[ApiController]
[Route("api/[controller]")]
public class AuthenticationController : ControllerBase
{
    private static readonly string[] Summaries = new[]
    {
        "Freezing", "Bracing", "Chilly", "Cool", "Mild", "Warm", "Balmy", "Hot", "Sweltering", "Scorching"
    };

    private readonly IUserAuthentication _userAuth;

    public AuthenticationController(IUserAuthentication userAuth)
    {
        this._userAuth = userAuth;
    }

    [HttpPost]
    [Route("login")]
    public async Task<IActionResult> Login(LoginDTO login){
        var result = await this._userAuth.VerifyUser(login);

        if (result.Result == "Failed")
        return NotFound(new { Message= "Invalid username or password"});

        return Ok(JsonSerializer.Serialize(result));
    }

    [HttpGet]
    public IActionResult Test(){
        return Ok("Works");
    }
}
