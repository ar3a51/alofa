using Microsoft.AspNetCore.Mvc;
using Alofa.Auth.Service;
using Alofa.Auth.Service.Services.Query;
using Alofa.Auth.Contract.DTOs;
using System.Text.Json;
using Alofa.Auth.Service.Services.Command;
using Microsoft.AspNetCore.Authorization;

namespace Alofa.Apis.Authentication.AuthenticationEndPoint.Controllers;

[ApiController]
[Route("api/[controller]")]
public class RegistrationController : ControllerBase
{
   
    private readonly IUserAuthentication _userAuth;
    private readonly IUserAdminService _userAdmin;

    public RegistrationController(
        IUserAuthentication userAuth,
        IUserAdminService userAdmin
        )
    {
        this._userAuth = userAuth;
        this._userAdmin = userAdmin;
    }


    [HttpPost]
    [Route("register")]
    public async Task<IActionResult> RegisterNewLogin(LoginDTO login){

        var auth = new AuthDTO
        {
            EmailAddress = login.EmailAddress,
            Password = login.Password,
            Role = new List<string> { "Member"}
        };

        await this._userAdmin.RegisterNewUser(auth);
        return Ok();

    }

    [Authorize]
    [HttpPost]
    [Route("updatePassword")]
    public async Task<IActionResult> UpdatePassword(LoginDTO login){


        await this._userAdmin.UpdatePassword(login.EmailAddress, login.Password);
        return Ok();

    }

}
