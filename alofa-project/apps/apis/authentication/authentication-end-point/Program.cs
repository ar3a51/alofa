using System.Text;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using Alofa.Auth.Service;
//using Mediatr;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.
Console.WriteLine($"Connecting to DB {builder.Configuration.GetConnectionString("AuthDB")}");
builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddCors(options => {
    options.AddDefaultPolicy(policy => {
            policy.WithOrigins("http://localhost:4200");
            policy.AllowCredentials();
            policy.AllowAnyHeader();
        }
    
    );
});

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
                    .AddJwtBearer(opt =>
                    {
                        var key = Encoding.ASCII.GetBytes(builder.Configuration["Authentication:Secret"]);
#if DEBUG
                        opt.RequireHttpsMetadata = false;
                        opt.IncludeErrorDetails = true;
#else
                        opt.RequireHttpsMetadata = true;
                        opt.IncludeErrorDetails = true;
#endif
                        opt.Audience = "http://localhost:5000";
                        opt.TokenValidationParameters = new TokenValidationParameters
                        {
                            ValidateIssuerSigningKey = true,
                            IssuerSigningKey = new SymmetricSecurityKey(key),
                            ValidateIssuer = false,
                            ValidateAudience = false,
                            ClockSkew = TimeSpan.Zero,
                            ValidateLifetime = true
                            //ValidIssuer = "http://localhost:5000"
                        };
                    });
builder.Services.AddAuthorization();
builder.Services.AddAlofaAuth(builder.Configuration);


var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseAuthorization();

app.MapControllers();

app.Run();