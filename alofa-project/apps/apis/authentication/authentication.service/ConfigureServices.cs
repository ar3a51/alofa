﻿using Microsoft.Extensions.Configuration;
using LinqToDB.AspNet;
using Microsoft.Extensions.DependencyInjection;
using Alofa.Auth.Service.Entities;
using LinqToDB.Configuration;
using Alofa.Auth.Service.Repositories.Write;
using Alofa.Auth.Service.Repositories.Read;

using FluentMigrator.Runner;
using System.Reflection;
using LinqToDB.AspNet.Logging;
using FluentMigrator.Runner.Initialization;
using Alofa.Auth.Service.Services.Query;
using Alofa.Auth.Service.Services.Command;

namespace Alofa.Auth.Service
{
    
  public static class ConfigureServices
    {
        
        
/*
        public static void AddCloudEvents(this WebApplication app) 
        {
            app.ConfigureCloudEvent();
        }
        
*/
        
        public static IServiceCollection AddAlofaAuth(this IServiceCollection service, IConfiguration configuration)
        {
            //service.ConfigureDapr();

            service.AddLinqToDbContext<AuthDbConnection>((provider, options) => {
                options
                    .UsePostgreSQL(configuration.GetConnectionString("AuthDB"))
                     .UseDefaultLogging(provider);
            });

            service.AddScoped<IUnitOfWork, UnitOfWork>();
            service.AddScoped<IRoleQueryRepo, RoleQuery>();
            service.AddScoped<IUserQueryRepo, UserQuery>();
            service.AddScoped<IUserQueryService, MockedUserService>();
            service.AddScoped<IUserAuthentication, UserAuthentication>();
            service.AddScoped<IUserAdminService, UserAdminService>();
            //service.AddScoped<IUserCommandService, UserCommand>();
           


           var serviceProvider =  new ServiceCollection()
                .AddFluentMigratorCore()
                    .ConfigureRunner(rb => rb
                    .AddPostgres()
                    .WithGlobalConnectionString(configuration.GetConnectionString("AuthDB"))
                     .WithMigrationsIn(Assembly.Load("Alofa.Apis.Authentication.AuthenticationService"))) 
                 .AddLogging(lb => lb.AddFluentMigratorConsole())
                 .Configure<RunnerOptions>(opt => {
                     opt.Tags = new[] { "AlofaAuth" };
                 })
                 .BuildServiceProvider(false);

            using (var scope = serviceProvider.CreateScope())
            {
                var runner = scope.ServiceProvider.GetRequiredService<IMigrationRunner>();
                runner.MigrateUp();
            }
            

            return service;
        }
        
    }
    
    
}
