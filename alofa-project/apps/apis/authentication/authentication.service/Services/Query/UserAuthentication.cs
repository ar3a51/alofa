﻿using Alofa.Auth.Contract.DTOs;
using Alofa.Auth.Service.Repositories.Read;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IdentityModel.Tokens.Jwt;
using BCrypt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;

namespace Alofa.Auth.Service.Services.Query
{
    public class UserAuthentication : IUserAuthentication
    {
        private IUserQueryRepo _userQuery;
        public UserAuthentication(IUserQueryRepo userQuery)
        {
            _userQuery = userQuery;
        }
        public async Task<AuthResultDTO> VerifyUser(LoginDTO login)
        {
           var user = await _userQuery.GetUserByUsernameAsync(login.EmailAddress);

            if (!(user is null))
            {
                var validationResult = user.ValidatePassword(login.Password);

                if (validationResult)
                {
                    var roles = await _userQuery.GetUserRolesAsync(login.EmailAddress);

                    return new AuthResultDTO
                    {
                        Token = GenerateJWTToken(user,roles),
                        Result = "Success"
                    };
                } 
            }

            return  new AuthResultDTO
            {

                Result = "Failed",
                ErrMessage = "Invalid Username or password"
            }; ;
        }

        private string GenerateJWTToken(Contract.Domains.User user, IEnumerable<Contract.Domains.Role> roles)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            var key = Encoding.ASCII.GetBytes("RWE%^EWRGSEQAF{%}^$%#%$");
            List<Claim> claims = new List<Claim>();
            claims.Add(new Claim(ClaimTypes.Email, user.EmailAddress));

            foreach(var role in roles)
            {
                claims.Add(new Claim(ClaimTypes.Role, role.RoleName));
            }

            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Issuer = "http://localhost:5000",
                Audience = "http://localhost:5000",
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddDays(5),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256Signature)
                
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
