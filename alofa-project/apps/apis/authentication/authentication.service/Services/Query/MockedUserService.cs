﻿using Alofa.Auth.Contract.DTOs;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Service.Services.Query
{
    public class MockedUserService : IUserQueryService
    {
        public Task<UserDetailsDTO> GetUserDetailsAsync(string username)
        {
            var user = new UserDetailsDTO
            {
                Email = "me@nowhere.net",
                FirstName = "jono",
                LastName = "jini",
                Username = "jonJin"
            };

            return Task.FromResult<UserDetailsDTO>(user);
        }
    }
}
