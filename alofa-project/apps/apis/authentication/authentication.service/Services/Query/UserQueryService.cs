﻿using Alofa.Auth.Contract.DTOs;
using Alofa.Auth.Service.Repositories.Read;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Service.Services.Query
{
    class UserQueryService : IUserQueryService
    {
        private IUserQueryRepo _userRepo;
        public UserQueryService(IUserQueryRepo userRepo)
        {
            _userRepo = userRepo;
        }
        public async Task<UserDetailsDTO> GetUserDetailsAsync(string username)
        {
            var result = await _userRepo.GetUserByUsernameAsync(username);
            return new UserDetailsDTO
            {
                Email = result.EmailAddress,
            };
        }
    }
}
