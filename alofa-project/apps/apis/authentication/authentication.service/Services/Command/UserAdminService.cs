using Alofa.Auth.Contract.Domains;
using Alofa.Auth.Contract.DTOs;
using Alofa.Auth.Service.Entities;
using Alofa.Auth.Service.Repositories.Read;
using Alofa.Auth.Service.Repositories.Write;

namespace Alofa.Auth.Service.Services.Command;

public class UserAdminService : IUserAdminService
{

  private readonly IUnitOfWork _unitOfWork;
  private readonly IUserQueryRepo _userQueryRepo;
  private readonly IRoleQueryRepo _roleQueryRepo;

  public UserAdminService(
      IUnitOfWork unitOfWork, 
      IUserQueryRepo userQueryRepo, 
      IRoleQueryRepo roleQueryRepo
  ){
      _unitOfWork = unitOfWork;
      _userQueryRepo = userQueryRepo;
      _roleQueryRepo = roleQueryRepo;
  }
  public async Task RegisterNewUser(AuthDTO newLoginUser)
  {
    var existingUser = await _userQueryRepo.GetUserByUsernameAsync(newLoginUser.EmailAddress);
            if (existingUser != null) throw new ArgumentException("User already exists");

            var newUser = Authentication.Create(newLoginUser);
            var roles = await _roleQueryRepo.GetRolesByName(newLoginUser.Role);

            foreach (var role in roles)
            {
                newUser.AddRole(role);
            }

            await _unitOfWork.userRepo.AddUserAsync(newUser.ToEntity());

            await _unitOfWork.userRepo.AddUserRolesAsync(newUser.Id,
                newUser.Roles.Select(r => r.RoleId).ToList());

           
            /*
            AuthCreatedEvent authCreated = new AuthCreatedEvent
            {
                EmailAddress = newUser.User.EmailAddress,
            };*/

            //await _capPublisher.PublishAsync(nameof(AuthCreatedEvent), authCreated);
          

            await _unitOfWork.SaveAsync();
  }

  public async Task UpdatePassword(string emailAddress, string newPassword)
  {
     var hashedPassword = BCrypt.Net.BCrypt.HashPassword(newPassword);
            await _unitOfWork.userRepo.UpdatePasswordAsync(emailAddress, hashedPassword);
            //await _capPublisher.PublishAsync(nameof(AuthPasswordUpdatedEvent), new AuthPasswordUpdatedEvent(request.Username, hashedPassword, request.UpdatedDate));
          /*  await _daprClient.Publish<AuthPasswordUpdatedEvent>(
            "AlofaPubSub",
            nameof(AuthPasswordUpdatedEvent), new AuthPasswordUpdatedEvent(request.Username, hashedPassword, request.UpdatedDate));
        */
            await _unitOfWork.SaveAsync();
  }
}