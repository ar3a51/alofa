using Alofa.Auth.Contract.DTOs;

namespace Alofa.Auth.Service.Services.Command;
public interface IUserAdminService {
    Task RegisterNewUser(AuthDTO newLoginUser);
    Task UpdatePassword(string emailAddress, string newPassword);
}