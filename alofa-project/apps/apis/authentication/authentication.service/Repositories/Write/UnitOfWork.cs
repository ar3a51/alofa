﻿using Alofa.Auth.Service.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Service.Repositories.Write
{
    public class UnitOfWork : IUnitOfWork
    {
        private UserWrite _userWrite;
        private RoleWrite _roleWrite;

        private AuthDbConnection _db;
        public UnitOfWork(AuthDbConnection db)
        {
            _db = db;
            _db.BeginTransaction();

            _userWrite = new UserWrite(_db);
            _roleWrite = new RoleWrite(_db);
           
        }
        public UserWrite userRepo
        {
            get
            {
                return _userWrite;
            }
        }

        public RoleWrite roleRepo
        {
            get
            {
                return _roleWrite;
            }
        }

        public Task SaveAsync()
        {
            try
            {
                _db.CommitTransaction();
            }
            catch
            {
               _db.RollbackTransaction();
                throw;
            }

            return Task.CompletedTask;
        }
    }
}
