﻿using Alofa.Auth.Contract.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToDB;
using Alofa.Auth.Service.Entities;

namespace Alofa.Auth.Service.Repositories.Write
{
    public class RoleWrite
    {
        private AuthDbConnection _db;
        public RoleWrite(AuthDbConnection db)
        {
            _db = db;
        }
        public async Task AddNewRoleAsync(Contract.Domains.Role role)
        {
           await _db.InsertAsync(role.ToEntity());
        }

        public async Task DeleteRoleAsync(Contract.Domains.Role role)
        {
            await _db.DeleteAsync(role.ToEntity());
        }
    }
}
