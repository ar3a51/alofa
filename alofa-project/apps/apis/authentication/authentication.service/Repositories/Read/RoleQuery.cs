﻿using Alofa.Auth.Contract.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using LinqToDB;
using Alofa.Auth.Service.Entities;
using Role = Alofa.Auth.Contract.Domains.Role;

namespace Alofa.Auth.Service.Repositories.Read
{
    public class RoleQuery : IRoleQueryRepo
    {
        private AuthDbConnection _connection;
        public RoleQuery(AuthDbConnection connection)
        {
            _connection = connection;
        }
        public async Task<Role?> GetRole(Guid RoleId)
        {
            return await _connection.Role.Where(r => r.Id == RoleId)
                            .Select(r => new Role(r.Id, r.RoleName)).FirstOrDefaultAsync();
        }

        public async Task<Role?> GetRoleByName(string name)
        {
            return await _connection.Role.Where(r => r.RoleName.Equals(name,StringComparison.OrdinalIgnoreCase))
                            .Select(r => new Role(r.Id, r.RoleName)).FirstOrDefaultAsync();
        }

        public async Task<IEnumerable<Role>> GetRolesByName(IEnumerable<string> roleNames)
        {
            return await _connection.Role.Where(r => roleNames.Contains(r.RoleName))
                .Select(r => new Role(r.Id, r.RoleName)).ToListAsync();
        }
    }
}
