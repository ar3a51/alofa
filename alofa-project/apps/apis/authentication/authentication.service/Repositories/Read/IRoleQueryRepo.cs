﻿using Alofa.Auth.Contract.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Service.Repositories.Read
{
    public interface IRoleQueryRepo
    {
        Task<Role?> GetRole(Guid RoleId);
        Task<Role?> GetRoleByName(string name);

        Task<IEnumerable<Role>> GetRolesByName(IEnumerable<string> roleNames);
    }
}
