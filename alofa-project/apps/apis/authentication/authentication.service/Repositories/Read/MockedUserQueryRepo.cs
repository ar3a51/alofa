﻿using Alofa.Auth.Contract.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Service.Repositories.Read
{
    public class MockedUserQueryRepo : IUserQueryRepo
    {
        public Task<User> GetUserByUsernameAsync(string username)
        {
            var user = new Contract.Domains.User("test", "test@nowhere.net");
           

            return Task.FromResult<Contract.Domains.User>(user);
        }

        public Task<IEnumerable<Role>> GetUserRolesAsync(string username)
        {
            return Task.FromResult<IEnumerable<Role>>(new List<Role> { new Role(Guid.NewGuid(), "member") });
        }
    }
}
