﻿using Alofa.Auth.Contract.Domains;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Service.Repositories.Read
{
    class MockedRoleRepo : IRoleQueryRepo
    {
        public Task<Role?> GetRole(Guid RoleId)
        {
            throw new NotImplementedException();
        }

        public Task<Role?> GetRoleByName(string name)
        {
            throw new NotImplementedException();
        }

        public async Task<IEnumerable<Role>> GetRolesByName(IEnumerable<string> roleNames)
        {
           return await Task.Run(() => new List<Role>{ new Role(Guid.NewGuid(),"member")});
        }
    }
}
