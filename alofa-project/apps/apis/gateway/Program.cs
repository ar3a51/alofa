var builder = WebApplication.CreateBuilder(args);

builder.Services.AddReverseProxy()
    .LoadFromConfig(builder.Configuration.GetSection("AuthProxy"))
    .LoadFromConfig(builder.Configuration.GetSection("ClientProxy"))
    .LoadFromConfig(builder.Configuration.GetSection("MemberProxy"))
    .LoadFromConfig(builder.Configuration.GetSection("PhotoProxy"))
    .LoadFromConfig(builder.Configuration.GetSection("MessagingProxy"))
    .LoadFromConfig(builder.Configuration.GetSection("NotificationProxy"))
    .LoadFromConfig(builder.Configuration.GetSection("FavouriteProxy"));

var app = builder.Build();

app.UseStaticFiles();

app.MapReverseProxy();

app.Run();
