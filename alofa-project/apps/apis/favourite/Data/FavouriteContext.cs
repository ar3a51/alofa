using Alofa.Apis.Favourite.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Alofa.Apis.Favourite.Data
{
  public class FavouriteContext : DbContext
  {
    public DbSet<FavouriteEntity> Favourites { set; get; }
    public FavouriteContext(DbContextOptions options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.HasDefaultSchema("Alofa.Favourite");
      //base.OnModelCreating(modelBuilder);
    }
  }
}
