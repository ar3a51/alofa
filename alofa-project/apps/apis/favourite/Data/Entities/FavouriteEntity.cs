using Microsoft.EntityFrameworkCore;

namespace Alofa.Apis.Favourite.Data.Entities;

[PrimaryKey(nameof(Id))]
public class FavouriteEntity
{
  public Guid Id { get; set; }
  public string OwnerUsername { set; get; }
  public string FavouriteUsername { set; get; }
  public DateTime DateAdded { set; get; }
}

