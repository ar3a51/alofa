using Alofa.Apis.Favourite.Models;

namespace Alofa.Apis.Favourite.Services
{
  public interface IFavouriteService
  {
    Task AddFavouriteUser(string ownerUsername, string favouriteUsername, DateTime dateAdded, CancellationToken token);
    Task<IList<FavouriteDto>> GetAllFavouriteUser(string ownerUsername, CancellationToken token);
    Task DeleteFavouriteUser(string ownerUsername, string usernameToDelete, CancellationToken token);
  }
}
