using Alofa.Apis.Favourite.Data;
using Alofa.Apis.Favourite.Data.Entities;
using Alofa.Apis.Favourite.Models;
using Microsoft.EntityFrameworkCore;

namespace Alofa.Apis.Favourite.Services
{
  public class FavouriteService : IFavouriteService
  {
    private FavouriteContext _context;
    public FavouriteService(FavouriteContext context)
    {
      _context = context;
    }

    public async Task AddFavouriteUser(string ownerUsername, string favouriteUsername, DateTime dateAdded, CancellationToken token)
    {
      var existingUser = _context.Favourites.Where(favourite => favourite.FavouriteUsername == favouriteUsername && favourite.OwnerUsername == ownerUsername).FirstOrDefault();
      if (existingUser != null) { return;}

      var favouriteEntity = new FavouriteEntity
      {
        Id = Guid.NewGuid(),
        OwnerUsername = ownerUsername,
        FavouriteUsername = favouriteUsername,
        DateAdded = dateAdded,
      };

      await _context.Favourites.AddAsync(favouriteEntity,token);
      await _context.SaveChangesAsync(token);
    }

    public async Task DeleteFavouriteUser(string ownerUsername, string usernameToDelete, CancellationToken token)
    {
      await this._context.Favourites.Where(favourite => favourite.OwnerUsername == ownerUsername
        && favourite.FavouriteUsername == usernameToDelete).ExecuteDeleteAsync(token);
    }

    public async Task<IList<FavouriteDto>> GetAllFavouriteUser(string ownerUsername, CancellationToken token)
    {
      return await _context.Favourites.Where(favourite => favourite.OwnerUsername == ownerUsername)
                .Select(favourite => new FavouriteDto
                {
                  Id = favourite.Id,
                  FavouriteUsername = favourite.FavouriteUsername,
                  DateAdded = favourite.DateAdded,
                }).ToListAsync(token);
    }
  }
}
