﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Alofa.Apis.Favourite.Migrations
{
    /// <inheritdoc />
    public partial class renameTable : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_favourites",
                schema: "Alofa.Favourite",
                table: "favourites");

            migrationBuilder.RenameTable(
                name: "favourites",
                schema: "Alofa.Favourite",
                newName: "Favourites",
                newSchema: "Alofa.Favourite");

            migrationBuilder.AddPrimaryKey(
                name: "PK_Favourites",
                schema: "Alofa.Favourite",
                table: "Favourites",
                column: "Id");
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropPrimaryKey(
                name: "PK_Favourites",
                schema: "Alofa.Favourite",
                table: "Favourites");

            migrationBuilder.RenameTable(
                name: "Favourites",
                schema: "Alofa.Favourite",
                newName: "favourites",
                newSchema: "Alofa.Favourite");

            migrationBuilder.AddPrimaryKey(
                name: "PK_favourites",
                schema: "Alofa.Favourite",
                table: "favourites",
                column: "Id");
        }
    }
}
