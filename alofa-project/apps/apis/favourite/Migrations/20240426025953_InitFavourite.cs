﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Alofa.Apis.Favourite.Migrations
{
    /// <inheritdoc />
    public partial class InitFavourite : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Alofa.Favourite");

            migrationBuilder.CreateTable(
                name: "favourites",
                schema: "Alofa.Favourite",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    OwnerUsername = table.Column<string>(type: "text", nullable: false),
                    FavouriteUsername = table.Column<string>(type: "text", nullable: false),
                    DateAdded = table.Column<DateTime>(type: "timestamp with time zone", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_favourites", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "favourites",
                schema: "Alofa.Favourite");
        }
    }
}
