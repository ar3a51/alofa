using Alofa.Apis.Favourite.Models;
using Alofa.Apis.Favourite.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Alofa.Apis.Favourite.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  [Authorize]
  public class FavouriteController : ControllerBase
  {
    private IFavouriteService _favouriteService;
    public FavouriteController(IFavouriteService favouriteService) {
      _favouriteService = favouriteService;
    }
    [HttpGet("healthcheck")]
    [AllowAnonymous]
    public IActionResult HealthCheck()
    {
      return Ok("Favourite end point works");
    }

    [HttpPost]
    public async Task<IActionResult> SaveFavourite([FromBody] FavouriteDto favouriteDto, CancellationToken token)
    {
      var username = User.Claims.FirstOrDefault(c => c.Type == "username")?.Value;
      await _favouriteService.AddFavouriteUser(username, favouriteDto.FavouriteUsername, favouriteDto.DateAdded, token);
      return Ok();
    }

    [HttpGet("retrieveall")]
    public async Task<IActionResult> GetAllFavourite(CancellationToken token)
    {
      var username = User.Claims.FirstOrDefault(c => c.Type == "username")?.Value;
      var result = await _favouriteService.GetAllFavouriteUser(username, token);
      return Ok(result);
    }

    [HttpPost("delete/{favoriteUsername}")]
    public async Task<IActionResult> DeleteFavourite(string favoriteUsername, CancellationToken token)
    {
      var username = User.Claims.FirstOrDefault(c => c.Type == "username")?.Value;
      await _favouriteService.DeleteFavouriteUser(username, favoriteUsername, token);
      return Ok();
    }
  }
}
