namespace Alofa.Apis.Favourite.Models
{
  public class FavouriteDto
  {
    public Guid Id { get; set; }
    public string FavouriteUsername { set; get; }
    public DateTime DateAdded { set; get; }
  }
}
