using Alofa.Apis.Favourite.Data;
using Alofa.Apis.Favourite.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;

var builder = WebApplication.CreateBuilder(args);

builder.Services.AddCognitoIdentity();
builder.Services.AddAuthentication(options =>
{
  options.DefaultAuthenticateScheme = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme;
  options.DefaultChallengeScheme = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme;
})
.AddJwtBearer(options =>
{
  options.Authority = builder.Configuration["Cognito:Authority"];
  options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
  {
    ValidateIssuerSigningKey = true,
    ValidateAudience = false,
    ValidateLifetime = true,
  };
});

Console.WriteLine($"Favorite service connecting to DB: {builder.Configuration.GetConnectionString("AlofaDB")}");
// Add services to the container.
builder.Services.AddDbContext<FavouriteContext>(context => context.UseNpgsql(
  builder.Configuration.GetConnectionString("AlofaDB"),
  option => option.MigrationsHistoryTable(
   tableName: HistoryRepository.DefaultTableName,
   schema: "Alofa.Favourite"
  )));

builder.Services.AddScoped<IFavouriteService, FavouriteService>();

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
