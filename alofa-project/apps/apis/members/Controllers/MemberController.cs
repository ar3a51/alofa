using Microsoft.AspNetCore.Mvc;
using member.contract.Dtos;
using Microsoft.AspNetCore.Authorization;

namespace Alofa.Apis.Members.Controllers
{
  [Route("api/[controller]")]
    [ApiController]
    [Authorize]
    public class MemberController : ControllerBase
    {

       
        public MemberController(){
         
        }

        [HttpPost]
        [Route("saveprofile")]
        public IActionResult SaveProfile(GroupField[] groupProfiles) {
          var result = User.Claims.FirstOrDefault(c => c.Type == "username")?.Value;
            return Ok(groupProfiles);
        }

        [HttpGet()]
        [Route("test")]
        public IActionResult Test() {
            return Ok("Hello World");
        }
    }
}
