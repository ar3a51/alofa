using Alofa.Apis.Photos.AlofaPhotos.Models;

namespace Alofa.Apis.Photos.AlofaPhotos.Services
{
  public interface IPhotoService
  {
    Task SavePhoto(
      string userName,
      bool isPrimary,
      IFormFile photoFile,
      DateTime dateTime,
      CancellationToken cancellationToken);

    Task<GetPhotoDto?> GetPhotoProfile(string username, CancellationToken cancellationToken);

    Task UploadPhotoProfile(string username, IFormFile photoProfile, DateTime dateUpdated, CancellationToken cancellationToken);

  }
}
