using Alofa.Apis.Photos.AlofaPhotos.Models;

namespace Alofa.Apis.Photos.AlofaPhotos.Services
{
  public interface IS3BucketService
  {
    Task StorePhoto(IFormFile file, string path, CancellationToken token);
    Task<GetPhotoDto> GetPhoto(string path, CancellationToken cancellationToken);

    Task DeletePhoto(string path, CancellationToken token);
  }
}
