using Alofa.Apis.Photos.AlofaPhotos.Data;
using Alofa.Apis.Photos.AlofaPhotos.Data.Entities;
using Alofa.Apis.Photos.AlofaPhotos.Models;
using Microsoft.EntityFrameworkCore;
using System;
using System.Reflection.Metadata.Ecma335;

namespace Alofa.Apis.Photos.AlofaPhotos.Services
{
  public class PhotoService : IPhotoService
  {
    private PhotoContext _photoContext;
    private IS3BucketService _bucketService;
    public PhotoService(PhotoContext photoContext, IS3BucketService bucketService)
    {
      _photoContext = photoContext;
      _bucketService = bucketService;
    }

    public async Task SavePhoto(
      string userName,
      bool isPrimary,
      IFormFile photoFile,
      DateTime dateTime,
      CancellationToken cancellationToken)
    {
      
      var photoPath = $"{userName}/profilephotos";
      await _bucketService.StorePhoto(photoFile, photoPath, cancellationToken);

      var photoEntity = new Photo
      {
        Id = Guid.NewGuid(),
        Username = userName,
        IsPrimary = isPrimary,
        PhotoUrl = $"{photoPath}/{photoFile.FileName}",
        DateUpdated = dateTime
      };

      _photoContext.Photos.Add(photoEntity);
      await _photoContext.SaveChangesAsync(cancellationToken);
    }

    public async Task<GetPhotoDto?> GetPhotoProfile(string username, CancellationToken cancellationToken)
    {
      //var key = $"{username}/profilephotos";
      var photoProfileUrl = this._photoContext.Photos.Where(photo => photo.Username == username && photo.IsPrimary == true)
          .Select(photo => photo.PhotoUrl).AsNoTracking()
          .FirstOrDefault();
      if (String.IsNullOrEmpty(photoProfileUrl)) return null;

      return await _bucketService.GetPhoto(photoProfileUrl, cancellationToken);
    }

    public async Task UploadPhotoProfile(string username, IFormFile photoProfile, DateTime dateUpdated, CancellationToken cancellationToken)
    {
      var photoPath = $"{username}/profilephotos";

      var existingPhoto = _photoContext.Photos.Where(photo => photo.Username == username && photo.IsPrimary == true).FirstOrDefault();

      if (existingPhoto != null) {
       
        await _bucketService.DeletePhoto(existingPhoto.PhotoUrl, cancellationToken);
        await _bucketService.StorePhoto(photoProfile,photoPath, cancellationToken);

         existingPhoto.PhotoUrl = $"{photoPath}/{photoProfile.FileName}";
        _photoContext.Photos.Update(existingPhoto);
        await _photoContext.SaveChangesAsync(cancellationToken);
        return;
      }

     
      await _bucketService.StorePhoto(photoProfile, photoPath, cancellationToken);

      var photoProfileEntity = new Photo
      {
        Id = Guid.NewGuid(),
        Username = username,
        IsPrimary = true,
        PhotoUrl = $"{photoPath}/{photoProfile.FileName}",
        DateUpdated = dateUpdated
      };

      _photoContext.Photos.Add(photoProfileEntity);
      await _photoContext.SaveChangesAsync(cancellationToken);
    }
  }
}
