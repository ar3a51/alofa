using Alofa.Apis.Photos.AlofaPhotos.Models;
using Amazon.S3;
using Amazon.S3.Model;
using Amazon.S3.Transfer;
using Microsoft.EntityFrameworkCore.Metadata.Internal;
using System.IO;

namespace Alofa.Apis.Photos.AlofaPhotos.Services
{
  public class S3BucketService : IS3BucketService
  {
    private IAmazonS3 _amazonS3;
    private readonly string _bucketName = "alofa-storage";
    public S3BucketService(IAmazonS3 amazonS3) {
      _amazonS3 = amazonS3;
    }
    public async Task StorePhoto(IFormFile file, string path, CancellationToken token)
    {
      var key = $"{path}/{file.FileName}";
      using (var fileTransferUtility = new TransferUtility(_amazonS3))
      {
        await fileTransferUtility.UploadAsync(file.OpenReadStream(), _bucketName, key);
      }
    }

    public async Task<GetPhotoDto> GetPhoto(string path, CancellationToken cancellationToken)
    {
      var key = path;

      Console.WriteLine($"Getting from: {key}");
      var response = await _amazonS3.GetObjectAsync(_bucketName, key, cancellationToken);
      return new GetPhotoDto(response.ResponseStream, response.Headers.ContentType);
    }

    public async Task DeletePhoto(string path, CancellationToken token)
    {
      var key = path;
      await _amazonS3.DeleteAsync(_bucketName, key, null, token);
    }
  }
}
