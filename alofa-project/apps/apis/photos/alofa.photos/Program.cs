using Alofa.Apis.Photos.AlofaPhotos.Data;
using Alofa.Apis.Photos.AlofaPhotos.Services;
using Amazon.Extensions.NETCore.Setup;
using Amazon.S3;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;


var builder = WebApplication.CreateBuilder(args);


// Add services to the container.
builder.Services.AddDefaultAWSOptions(builder.Configuration.GetAWSOptions());

builder.Services.AddAWSService<IAmazonS3>();
builder.Services.AddScoped<IS3BucketService, S3BucketService>();
builder.Services.AddScoped<IPhotoService, PhotoService>();

builder.Services.AddCognitoIdentity();
builder.Services.AddAuthentication(options =>
{
  options.DefaultAuthenticateScheme = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme;
  options.DefaultChallengeScheme = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme;
})
.AddJwtBearer(options =>
{
  options.Authority = builder.Configuration["Cognito:Authority"];
  options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
  {
    ValidateIssuerSigningKey = true,
    ValidateAudience = false,
    ValidateLifetime = true,
  };
});

builder.Services.AddDbContext<PhotoContext>(options => options.UseNpgsql(builder.Configuration.GetConnectionString("AlofaDB"),
  option=> option.MigrationsHistoryTable(
    tableName: HistoryRepository.DefaultTableName,
    schema: "Alofa.Photos")
  )
);

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

builder.Services.AddAWSLambdaHosting(LambdaEventSource.HttpApi);

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
