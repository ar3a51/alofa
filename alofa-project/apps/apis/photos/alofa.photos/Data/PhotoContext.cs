using Alofa.Apis.Photos.AlofaPhotos.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Alofa.Apis.Photos.AlofaPhotos.Data
{
  public class PhotoContext : DbContext
  {
    public DbSet<Photo> Photos { get; set; }
    public PhotoContext(DbContextOptions options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.HasDefaultSchema("Alofa.Photos");
    }
  }
}
