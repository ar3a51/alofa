using Microsoft.EntityFrameworkCore;

namespace Alofa.Apis.Photos.AlofaPhotos.Data.Entities
{
  [PrimaryKey(nameof(Id))]
  public class Photo
  {
    public Guid Id { set; get; }
    public string Username { set; get; }
    public string PhotoUrl { set; get; }
    public bool IsPrimary { set; get; }
    public DateTime DateUpdated { set; get; }
  }
}
