using Alofa.Apis.Photos.AlofaPhotos.Services;
using Amazon.S3;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;

namespace Alofa.Apis.Photos.AlofaPhotos.Controllers;

[ApiController]
[Route("api/[controller]")]
[Authorize]
public class PhotosController : ControllerBase
{
  
    private readonly ILogger<PhotosController> _logger;
    private readonly IPhotoService _photoService;
   
    public PhotosController(ILogger<PhotosController> logger, IPhotoService photoService)
    {
        _logger = logger;
        _photoService = photoService;
    }

    [HttpGet]
    [Route("healthcheck")]
    [AllowAnonymous]
    public IActionResult Get() {
      return Ok("Photo end point works");
    }
   
    [HttpPost("photoupload")]
  //[AllowAnonymous]
    [NonAction]
    public async Task<IActionResult> TestUploadPhotos(
      IFormFile file, DateTime dateCreated, CancellationToken token)
    {
      
    //TODO:  Save the received files to S3 bucket
    await this._photoService.SavePhoto("testUser1", false, file, dateCreated, token);
      return Ok();
      //return Ok($"Success filename:{file.FileName}, length: {file.Length}, additionalData: {additionalData}");
    }

    [HttpPost("uploadphotoprofile")]
    public async Task<IActionResult> UploadPhotoProfile(
      IFormFile photoProfileFile, DateTime dateUpdated, CancellationToken token)
    {

      var username = User.Claims.FirstOrDefault(c => c.Type == "username")?.Value;
    if (username == null) { return NotFound("username not found"); }
    await this._photoService.UploadPhotoProfile(username, photoProfileFile, dateUpdated, token);
      return Ok();
    }

    [HttpGet("profilephoto/{username}")]
    [AllowAnonymous]
    public async Task<IActionResult> GetPhotoProfile(string username, CancellationToken token)
    {
      var result = await this._photoService.GetPhotoProfile(username, token);
      if (result == null) { return NotFound("Photo not found"); }
      return File(result.DataStream, result.ContentType);
    }
}
