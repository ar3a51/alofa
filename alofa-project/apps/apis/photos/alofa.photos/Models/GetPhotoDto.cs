namespace Alofa.Apis.Photos.AlofaPhotos.Models
{
  public record GetPhotoDto(Stream DataStream, string ContentType);
 
}
