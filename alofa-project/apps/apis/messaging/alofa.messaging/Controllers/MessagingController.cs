using Alofa.Apis.Messaging.AlofaMessaging.Models;
using Alofa.Apis.Messaging.AlofaMessaging.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Alofa.Apis.Messaging.AlofaMessaging.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  [Authorize]
  public class MessagingController : ControllerBase
  {
    private IMessagingService _messageService;
    public MessagingController(IMessagingService messageService)
    {
      _messageService = messageService;
    }

    [HttpPost]
    public async Task SendMessage([FromBody] SendMessageDto sendMessage)
    {
      await this._messageService.SendMessage(sendMessage.sender, sendMessage.receiver, sendMessage.messageBody, Guid.Empty, sendMessage.dateSent);
    }

     [HttpGet]
     [Route("conversation")]
    public async Task<IActionResult> GetConversation([FromQuery] string sender)
    {
      var loggedInUser = User.Claims.FirstOrDefault(c => c.Type == "username")?.Value;
      var result = await this._messageService.GetMessage(sender,loggedInUser);
      return Ok(result);
    }

    [HttpGet]
    public async Task<IActionResult> GetTopLevelMessage() {
       var loggedInUser = User.Claims.FirstOrDefault(c => c.Type == "username")?.Value;
      var result = await this._messageService.GetTopLevelMessage(loggedInUser);
      return Ok(result);
    }
  }

}
