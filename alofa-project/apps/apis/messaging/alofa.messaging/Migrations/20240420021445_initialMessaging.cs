﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Alofa.Apis.Messaging.AlofaMessaging.Migrations
{
    /// <inheritdoc />
    public partial class initialMessaging : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.EnsureSchema(
                name: "Alofa.Messaging");

            migrationBuilder.CreateTable(
                name: "messages",
                schema: "Alofa.Messaging",
                columns: table => new
                {
                    Id = table.Column<Guid>(type: "uuid", nullable: false),
                    SenderUsername = table.Column<string>(type: "text", nullable: false),
                    ReceiverUsername = table.Column<string>(type: "text", nullable: false),
                    MessageBody = table.Column<string>(type: "text", nullable: false),
                    ParentMessageId = table.Column<Guid>(type: "uuid", nullable: false)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_messages", x => x.Id);
                });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "messages",
                schema: "Alofa.Messaging");
        }
    }
}
