﻿using System;
using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

namespace Alofa.Apis.Messaging.AlofaMessaging.Migrations
{
    /// <inheritdoc />
    public partial class addSentDate : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AddColumn<DateTime>(
                name: "DateSent",
                schema: "Alofa.Messaging",
                table: "messages",
                type: "timestamp with time zone",
                nullable: false,
                defaultValue: new DateTime(1, 1, 1, 0, 0, 0, 0, DateTimeKind.Unspecified));
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "DateSent",
                schema: "Alofa.Messaging",
                table: "messages");
        }
    }
}
