using Alofa.Apis.AlofaMessaging;

namespace Alofa.Apis.Messaging.AlofaMessaging.Services
{
  public interface IMessagingService
  {
    Task SendMessage(string fromUsername,
      string toUsername,
      string message,
      Guid parentMessageId,
      DateTime dateSent);
    Task<List<MessagingDto>> GetMessage(string fromUsername, string receiverUsername);

    Task<List<MessagingDto>> GetTopLevelMessage(string receiverName);
  }
}
