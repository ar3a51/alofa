using Alofa.Apis.AlofaMessaging;
using Alofa.Apis.Messaging.AlofaMessaging.Data;
using Alofa.Apis.Messaging.AlofaMessaging.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Alofa.Apis.Messaging.AlofaMessaging.Services
{
  public class MessagingService : IMessagingService
  {
    private MessageContext _messageContext;
    public MessagingService(MessageContext context)
    {
      _messageContext = context;
    }

    public async Task<List<MessagingDto>> GetMessage(string fromUsername, string receiverUsername)
    {
      return await this._messageContext.messages.Where(
        message => (message.ReceiverUsername == receiverUsername && message.SenderUsername == fromUsername) ||
        (message.ReceiverUsername == fromUsername && message.SenderUsername == receiverUsername)
        ).OrderBy(message => message.DateSent)
          .Select(message => new MessagingDto
          {
            MessageId = message.Id,
            ParentMessageId = message.ParentMessageId == Guid.Empty ? null : message.ParentMessageId,
            SenderUsername = message.SenderUsername,
            ReceiverUsername = message.ReceiverUsername,
            MessageSentDate = message.DateSent,
            MessageBody = message.MessageBody
          }).ToListAsync();
    }

    public async Task<List<MessagingDto>> GetTopLevelMessage(string receiverName)
    {
      return await this._messageContext.messages.Where(message => message.ReceiverUsername == receiverName)
         .Select(message => new MessagingDto
          {
            MessageId = message.Id,
            ParentMessageId = message.ParentMessageId == Guid.Empty ? null : message.ParentMessageId,
            SenderUsername = message.SenderUsername,
            ReceiverUsername = message.ReceiverUsername,
            MessageSentDate = message.DateSent,
            MessageBody = message.MessageBody
          }).ToListAsync();
    }

    public async Task SendMessage(
      string fromUsername,
      string toUsername,
      string message,
      Guid parentMessageId,
      DateTime dateSent)
    {
      var messageEntity = new Message
      {
        Id = Guid.NewGuid(),
        ParentMessageId = parentMessageId,
        SenderUsername = fromUsername,
        ReceiverUsername = toUsername,
        MessageBody = message,
        DateSent = dateSent
      };

      await this._messageContext.AddAsync(messageEntity);
      await this._messageContext.SaveChangesAsync();
    }
  }
}
