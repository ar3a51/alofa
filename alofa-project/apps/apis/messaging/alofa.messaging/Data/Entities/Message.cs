using Microsoft.EntityFrameworkCore;
using System.Diagnostics.CodeAnalysis;

namespace Alofa.Apis.Messaging.AlofaMessaging.Data.Entities
{
  [PrimaryKey(nameof(Id))]
  public class Message
  {
    [NotNull]
    public Guid Id { get; set; }
    public string SenderUsername { set; get; }
    public string ReceiverUsername { set; get; }

    public string MessageBody { set; get; }
    public Guid ParentMessageId { set; get; }
    public DateTime DateSent { set; get; }
  }
}
