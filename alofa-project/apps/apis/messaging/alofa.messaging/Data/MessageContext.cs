using Alofa.Apis.Messaging.AlofaMessaging.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Alofa.Apis.Messaging.AlofaMessaging.Data
{
  public class MessageContext : DbContext
  {
    public DbSet<Message> messages { get; set; }
    public MessageContext(DbContextOptions options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.HasDefaultSchema("Alofa.Messaging");
    }
  }
}
