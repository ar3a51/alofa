namespace Alofa.Apis.Messaging.AlofaMessaging.Models;
public record SendMessageDto(string sender, string receiver, string messageBody, DateTime dateSent);