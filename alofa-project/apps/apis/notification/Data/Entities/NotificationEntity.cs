using Microsoft.EntityFrameworkCore;

namespace Alofa.Apis.Notification.Data.Entities
{
  [PrimaryKey(nameof(Id))]
  public class NotificationEntity
  {
    public Guid Id { get; set; }
    public string OwnerNotificationUsername { set; get; }
    public string Message { set; get; }
    public string Type { set; get; }
    public bool IsRead { set; get; }
    public DateTime DateSent { set; get; }
  }
}
