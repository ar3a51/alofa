using Alofa.Apis.Notification.Data.Entities;
using Microsoft.EntityFrameworkCore;

namespace Alofa.Apis.Notification.Data
{
  public class NotificationContext : DbContext
  {
    public DbSet<NotificationEntity> Notifications { get; set; }
    public NotificationContext(DbContextOptions options) : base(options)
    {
    }

    protected override void OnModelCreating(ModelBuilder modelBuilder)
    {
      modelBuilder.HasDefaultSchema("Alofa.Notification");
      //base.OnModelCreating(modelBuilder);
    }
  }
}
