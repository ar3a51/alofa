namespace Alofa.Apis.Notification.Models;

public record NotificationDto(Guid id, string username, string message, string notificationType, bool isNew);