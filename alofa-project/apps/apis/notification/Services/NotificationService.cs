using Alofa.Apis.Notification.Data;
using Alofa.Apis.Notification.Models;
using Microsoft.EntityFrameworkCore;

namespace Alofa.Apis.Notification.Services
{
  public class NotificationService : INotificationService
  {
    private readonly NotificationContext _notificationContext;
    public NotificationService(NotificationContext context)
    {
      _notificationContext = context;
    }

    public async Task DeleteAllNotification(string ownerUsername, CancellationToken token)
    {
      await _notificationContext.Notifications
        .Where(notification => notification.OwnerNotificationUsername == ownerUsername)
        .ExecuteDeleteAsync(token);
    }

    public async Task<IList<NotificationDto>> GetNotifications(string username, CancellationToken token)
    {
      return await _notificationContext.Notifications
        .Where(notification => notification.OwnerNotificationUsername == username)
        .Select(notification => new NotificationDto(
          notification.Id,
          notification.OwnerNotificationUsername,
          notification.Message,
          notification.Type,
          !notification.IsRead)
          )
        .ToListAsync(token);
    }

    public async Task<IList<NotificationDto>> GetTop5NewNotifications(string username, CancellationToken token)
    {
      return await _notificationContext.Notifications
        .Where(notification => notification.OwnerNotificationUsername == username)
        .OrderByDescending(notification => notification.DateSent)
        .Select(notification => new NotificationDto(
          notification.Id,
          notification.OwnerNotificationUsername,
          notification.Message,
          notification.Type,
          !notification.IsRead)
          ).ToListAsync(token);
    }

    public async Task MarkAllReadNotification(string ownerUsername, CancellationToken token)
    {
      await _notificationContext.Notifications
        .Where(notification => notification.OwnerNotificationUsername == ownerUsername)
        .ExecuteUpdateAsync(notification => notification.SetProperty(p => p.IsRead, true), token);
    }

    public async Task ReadOneNotification(string notificationId, string ownerUsername, CancellationToken token)
    {
      await _notificationContext.Notifications
        .Where(notification => notification.OwnerNotificationUsername == ownerUsername && notification.Id == Guid.Parse(notificationId))
        .ExecuteUpdateAsync(notification => notification.SetProperty(p => p.IsRead, true), token);
    }
  }
}
