using Alofa.Apis.Notification.Models;

namespace Alofa.Apis.Notification.Services
{
  public interface INotificationService
  {
    Task<IList<NotificationDto>> GetNotifications(string username, CancellationToken token);
    Task<IList<NotificationDto>> GetTop5NewNotifications(string username, CancellationToken token);
    Task MarkAllReadNotification(string ownerUsername, CancellationToken token);
    Task DeleteAllNotification(string ownerUsername, CancellationToken token);
    Task ReadOneNotification(string notificationId, string ownerUsername, CancellationToken token);
  }
}
