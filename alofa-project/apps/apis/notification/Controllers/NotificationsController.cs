using Alofa.Apis.Notification.Services;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace Alofa.Apis.Notification.Controllers
{
  [Route("api/[controller]")]
  [ApiController]
  [Authorize]
  public class NotificationsController : ControllerBase
  {
    private readonly INotificationService _notificationService;
    public NotificationsController(INotificationService notificationService) {
      _notificationService = notificationService;
    }

    [HttpGet("healthcheck")]
    [AllowAnonymous]
    public IActionResult HealthCheck() {
      return Ok("Notification end point works");
    }

    [HttpGet]
    public async Task<IActionResult> GetNotifications(CancellationToken token)
    {
      var result = await _notificationService.GetNotifications(GetUsername(), token);
      return Ok(result);
    }

    [HttpGet("mini")]
    public async Task<IActionResult> GetTop5Notifications(CancellationToken token)
    {
      var result = await _notificationService.GetTop5NewNotifications(GetUsername(), token);
      return Ok(result);
    }

    [HttpPut]
    public async Task<IActionResult> ReadAll(CancellationToken token) {

      await _notificationService.MarkAllReadNotification(GetUsername(), token);
      return Ok();
    }

    [HttpDelete]
    public async Task<IActionResult> ClearAll(CancellationToken token)
    {
      await _notificationService.DeleteAllNotification(GetUsername(), token);
      return Ok();
    }

    [HttpPut("read/{notificationId}")]
    public async Task<IActionResult> ReadNotification(string notificationId, CancellationToken token)
    {
      await _notificationService.ReadOneNotification(notificationId, GetUsername(), token);
      return Ok();
    }

    private string GetUsername()
    {
      return User.Claims.First(c => c.Type == "username").Value;
    }
  }
}
