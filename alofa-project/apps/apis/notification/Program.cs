using Alofa.Apis.Notification.Data;
using Alofa.Apis.Notification.Services;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Migrations;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddCognitoIdentity();
builder.Services.AddAuthentication(options =>
{
  options.DefaultAuthenticateScheme = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme;
  options.DefaultChallengeScheme = Microsoft.AspNetCore.Authentication.JwtBearer.JwtBearerDefaults.AuthenticationScheme;
})
.AddJwtBearer(options =>
{
  options.Authority = builder.Configuration["Cognito:Authority"];
  options.TokenValidationParameters = new Microsoft.IdentityModel.Tokens.TokenValidationParameters
  {
    ValidateIssuerSigningKey = true,
    ValidateAudience = false,
    ValidateLifetime = true,
  };
});

builder.Services.AddDbContext<NotificationContext>(context =>
  context.UseNpgsql(builder.Configuration.GetConnectionString("AlofaDB"), option =>
    option.MigrationsHistoryTable(
      tableName: HistoryRepository.DefaultTableName,
      schema: "Alofa.Notification")
    )
);

builder.Services.AddScoped<INotificationService, NotificationService>();


builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

//app.UseHttpsRedirection();

app.UseAuthorization();

app.MapControllers();

app.Run();
