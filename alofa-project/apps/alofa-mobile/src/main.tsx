import { StrictMode } from 'react';
import * as ReactDOM from 'react-dom/client';
import { Provider } from 'react-redux';
import { BrowserRouter, Route, Routes } from 'react-router-dom';
import { store } from './app/store';

import { Protected } from './app/Protected';
import React from 'react';
import LoadingFallback from './app/Loading-fallback';
import { ChakraProvider } from '@chakra-ui/react';
import { ChakraTest } from './app/pages/test-page/chakra-test';
import alofaTheme from './alofa-theme';
import AlofaProfileRouting from './app/pages/Profile/alofa-mobile-profile.routing';

const AlofaMobileLogin = React.lazy(
  () => import('./app/pages/alofa-mobile-login/alofa-mobile-login')
);
const AlofaMobileRegistration = React.lazy(
  () =>
    import('./app/pages/alofa-mobile-registration/alofa-mobile-registration')
);
const Welcome = React.lazy(
  () => import('./app/pages/alofa-mobile-welcome/alofa-mobile-welcome')
);
const AlofaMobileMainRouting = React.lazy(
  () => import('./app/pages/alofa-mobile-main/alofa-mobile-main.routing')
);

const root = ReactDOM.createRoot(
  document.getElementById('root') as HTMLElement
);
root.render(
  <StrictMode>
    <ChakraProvider theme={alofaTheme}>
      <BrowserRouter>
        <Provider store={store}>
          <Routes>
            <Route path="/*" element={<Welcome />} />
            <Route path="/chakratest" element={<ChakraTest />} />
            <Route
              path="/register"
              element={
                <React.Suspense fallback={<LoadingFallback />}>
                  <AlofaMobileRegistration />
                </React.Suspense>
              }
            />
            <Route
              path="main/*"
              element={
                <React.Suspense fallback={<LoadingFallback />}>
                  <AlofaMobileMainRouting />
                </React.Suspense>
              }
            />
            <Route
              path="profile/*"
              element={
                <React.Suspense fallback={<LoadingFallback />}>
                  <AlofaProfileRouting />
                </React.Suspense>
              }
            />
            <Route
              path="/login"
              element={
                <React.Suspense fallback={<LoadingFallback />}>
                  <AlofaMobileLogin />
                </React.Suspense>
              }
            />
            <Route element={<Protected roles={['user']} />}></Route>
          </Routes>
        </Provider>
      </BrowserRouter>
    </ChakraProvider>
  </StrictMode>
);
