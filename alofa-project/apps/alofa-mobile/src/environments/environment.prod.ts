export const environment = {
  production: true,
  goApi: '',
  goApiKey: '',
  authentication: {
    userPoolId: "",
    clientId: ""
  }
};
