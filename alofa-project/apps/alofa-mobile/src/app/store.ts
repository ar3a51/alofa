import { configureStore } from "@reduxjs/toolkit";
import { authSlice } from '@alofa/alofa-shared-lib';
import { statusSlice } from '@alofa/alofa-shared-lib'
import apiSlice from "./query";
import { profileSlice } from "./pages/Profile/store/profile.store";


export const store = configureStore({
    reducer: {
        auth: authSlice.reducer,
        status: statusSlice.reducer,
        profile: profileSlice.reducer,
        [apiSlice.reducerPath]: apiSlice.reducer
    },
    middleware: (getDefaultMiddleWare)=> getDefaultMiddleWare().concat(apiSlice.middleware),
    devTools: true
})