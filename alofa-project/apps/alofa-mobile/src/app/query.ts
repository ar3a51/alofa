import { RootState } from './store.typed';
import {BaseQueryApi, FetchArgs, fetchBaseQuery, createApi} from '@reduxjs/toolkit/query/react'
import { loginStateType, logout, setCredentials } from '@alofa/alofa-shared-lib';



const baseQuery = fetchBaseQuery({
    //baseUrl: 'http://localhost:5000',
    credentials: 'include',
    prepareHeaders: (headers, {getState})=>{
        const token = (getState() as RootState).auth.accessToken;
        if (token)
            headers.set('Authorization', `Bearer ${token}`)

        return headers;
    }
})


const fetchApi = async (args: string | FetchArgs, api: BaseQueryApi, extraOptions: any)=>{
    let result = await baseQuery(args, api, extraOptions);
    console.log(result);
    if ((result?.error as any)?.originalStatus === 403) {
        console.log('sending refresh token');
        const refreshResult = await baseQuery('/refresh', api, extraOptions);
        console.log(refreshResult.data);

        if (refreshResult?.data){
            const credDetails = refreshResult?.data as loginStateType;

            api.dispatch(setCredentials(credDetails))

            result = await baseQuery(args, api, extraOptions);
        } else {
            api.dispatch(logout());
        }
    }

    return result;
}

const apiSlice = createApi({
    baseQuery: fetchApi,
    endpoints: (builder)=>({})
})

export default apiSlice;
