import apiSlice from "../../query";

export const registrationApiSlice = apiSlice.injectEndpoints({
    endpoints: builder => ({
        register: builder.mutation({
            query: loginDetails => ({
                url: '/api/registration/register',
                method: 'POST',
                body: { ...loginDetails }
            })
        })
    })
})

 export const { useRegisterMutation  } = registrationApiSlice;