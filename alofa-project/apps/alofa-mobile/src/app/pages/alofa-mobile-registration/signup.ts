import { CognitoUserAttribute } from "amazon-cognito-identity-js";
import userPool from "../../user-pool";

export const SignUp = (username: string, password: string, email: string) => {
    
   
    const attributeList: any = [
    new CognitoUserAttribute({
        Name: "email",
        Value: email,
    }),
    ];
    userPool.signUp(username, password, attributeList, [], (err, result) => {
        if (err) {
            console.log(err);
            return;
        }
        console.log("call result: ", result);
    });
};
