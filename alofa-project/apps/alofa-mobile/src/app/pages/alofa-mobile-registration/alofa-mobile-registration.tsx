import { AlofaButton, AlofaTextbox } from '@alofa/alofa-ui';
import { Heading } from '@chakra-ui/react';
import { useForm, Formiz, FormizStep, useField } from '@formiz/core';
import { isEmail, isRequired } from '@formiz/validations';
import styles from './alofa-mobile-registration.module.scss'
import { AlofaMandatoryMark, AlofaMobileHeader } from '@alofa/alofa-mobile-ui';
import { useNavigate } from 'react-router-dom';
import { LoginDTO } from '@alofa/generated/authentication-end-point-types';
import { useRegisterMutation } from './registrationApiSlice';
import { SignUp } from './signup';


function TextField(props: any){
    const {value, isValid, errorMessage, setValue} = useField(props);

    return(
        <>
            {
                !isValid
                &&<AlofaMandatoryMark/> // Display error message
            }
            <AlofaTextbox 
                id={props.id} 
                name={props.name}
                value={value ?? ''}
                onChange={(e: any)=>setValue(e.target.value)}
                type={props.type}
            ></AlofaTextbox>
            
        </>
    )

}

interface regModel {
    username: string;
    email: string;
    password: string;
}

function AlofaMobileRegistration(){
    const regForm = useForm();
    const navigate = useNavigate();
    const [register, {isLoading, isError}] = useRegisterMutation();
    
    const handleValidSubmit = async (values: regModel) => {
        const newLogin: LoginDTO = {
            emailAddress: values.email,
            password: values.password
        }
        SignUp("TestUser1", newLogin.password, newLogin.emailAddress);
        //setTimeout(()=>navigate("/login"),1000);
        

    } 

    const goPrevStep = (e: any)=>{
        e.preventDefault();
        regForm.prevStep();
    }

    const handleSubmitStep=(e:any)=>{
        e.preventDefault();
        regForm.submitStep();
    }

    const backButtonClick=(e:any)=>{
        e.preventDefault();
        if (regForm.isFirstStep)
            navigate('/');
        else
            regForm.prevStep();
    }

    return(
        <div className={styles['container-form']}>
            <AlofaMobileHeader onClick={backButtonClick}></AlofaMobileHeader>
            {!regForm.isSubmitted &&
                <Formiz connect={regForm} onValidSubmit={handleValidSubmit}>
                    <form noValidate className={styles['reg-form']} onSubmit={handleSubmitStep}>
                        <FormizStep name='emailForm'>
                            <Heading as='h1' className={styles['title']}>What is your email address?</Heading> 
                            <TextField id='email' name='email' type='email' validations={[
                                {
                                    rule: isRequired(),
                                },
                                {
                                    rule: isEmail(),
                                }
                            ]}/>           
                        </FormizStep>
                        <FormizStep name='passwordForm'>
                            <Heading as='h1' className={styles['title']}>What is your Password?</Heading> 
                            <TextField id='password' name='password' type='password' validations={[
                                {
                                    rule: isRequired(),
                                }
                            ]}
                            
                            />
                        </FormizStep>
                        <FormizStep name='retypePasswordForm'>
                            <Heading as='h1'>Type it again just in case...</Heading>
                            <TextField id='retype-password' name='retype-password' type='password'></TextField>
                        </FormizStep>
                        <div className={styles['btn-group']}>
                        
                        
                            {
                                regForm.isLastStep ? 
                                    <AlofaButton>Submit</AlofaButton> :
                                    <AlofaButton>Continue</AlofaButton>
                                    
                            }
                        
                        
                            {!regForm.isFirstStep && 
                                <AlofaButton isPrimary={false} 
                                    onClick={goPrevStep}>Prev</AlofaButton>
                                }
                        
                        </div>
                    </form>
                </Formiz>
            }
            {regForm.isSubmitted &&
                <h1>Registration succeeded</h1>
            }
        </div>
    )
}

export default AlofaMobileRegistration;