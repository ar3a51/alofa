import { Box, Stack } from "@chakra-ui/react";
import { useField } from "@formiz/core";
import { AlofaButton, AlofaCheckbox } from '@alofa/alofa-ui';

function ShowDetailsInProfile(props: any) {
    const {value, setValue} = useField(props);
    return(
        <Stack position={'sticky'}
                left={'0'}
                right={'0'}
                flexDirection={'column-reverse'} 
                bottom='0' 
                width={'100%'}
                alignItems={'center'}
                justify="center"
                paddingTop={'2em'}>
            <Box p={'2'}>
                <AlofaButton 
                    onClick={props.onSave} disabled={props.disableSave}>Save & Continue</AlofaButton>
            </Box>
            { 
              props.showSkipButton === true ? 
                <Box p={'2'}>
                    <AlofaButton isPrimary={false} onClick={props.onSkip}>Skip</AlofaButton>
                </Box>
                : null 
            }
            <Box as={'fieldset'}>
                <AlofaCheckbox 
                    id={props.name} checked={value || false} 
                    name={props.name} label="Show in Profile"
                    onChange={(e)=>setValue(e.target.checked)}/>
            </Box>
        </Stack>
    )
}

export default ShowDetailsInProfile;