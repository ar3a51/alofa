import apiSlice from "../../query";

export const memberApiSlice = apiSlice.injectEndpoints({
    endpoints: builder => ({
        saveProfile: builder.mutation({
            query: profile => ({
                url: '/api/member/saveprofile',
                method: 'POST',
                body: profile
            })
        }),
        getProfile: builder.mutation({
            query: () => ({
                url: '/api/member/getprofile',
                method: 'GET'
            })
        })
    })
})

 export const { useSaveProfileMutation, useGetProfileMutation  } = memberApiSlice;