interface ShowInProfileInterface {
    showInProfile: boolean;
}

interface LocationDataInterface {
    id: string,
    name: string
}

export interface AboutYouInterface extends ShowInProfileInterface {
    description: string;
}

export interface DateOfBirthInterface extends ShowInProfileInterface {
    dateOfBirth: string;
    placeOfBirth: string;
}

export interface GenderInterface extends ShowInProfileInterface {
    genderDescription: string;
    genderOtherDescription: string;
}

export interface ReligionInterface extends ShowInProfileInterface {
    religionDescription: string
    otherDescription: string
}

export interface LocationInterface extends ShowInProfileInterface {
    province: LocationDataInterface;
    municipality: LocationDataInterface;
    subdistrict: LocationDataInterface;
}

export interface ProfileInterface {
    aboutYou: AboutYouInterface,
    dob: DateOfBirthInterface,
    gender: GenderInterface,
    religion: ReligionInterface,
    location: LocationInterface
}

export const initialProfile: ProfileInterface = {
    aboutYou: {description: '', showInProfile: false},
    dob: {dateOfBirth: '', placeOfBirth: '', showInProfile: false},
    gender: {genderDescription: '', genderOtherDescription: '', showInProfile: false},
    religion: {religionDescription: '', otherDescription: '', showInProfile: false},
    location: {province: {id: '', name: ''}, municipality: {id: '', name: ''}, subdistrict: {id: '', name: ''}, showInProfile: false}
}