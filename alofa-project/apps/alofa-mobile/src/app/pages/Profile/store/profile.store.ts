import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { AboutYouInterface, DateOfBirthInterface, GenderInterface, initialProfile, LocationInterface, ReligionInterface } from "./profile-store.model";

export const profileSlice = createSlice({
    name: 'profile',
    initialState: initialProfile,
    reducers: {
        setAboutYou: (state, action: PayloadAction<AboutYouInterface>)=>{
            state.aboutYou.description = action.payload.description;
            state.aboutYou.showInProfile = action.payload.showInProfile;
        },
        setDob: (state, action: PayloadAction<DateOfBirthInterface>) => {
            state.dob.dateOfBirth = action.payload.dateOfBirth;
            state.dob.placeOfBirth = action.payload.placeOfBirth;
            state.dob.showInProfile = action.payload.showInProfile;
        },
        setReligion: (state, action: PayloadAction<ReligionInterface>)=> {
            state.religion.religionDescription = action.payload.religionDescription;
            state.religion.otherDescription = action.payload.otherDescription;
            state.religion.showInProfile = action.payload.showInProfile;
        },
        setGender: (state, action: PayloadAction<GenderInterface>) => {
            state.gender.genderDescription = action.payload.genderDescription;
            state.gender.genderOtherDescription = action.payload.genderOtherDescription;
            state.gender.showInProfile = action.payload.showInProfile;
        },
        setLocation: (state, action: PayloadAction<LocationInterface>) => {
            state.location.province = action.payload.province;
            state.location.municipality = action.payload.municipality;
            state.location.subdistrict = action.payload.subdistrict;
            state.location.showInProfile = action.payload.showInProfile;
        }
    }
})

export const {setAboutYou, setDob, setReligion, setGender, setLocation} = profileSlice.actions;