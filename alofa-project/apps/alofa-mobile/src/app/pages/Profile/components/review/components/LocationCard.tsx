import { RootState } from "@alofa/alofa-mobile-app";
import { useSelector } from "react-redux";
import MainCard from "./MainCard";
import { Box, Grid, GridItem } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

export default function LocationCard() {
    const locationData = useSelector((state: RootState)=> state.profile.location);
    const navigate = useNavigate();

    const editLocation = () => {
        navigate('/profile/location');
    }

    return (
        <MainCard 
            title={'Location'}
            onClick={editLocation}
            body={
                <Grid templateColumns={'1fr'} w={'100%'}>
                    <GridItem w={'100%'}>
                        <Box as={"b"}>Your Location</Box>
                    </GridItem>
                    <GridItem w={'100%'} paddingTop={'1em'} paddingBottom={'1em'}>
                        <Box as={'span'}>{locationData?.province?.name}</Box>
                    </GridItem>
                    <GridItem w={'100%'}>
                        <Box as={"span"}>{locationData?.municipality?.name}</Box>
                    </GridItem>
                    <GridItem w={'100%'} paddingTop={'1em'} paddingBottom={'1em'}>
                            <Box as={'span'}>{locationData?.subdistrict?.name}</Box>
                        </GridItem>
                    <GridItem w={'100%'}>
                        <Box as="b">Show in the profile?</Box>
                    </GridItem>
                    <GridItem w={'100%'}>
                        <Box as={'span'}>{!locationData.showInProfile ? 'no' : 'yes'}</Box>
                    </GridItem>
                </Grid>
            }>

        </MainCard>
    )
}