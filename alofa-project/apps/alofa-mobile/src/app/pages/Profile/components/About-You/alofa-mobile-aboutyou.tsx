import { Box, Heading, Textarea, Grid, GridItem } from "@chakra-ui/react";
import { Formiz, useField, useForm } from "@formiz/core";
import { commonStepProp } from "../../commonStepProperty";
import ShowDetailsInProfile from "../../show-details-in-profile";
import { useDispatch, useSelector } from "react-redux";
import { useNavigate } from "react-router-dom";
import { setAboutYou } from "../../store/profile.store";
import { useEffect } from "react";
import { RootState } from "apps/alofa-mobile/src/app/store.typed";


function AboutYouField(props: any){
    const {value, setValue} = useField(props);
  

    return(
        <Box as={'fieldset'} marginTop={'3em'}>
            <p>Share a little about yourself</p>
            <Textarea 
                {...props}
                onChange={(e: any)=>setValue(e.target.value)}
                rows="8"
                value={value || ''}
                marginTop={'1rem'}/>
        </Box>
    )
}

export interface aboutYouProps {
    onSkip?: (e: any)=>void;
}

export default function AboutYouForm(props: commonStepProp) {
    //const {value, setValue} = useField(props);
    const abtYouForm = useForm();
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const aboutYouSelector = useSelector((state: RootState)=> state.profile.aboutYou);
    const init = {abtYouField: aboutYouSelector.description, showAbtYouInProfile: aboutYouSelector.showInProfile}

    function handleFormSubmit(e: any) {
        dispatch(setAboutYou({
            description: e.abtYouField,
            showInProfile: e.showAbtYouInProfile
        }));

        navigate("/profile/religion");
        
    }
        
    return (
    <Formiz initialValues={init} connect={abtYouForm} onSubmit={handleFormSubmit}>
        <Grid templateColumns={'1fr'} width={'100%'}>
            <GridItem width={'100%'}>
                <Heading>About you</Heading>
            </GridItem>
            <GridItem width={'100%'}>
                <AboutYouField name="abtYouField"/>
            </GridItem>
            <GridItem width={'100%'}>
                <Box>
                    <ShowDetailsInProfile name="showAbtYouInProfile" 
                        onSave={()=>{abtYouForm.submit()}} 
                        showSkipButton={props.showSkipButton}/>
                </Box>
            </GridItem>
        </Grid>
    </Formiz>
    )
} 
