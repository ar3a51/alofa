import { RootState } from "@alofa/alofa-mobile-app";
import { useSelector } from "react-redux";
import MainCard from "./MainCard";
import { Box, Grid, GridItem } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

export default function DateOfBirthCard() {
    const dobData = useSelector((state: RootState)=> state.profile.dob);
    const navigate = useNavigate();

    const editDob = () => {
        navigate('/profile/date-of-birth');
    }

    return (
        <MainCard 
            title={'Date of Birth'}
            onClick={editDob}
            body={
                <Grid templateColumns={'1fr'} w={'100%'}>
                    <GridItem w={'100%'}>
                        <Box as={"b"}>What is your date of birth?</Box>
                    </GridItem>
                    <GridItem w={'100%'} paddingTop={'1em'} paddingBottom={'1em'}>
                        <Box as={'p'}>{dobData.dateOfBirth}</Box>
                    </GridItem>
                    <GridItem w={'100%'}>
                        <Box as={"b"}>Place of birth</Box>
                    </GridItem>
                    <GridItem w={'100%'} paddingTop={'1em'} paddingBottom={'1em'}>
                        <Box as={'p'}>{dobData.placeOfBirth}</Box>
                    </GridItem>
                    <GridItem w={'100%'}>
                        <Box as="b">Show in the profile?</Box>
                    </GridItem>
                    <GridItem w={'100%'}>
                        <Box as={'span'}>{!dobData.showInProfile ? 'no' : 'yes'}</Box>
                    </GridItem>
                </Grid>
            }>

        </MainCard>
    )
}