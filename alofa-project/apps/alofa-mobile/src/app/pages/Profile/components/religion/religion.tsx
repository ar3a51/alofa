import { Box, Grid, GridItem, Heading } from '@chakra-ui/react';
import ShowDetailsInProfile from '../../show-details-in-profile';
import { AlofaDropdown, AlofaTextbox } from '@alofa/alofa-ui';
import { Formiz, useField, useForm } from '@formiz/core';
import { commonStepProp } from '../../commonStepProperty';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from "@alofa/alofa-mobile-app";
import { setReligion } from '../../store/profile.store';
import { useNavigate } from 'react-router-dom';

function ReligionField(props: any){
  const {value, setValue} = useField(props);
  const religionData = {
    data: [
      {id: '', label: 'Select religion'},
      {id: 'isl', label: 'Islam'},
      {id: 'chr', label: 'Christian'},
      {id: 'oth', label: 'other'}
    ],
    idField: 'id',
    labelField: 'label',
    valueField: 'id'
  }

  return(
    <Box as='fieldset'>
      <label htmlFor='religion-dropdown'>What's your religion?</label>
      <AlofaDropdown {...props} 
        id='religion-dropdown' 
        name='religion-dropdown' 
        listInfo={religionData} 
        onChange={(e: any)=>setValue(e.target.value)}
        value={value ?? ''}></AlofaDropdown>
    </Box>
  )
}

function ReligionOther(props: any){
  const {value, setValue} = useField(props);

  return(
    <Box as='fieldset'>
      <AlofaTextbox {...props} id='religion-other' name='religion-other'
        onChange={(e: any)=> setValue(e.target.value)}
        value={value ?? ''}
      ></AlofaTextbox>
    </Box>
  )
}

/* eslint-disable-next-line */
export interface ReligionProps {}

const paddingSize = '1em';

export function Religion(props: commonStepProp) {
  const religionForm = useForm();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const religionFormData = useSelector((state: RootState)=> state.profile.religion);
  const initData = {
    religiondescdropdown: religionFormData.religionDescription,
    religiondescother: religionFormData.otherDescription,
    showDetailsInProfile: religionFormData.showInProfile
  }

  function handleSubmit(e: any) {
    console.log(religionForm.fields);
    dispatch(setReligion(
        {
          religionDescription: e.religiondescdropdown, 
          otherDescription: e.religiondescother, 
          showInProfile: e.showDetailsInProfile
        }
      )
    )
    navigate('/profile/date-of-birth');
  }

  return (
    <Formiz initialValues={initData} connect={religionForm} onSubmit={handleSubmit}>
      <Grid templateColumns={'1fr'} w={'100%'}>
      <GridItem w={'100%'}>
        <Heading>What is your faith?</Heading>
      </GridItem>
      <GridItem w={'100%'} paddingTop={paddingSize} paddingBottom={paddingSize}>
        <ReligionField name="religiondescdropdown" />
      </GridItem>
      <GridItem w={'100%'} paddingTop={paddingSize} paddingBottom={paddingSize}>
        {
          religionForm.fields['religiondescdropdown']?.value === 'oth' &&
          <ReligionOther placeholder={'Other'} name="religiondescother"/>
        }
      </GridItem>
      <GridItem w={'100%'}>
        <ShowDetailsInProfile name='showDetailsInProfile' 
          onSkip={props.onSkip} 
          showSkipButton={props.showSkipButton}
          onSave={()=> religionForm.submit()}/>
          
      </GridItem>
  </Grid>
 </Formiz>
  );
}

export default Religion;
