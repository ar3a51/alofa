import { Grid, GridItem, Heading } from "@chakra-ui/react";
import AboutUsCard from "./components/AboutYouCard";
import DateOfBirthCard from "./components/DateOfBirthCard";
import GenderCard from "./components/GenderCard";
import ReligionCard from "./components/ReligionCard";
import LocationCard from "./components/LocationCard";
import { useSelector } from "react-redux";
import { RootState } from "@alofa/alofa-mobile-app";
import { AlofaButton } from "@alofa/alofa-ui";



function Review() {
    const profileData = useSelector((state: RootState)=> state.profile);

    function handleSubmit() {
        console.log(profileData);
    }

    return (
        <Grid templateColumns={'1fr'} w={'100%'}>
            <GridItem w={'100%'}>
                <Heading>Review your profile</Heading>
            </GridItem>
            <GridItem w={'100%'} paddingTop={'1em'} paddingBottom={'1em'}>
                <AboutUsCard/>
            </GridItem>
            <GridItem w={'100%'} paddingTop={'1em'} paddingBottom={'1em'}>
                <DateOfBirthCard/>
            </GridItem>
            <GridItem w={'100%'} paddingTop={'1em'} paddingBottom={'1em'}>
                <GenderCard/>
            </GridItem>
            <GridItem w={'100%'} paddingTop={'1em'} paddingBottom={'1em'}>
                <ReligionCard/>
            </GridItem>
            <GridItem w={'100%'} paddingTop={'1em'} paddingBottom={'1em'}>
                <LocationCard/>
            </GridItem>
            <GridItem w={'100%'} paddingTop={'1em'} paddingBottom={'1em'}>
                <AlofaButton onClick={handleSubmit}>Submit Profile</AlofaButton>
            </GridItem>
        </Grid>
    )
}

export default Review;