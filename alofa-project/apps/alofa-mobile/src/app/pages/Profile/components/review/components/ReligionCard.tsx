import { RootState } from "@alofa/alofa-mobile-app";
import { useSelector } from "react-redux";
import MainCard from "./MainCard";
import { Box, Grid, GridItem } from "@chakra-ui/react";

export default function ReligionCard() {
    const religionData = useSelector((state: RootState)=> state.profile.religion);

    return (
        <MainCard 
            title={'Religion'}
            body={
                <Grid templateColumns={'1fr'} w={'100%'}>
                    <GridItem w={'100%'}>
                        <Box as={"b"}>What is your faith?</Box>
                    </GridItem>
                    <GridItem w={'100%'} paddingTop={'1em'} paddingBottom={'1em'}>
                        <Box as={'span'}>{religionData.religionDescription}</Box>
                    </GridItem>
                    {
                         religionData.religionDescription === 'other' && 
                        <GridItem w={'100%'}>
                            <Box as={"b"}>Please specify</Box>
                        </GridItem>
                    }
                    {
                         religionData.religionDescription === 'other' &&
                        <GridItem w={'100%'} paddingTop={'1em'} paddingBottom={'1em'}>
                            <Box as={'span'}>{religionData.otherDescription}</Box>
                        </GridItem>
                    }
                    <GridItem w={'100%'}>
                        <Box as="b">Show in the profile?</Box>
                    </GridItem>
                    <GridItem w={'100%'}>
                        <Box as={'span'}>{!religionData.showInProfile ? 'no' : 'yes'}</Box>
                    </GridItem>
                </Grid>
            }>

        </MainCard>
    )
}