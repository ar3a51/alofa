import { render } from '@testing-library/react';

import GenderSexualOrientation from './gender-sexual-orientation';

describe('GenderSexualOrientation', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<GenderSexualOrientation />);
    expect(baseElement).toBeTruthy();
  });
});
