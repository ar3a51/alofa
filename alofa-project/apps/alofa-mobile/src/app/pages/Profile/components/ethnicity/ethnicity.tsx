import { AlofaButton, AlofaDropdown, AlofaTextbox } from '@alofa/alofa-ui';
import { Box, Grid, GridItem, Heading, Stack } from '@chakra-ui/react';
import { useField } from '@formiz/core';
import ShowDetailsInProfile from '../../show-details-in-profile';
import styles from './ethnicity.module.scss';
import { commonStepProp } from '../../commonStepProperty';

function EthnicityDropdown(props: any) {
  const {value, setValue} = useField(props);

  return (
    <Box as='fieldset'>
      <label htmlFor=''>What's your ethnicity</label>
      <AlofaDropdown {...props} id='ethnicity-selection' name='ethnicity-selection'></AlofaDropdown>
    </Box>
  )

}

function Other(props: any) {
  const {value, setValue} = useField(props);

  return (
    <Box as='fieldset'>
      <label htmlFor=''>Other</label>
      <AlofaTextbox {...props} 
        id='ethnicity-selection' 
        name='ethnicity-selection'
        onChange={(e: any)=> setValue(e.target.value)}
        value={value ?? ''}
        ></AlofaTextbox>
    </Box>
  )

}
/* eslint-disable-next-line */

const paddingSize = '1.5em';

export function Ethnicity(props: commonStepProp) {
  return (
    <Grid templateColumns={'1fr'}>
        <GridItem w={'100%'} paddingTop={paddingSize} paddingBottom={paddingSize}>
            <Heading>Ethnicity</Heading>
        </GridItem>
        <GridItem w={'100%'} paddingTop={paddingSize} paddingBottom={paddingSize}>
            <EthnicityDropdown name='ethnicity.selection'/>
        </GridItem>
        <GridItem w={'100%'} paddingTop={paddingSize} paddingBottom={paddingSize}>
            <Other name='ethnicity.other'/>
        </GridItem>
        <GridItem w={'100%'}>
          <Box>
             <ShowDetailsInProfile name="ethnicity.showDobDetailsInProfile" 
                        onSkip={props.onSkip} 
                        showSkipButton={props.showSkipButton}
                    />
          </Box>
        </GridItem>
    </Grid>
  );
}

export default Ethnicity;
