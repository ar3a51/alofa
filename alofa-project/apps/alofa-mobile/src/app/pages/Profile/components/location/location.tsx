import { Formiz, useField, useForm} from '@formiz/core';
import { Box, Grid, GridItem, Heading } from '@chakra-ui/react';
import { AlofaButton, AlofaDropdown, AlofaSearchField, AlofaTextbox } from '@alofa/alofa-ui';
import ShowDetailsInProfile from '../../show-details-in-profile';
import { commonStepProp } from '../../commonStepProperty';
import { useEffect, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { RootState } from '@alofa/alofa-mobile-app';
import { setLocation } from '../../store/profile.store';
import { useNavigate } from 'react-router-dom';
import { environment } from '@alofa/alofa-mobile-env';


function ProvinceList(props: any) {
  const currForm = useForm();
  const {value, setValue} = useField(props);
  const [provinceDD, setProvinceDD] = useState<{
    data:Array<any>,
    idField: string,
    labelField: string,
    valueField: string
  }>({
    data:[],
    idField: '',
    labelField: '',
    valueField: ''
  });

  const [selectedProvince, setSelectedProvince] = useState('');
    
  useEffect(() => {

    async function getProvince() {
      const result = await fetch(`${environment.goApi}/regional/provinsi?api_key=${environment.goApiKey}`);
      const provData = await result.json();
      const defaultSel = [{id: '', name: 'Please select'}];

      setProvinceDD({
        data: defaultSel.concat(provData?.data) ?? [],
        idField: 'id',
        labelField: 'name',
        valueField: 'id'
      })

      //setValue(currForm.fields['province']?.value ?? provData.data[0]);
    }
    
    getProvince();
  },[])

  
 
  return(
    
      <Box as='fieldset'>
        <label htmlFor='province-list'>What is your province?</label>
        <AlofaDropdown 
          id={'province-list'} 
          name={'province-list'} 
          listInfo={provinceDD} 
          value={value?.id || ''} 
          onChange={(e)=>setValue({id: e.target.value, name: (provinceDD.data.filter((data)=> data.id === e.target.value)[0])?.name})}></AlofaDropdown>
      </Box>
  
  )
}

function MunicipalityList(props: any) {
  const currForm = useForm();
  const {value, setValue} = useField(props);
  const [municipalityDD, setMunicipalityDD] = useState<{
    data:Array<any>,
    idField: string,
    labelField: string,
    valueField: string
  }>({
    data:[],
    idField: '',
    labelField: '',
    valueField: ''
  });

  useEffect(()=> {
    async function getCities(provinceId: string) {
      const result = await fetch(`${environment.goApi}/regional/kota?api_key=${environment.goApiKey}&provinsi_id=${provinceId}`);
      const municipalityData = await result.json();
      const defaultSel = [{id: '', name: 'Please select'}]

      setMunicipalityDD({
        data: defaultSel.concat(municipalityData?.data) ?? [],
        idField: 'id',
        labelField: 'name',
        valueField: 'id'
      })

      //setValue(currForm.fields['municipality']?.value ?? municipalityData.data[0])
    }

    const province = currForm.fields['province']?.value;
    if (province)
      getCities(province.id);

  },[currForm.fields['province']?.value])

  return (
    <Box as='fieldset'>
      <label htmlFor='city-list'>What is your municipality?</label>
      <AlofaDropdown 
        id={'municipality-list'} 
        name={'municipality-list'} 
        listInfo={municipalityDD} value={value?.id || ''} 
        onChange={(e)=>setValue({id: e.target.value, name: (municipalityDD.data.filter((data)=> data.id === e.target.value)[0])?.name})}></AlofaDropdown>
      
    </Box>
  )

}

function SubDistrictList(props: any) {
  const currForm = useForm();
  const {value, setValue} = useField(props);
  const [subDistrictDD, setSubDistrictDD] = useState<{
    data:Array<any>,
    idField: string,
    labelField: string,
    valueField: string
  }>({
    data:[],
    idField: '',
    labelField: '',
    valueField: ''
  });

  useEffect(()=> {
    async function getCities(provinceId: string) {
      const result = await fetch(`${environment.goApi}/regional/kecamatan?api_key=${environment.goApiKey}&kota_id=${provinceId}`);
      const subDistrictData = await result.json();
      const defaultSel = [{id: '', name: 'Please select'}]

      setSubDistrictDD({
        data: defaultSel.concat(subDistrictData?.data) ?? [],
        idField: 'id',
        labelField: 'name',
        valueField: 'id'
      })

      //setValue(currForm.fields['subdistrict']?.value ?? subDistrictData.data[0])
    }

    const city = currForm.fields['municipality']?.value;
    console.log('city', city);
    if (city?.id !== '')
      getCities(city.id);

  },[currForm.fields['municipality']?.value])

  return (
    <Box as='fieldset'>
      <label htmlFor='subdistrict-list'>What is your subdistrict?</label>
      <AlofaDropdown 
        id={'subdistrict-list'} 
        name={'subdistrict-list'} 
        listInfo={subDistrictDD} value={value?.id || ''} 
        onChange={(e)=>setValue({id: e.target.value, name: (subDistrictDD.data.filter((data)=> data.id === e.target.value)[0])?.name})}></AlofaDropdown>
      
    </Box>
  )

}
/* eslint-disable-next-line */


const paddingSize = '1em';
export function Location(props: commonStepProp) {
  const locationForm = useForm();
  const dispatch = useDispatch();
  const locationData = useSelector((state:RootState)=> state.profile.location);
  const navigate = useNavigate();
  const initData = {
    province: locationData.province,
    municipality: locationData.municipality,
    subdistrict: locationData.subdistrict,
    showDetailsInProfile: locationData.showInProfile ?? false
  }

  function handleSubmit(e: any) {
    dispatch(setLocation(
      {
        province: e.province,
        municipality: e.municipality,
        subdistrict: e.subdistrict,
        showInProfile: e.showDetailsInProfile
      }
    ))
    navigate('/profile/review-profile');
  }

  return (
    <Formiz initialValues={initData} connect={locationForm} onSubmit={handleSubmit}>
      <Grid templateColumns={'1fr'} w={'100%'}>
        <GridItem w={'100%'}>
            <Heading>Your Location</Heading>
          </GridItem>
          <GridItem w={'100%'} paddingTop={paddingSize} paddingBottom={paddingSize}>
            <ProvinceList name="province"/>
          </GridItem>
          { 
            locationForm.fields['province']?.value?.id !=='' &&
            <GridItem w={'100%'} paddingTop={paddingSize} paddingBottom={paddingSize}>
              <MunicipalityList name="municipality"/>
            </GridItem>
          }
          {
            locationForm.fields['municipality']?.value &&
            <GridItem w={'100%'} paddingTop={paddingSize} paddingBottom={paddingSize}>
              <SubDistrictList name="subdistrict"/>
            </GridItem>
          }
          <GridItem w={'100%'}>
            <ShowDetailsInProfile name='showDetailsInProfile'
              onSave={()=>locationForm.submit()} 
              onSkip={props.onSkip} showSkipButton={props.showSkipButton}/>
        </GridItem>
      </Grid>
    </Formiz>
  );
}

export default Location;
