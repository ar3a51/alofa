import { AlofaButton, AlofaCheckbox, AlofaDropdown, AlofaTextbox } from "@alofa/alofa-ui";
import { Box, Heading, HStack, Stack } from "@chakra-ui/react";
import { Formiz, useField, useForm } from "@formiz/core";
import { useState } from "react";
import ShowDetailsInProfile from "../../show-details-in-profile";
import { commonStepProp } from "../../commonStepProperty";
import { isRequired } from "@formiz/validations";
import { useDispatch, useSelector } from "react-redux";
import { RootState } from "apps/alofa-mobile/src/app/store.typed";
import { setDob } from "../../store/profile.store";
import { useNavigate } from "react-router-dom";


function range(size:number, startAt = 0):ReadonlyArray<number> {
    return [...Array(size).keys()].map(i => i + startAt);
}

function PlaceOfBirthField(props: any){
    const {value, setValue, isValid} = useField(props);
    return(
        <Box as={'fieldset'} marginTop={'1em'}>
            <Box as={'label'} htmlFor={props.name}>Place of Birth</Box>
            <AlofaTextbox type="text" id={props.name} name={props.name}
                value={value || ''}
                onChange={(e: any)=> setValue(e.target.value)}
            />
        </Box>
    )
}

function DateOfBirthField(props: any) {
    const {value, setValue, isValid} = useField(props);
    const maxYear = (new Date()).getFullYear() - 18;
    const maxDate = `${maxYear}-01-01`;

    return (
    <Box as={'fieldset'} width={'100%'} marginTop={'10px'}>
        <input
            className="txt"
            type="date" 
            id={props.name} 
            name={props.name}
            max={maxDate}
            value={value ?? ''}
            onChange={(e)=>setValue(e.target.value)}/>
        {!isValid && <span>*mandatory</span>}
    </Box>
    )
}



export default function DateOfBirthForm(props: commonStepProp) {

    const dobForm = useForm()
    const dispatch = useDispatch();
    const navigate = useNavigate();
    const dobData = useSelector((state: RootState)=> state.profile.dob);
    const initData = {
        dateofbirth: dobData.dateOfBirth,
        placeOfBirth: dobData.placeOfBirth,
        showDobDetailsInProfile: dobData.showInProfile
    }

    function handleSubmit(e: any) {
       dispatch(setDob({
        dateOfBirth: e.dateofbirth,
        placeOfBirth: e.placeOfBirth,
        showInProfile: e.showDobDetailsInProfile
       }));

       navigate('/profile/gender');
    }

    return(
        <Formiz initialValues={initData} connect={dobForm} onSubmit={handleSubmit}>
            <Box height={'70vh'}>
                <Heading>What is your date of birth?</Heading>
                <HStack justifyContent={'stretch'}>
                  <DateOfBirthField name='dateofbirth' validations={[{rule: isRequired()}]}/>
                </HStack>
                <PlaceOfBirthField name='placeOfBirth' />
                <ShowDetailsInProfile name="showDobDetailsInProfile" 
                    onSkip={props.onSkip} 
                    showSkipButton={props.showSkipButton}
                    onSave={()=>dobForm.submit()}
                    disableSave={!dobForm.isValid}
                />
            </Box>
        </Formiz>
    )
}

