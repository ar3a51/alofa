import { Box, Heading, Stack } from '@chakra-ui/react';
import { AlofaDropdown, AlofaRadioButton, AlofaSearchField, AlofaCheckbox, AlofaButton, AlofaTextbox } from '@alofa/alofa-ui';
import styles from './gender-sexual-orientation.module.scss';
import { Formiz, useField, useForm } from '@formiz/core';
import ShowDetailsInProfile from '../../show-details-in-profile';
import { commonStepProp } from '../../commonStepProperty';
import { isRequired } from '@formiz/validations';
import { useDispatch, useSelector } from 'react-redux';
import { useNavigate } from 'react-router-dom';
import { RootState } from 'apps/alofa-mobile/src/app/store.typed';
import { setGender } from '../../store/profile.store';


function GenderSelection(props: any) {
  const {value, setValue} = useField(props);

  return (
    <>
     <Box className={styles['radio']}>
        <AlofaRadioButton 
          id='gender-selection-male' 
          name='gender-selection'
          label='Male'
          value={'male'}
          checked={value === 'male'}
          onChange={(e)=> e.target.checked ? setValue(e.target.value) : setValue('')}
        />
      </Box>
      <Box className={styles['radio']}>
        <AlofaRadioButton 
          id='gender-selection-female' 
          name='gender-selection'
          label='Female'
          value={'female'}
          checked={value === 'female'}
          onChange={(e)=> e.target.checked ? setValue(e.target.value) : setValue('')}
        />
      </Box>
      <Box className={styles['radio']}>
        <AlofaRadioButton 
          id='gender-selection-other' 
          name='gender-selection'
          label='Other'
          value={'other'}
          checked={value === 'other'}
          onChange={(e)=> e.target.checked ? setValue(e.target.value) : setValue('')}
        />
      </Box>
    </>
  )
}

function GenderIdentity(props: any) {
  const {value, setValue} = useField(props);

  return(
    <Box as='fieldset'>
      <label htmlFor='gender-identity'>What gender do you identity as?</label>
      <AlofaTextbox
        type='text'
        id={'gender-identity'} 
        name={'gender-identity'}
        value={value || ''}
        onChange={(e)=>setValue(e.target.value)}/>
    </Box>
  )
}


/* eslint-disable-next-line */
export interface GenderSexualOrientationProps {}

export function GenderSexualOrientation(props: commonStepProp) {
  const genderForm = useForm();
  const dispatch = useDispatch();
  const navigate = useNavigate();
  const genderData = useSelector((state: RootState)=> state.profile.gender);
  const initData = {
    identity: genderData.genderDescription,
    identityother: genderData.genderOtherDescription,
    showGenderInProfile: genderData.showInProfile
  }
  
  function handleSubmit(e: any) {
    dispatch(setGender({
      genderDescription: e.identity,
      genderOtherDescription: e.identityother,
      showInProfile: e.showGenderInProfile
    }))

    navigate('/profile/location');
  }
  return (
    <Formiz initialValues={initData} connect={genderForm} onSubmit={handleSubmit}>
      <Box as={'div'} className={styles['container']}>
          <Box as={'div'} className={styles['field']}>
            <Heading>Gender/Sexual Orientation</Heading>
          </Box>
          <Box as={'div'} className={styles['field']}>
            <GenderSelection name='identity' validations={[{rule: isRequired()}]} />
          </Box>
          {
            genderForm.fields['identity']?.value === 'other' &&
            <Box as={'div'} className={styles['field']}>
              <GenderIdentity name='identityother' validations={[{rule: isRequired()}]} />
            </Box>
          }
          <Box>
            <ShowDetailsInProfile 
              name='showGenderInProfile' 
              onSave={()=>genderForm.submit()} 
              onSkip={props.onSkip} 
              showSkipButton={props.showSkipButton}
              disableSave={!genderForm.isValid}
            />
          </Box>
      </Box>
   </Formiz>
  );
}

export default GenderSexualOrientation;
