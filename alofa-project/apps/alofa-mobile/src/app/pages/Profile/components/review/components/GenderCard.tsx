import { RootState } from "@alofa/alofa-mobile-app";
import { useSelector } from "react-redux";
import MainCard from "./MainCard";
import { Box, Grid, GridItem } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

export default function GenderCard() {
    const genderData = useSelector((state: RootState)=> state.profile.gender);
    const navigate = useNavigate();

    const editGender = () => {
        navigate('/profile/gender');
    }

    return (
        <MainCard 
            title={'Gender/Sexual Orientation'}
            onClick={editGender}
            body={
                <Grid templateColumns={'1fr'} w={'100%'}>
                    <GridItem w={'100%'}>
                        <Box as={"b"}>How are you identify as?</Box>
                    </GridItem>
                    <GridItem w={'100%'} paddingTop={'1em'} paddingBottom={'1em'}>
                        <Box as={'span'}>{genderData.genderDescription}</Box>
                    </GridItem>
                    {
                         genderData.genderDescription === 'other' && 
                        <GridItem w={'100%'}>
                            <Box as={"b"}>Please specify</Box>
                        </GridItem>
                    }
                    {
                         genderData.genderDescription === 'other' &&
                        <GridItem w={'100%'} paddingTop={'1em'} paddingBottom={'1em'}>
                            <Box as={'span'}>{genderData.genderOtherDescription}</Box>
                        </GridItem>
                    }
                    <GridItem w={'100%'}>
                        <Box as="b">Show in the profile?</Box>
                    </GridItem>
                    <GridItem w={'100%'}>
                        <Box as={'span'}>{!genderData.showInProfile ? 'no' : 'yes'}</Box>
                    </GridItem>
                </Grid>
            }>

        </MainCard>
    )
}