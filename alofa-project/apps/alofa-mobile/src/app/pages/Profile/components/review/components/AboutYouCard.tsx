import { RootState } from "@alofa/alofa-mobile-app";
import { useSelector } from "react-redux";
import MainCard from "./MainCard";
import { Box, Grid, GridItem } from "@chakra-ui/react";
import { useNavigate } from "react-router-dom";

export default function AboutUsCard() {
    const aboutYouData = useSelector((state: RootState)=> state.profile.aboutYou);
    const navigate = useNavigate();

    const editAboutYou = () => {
        navigate('/profile/');
    }

    return (
        <MainCard 
            title={'About You'}
            onClick={editAboutYou}
            body={
                <Grid templateColumns={'1fr'} w={'100%'}>
                    <GridItem w={'100%'}>
                        <Box as={"b"}>Share a little about yourself</Box>
                    </GridItem>
                    <GridItem w={'100%'} paddingTop={'1em'} paddingBottom={'1em'}>
                        <Box as={'p'}>{aboutYouData.description}</Box>
                    </GridItem>
                    <GridItem w={'100%'}>
                        <Box as="b">Show in the profile?</Box>
                    </GridItem>
                    <GridItem w={'100%'}>
                        <Box as={'span'}>{!aboutYouData.showInProfile ? 'no' : 'yes'}</Box>
                    </GridItem>
                </Grid>
            }>

        </MainCard>
    )
}