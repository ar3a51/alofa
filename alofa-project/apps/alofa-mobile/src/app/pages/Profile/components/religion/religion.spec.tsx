import { render } from '@testing-library/react';

import Religion from './religion';

describe('Religion', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<Religion />);
    expect(baseElement).toBeTruthy();
  });
});
