import { useField } from '@formiz/core';
import { Box, Grid, GridItem, Heading } from '@chakra-ui/react';
import { AlofaTextbox } from '@alofa/alofa-ui';
import ShowDetailsInProfile from '../../show-details-in-profile';
import { commonStepProp } from '../../commonStepProperty';
import { isRequired } from '@formiz/validations';
import {AlofaMandatoryMark} from '@alofa/alofa-mobile-ui';

function FirstName(props: any){
  const {value, isValid, setValue} = useField(props);

  return(
    <Box as='fieldset'>
      <label htmlFor='first-name'>What's your first name? {!isValid && <AlofaMandatoryMark/>}</label>
      <AlofaTextbox {...props} 
        id='first-name' 
        name='first-name'
        onChange={(e:any)=>setValue(e.target.value)}
        value={value ?? ''}
        isvalidstate={isValid}
        ></AlofaTextbox>
    </Box>
  )
}

function MiddleName(props: any){
  const {value, isValid, setValue} = useField(props);

  return(
    <Box as='fieldset'>
      <label htmlFor='middle-name'>What's your middle name (if any)?</label>
      <AlofaTextbox {...props} 
        id='middle-name' 
        name='middle-name'
        onChange={(e:any)=>setValue(e.target.value)}
        value={value ?? ''}
        ></AlofaTextbox>
        {!isValid && <span>mandatory</span>}
    </Box>
  )
}

function LastName(props: any){
  const {value,isValid, setValue} = useField(props);

  return(
    <Box as='fieldset'>
      <label htmlFor='last-name'>What's your last name? {!isValid && <AlofaMandatoryMark/>}</label>
      <AlofaTextbox {...props} 
        id='last-name' 
        name='last-name'
        onChange={(e:any)=>setValue(e.target.value)}
        value={value ?? ''}
        isvalidstate={isValid}
        ></AlofaTextbox>
    </Box>
  )
}

/* eslint-disable-next-line */
const paddingSize = '1em';

export function Name(props: commonStepProp) {
  return (
    <Grid templateColumns={'1fr'} w={'100%'}>
    <GridItem w={'100%'}>
      <Heading>Let's start with your name</Heading>
    </GridItem>
    <GridItem w={'100%'} paddingTop={paddingSize} paddingBottom={paddingSize}>
      <FirstName name='name.firstname' validations={[{rule: isRequired()}]}/>
    </GridItem>
    <GridItem w={'100%'} paddingTop={paddingSize} paddingBottom={paddingSize}>
      <MiddleName name='name.middlename'/>
    </GridItem>
    <GridItem w={'100%'} paddingTop={paddingSize} paddingBottom={paddingSize}>
      <LastName name='name.lastname' validations={[{rule: isRequired()}]}/>
    </GridItem>
    <GridItem w={'100%'}>
      <ShowDetailsInProfile name='name.showDetailsInProfile' 
        onSkip={props.onSkip} 
        showSkipButton={props.showSkipButton}/>
    </GridItem>
 </Grid>
  );
}

export default Name;
