import { EditIcon } from "@chakra-ui/icons";
import { Card, CardBody, CardHeader, Heading } from "@chakra-ui/react";


export default function MainCard(props: any) {

    return (
        <Card>
            <CardHeader>
                <Heading as={'h3'} color={"alofa.primaryRed"} display={'flex'} justifyContent={'space-between'}>
                   {props.title}
                   <EditIcon onClick={props.onClick}/>
                </Heading>
            </CardHeader>
            <CardBody>
                {props.body}
            </CardBody>
        </Card>
    )
}