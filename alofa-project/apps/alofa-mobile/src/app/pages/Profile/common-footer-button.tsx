import { Box, Stack } from "@chakra-ui/react";
import { commonStepProp } from "./commonStepProperty";
import { AlofaButton } from "@alofa/alofa-ui";

function CommonFooterButton(props: commonStepProp){
    return (
        <Stack position={'fixed'}
                        left={'0'}
                        right={'0'}
                        flexDirection={'column-reverse'} 
                        bottom='0' 
                        width={'100%'}
                        alignItems={'center'}
                        justify="center"
                        >
            <Box p={'2'}><AlofaButton>Save & Continue</AlofaButton></Box>
            (
             if (props.showSkipButton === true)
                    <Box p={'2'}><AlofaButton isPrimary={false} onClick={props.onSkip}>Skip</AlofaButton></Box>
            )
        </Stack>
    )
}

export default CommonFooterButton;