import { Route, Routes } from "react-router-dom";
import AlofaMobileProfile from "./alofa-mobile-profile";
import React from "react";
import LoadingFallback from "../../Loading-fallback";

const AboutYouForm = React.lazy(()=> import('./components/About-You/alofa-mobile-aboutyou'));
const DateOfBirthForm = React.lazy(()=> import('./components/Date-of-birth/alofa-mobile-dob'));
const GenderForm = React.lazy(()=> import('./components/gender-sexual-orientation/gender-sexual-orientation'));
const ReligionForm = React.lazy(()=> import('./components/religion/religion'));
const LocationForm = React.lazy(()=> import('./components/location/location'));
const Review = React.lazy(()=> import('./components/review/review'));


export default function AlofaProfileRouting() {
    return (
        <Routes>
            <Route path="/" element={<AlofaMobileProfile/>}>
                <Route index element={
                    <React.Suspense fallback={<LoadingFallback/>}>
                        <AboutYouForm/>
                    </React.Suspense>
                }/>
                <Route path='/date-of-birth' element={
                    <React.Suspense fallback={<LoadingFallback/>}>
                        <DateOfBirthForm/>
                    </React.Suspense>
                }/>
                 <Route path='/gender' element={
                    <React.Suspense fallback={<LoadingFallback/>}>
                        <GenderForm/>
                    </React.Suspense>
                }/>
                <Route path='/religion' element={
                    <React.Suspense fallback={<LoadingFallback/>}>
                        <ReligionForm/>
                    </React.Suspense>
                }/>
                <Route path='/location' element={
                    <React.Suspense fallback={<LoadingFallback/>}>
                        <LocationForm/>
                    </React.Suspense>
                }/>
                  <Route path='/review-profile' element={
                    <React.Suspense fallback={<LoadingFallback/>}>
                        <Review/>
                    </React.Suspense>
                }/>
            </Route>
        </Routes>
    )
} 