export interface commonStepProp {
    onSkip?: (e: any)=>void;
    showSkipButton?: boolean;
}