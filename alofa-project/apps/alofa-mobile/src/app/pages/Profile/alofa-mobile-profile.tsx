import { Flex } from "@chakra-ui/react";
import { useForm } from "@formiz/core";
import { Field, GroupField } from "@alofa/generated/members-types";
import { AlofaMobileHeader } from "@alofa/alofa-mobile-ui";
import { useSaveProfileMutation } from "./profileApiSlice";
import { Outlet } from "react-router-dom";


export default function AlofaMobileProfile(){
    const profileForm = useForm();
    const [saveProfile] = useSaveProfileMutation();

    const init = {}
    const groupFields: GroupField[] = [];

    async function handleFormSubmit(e: any){
        console.log(e);
        
       Object.keys(e).forEach(key => {
        const grpField: GroupField = {
            groupName: key,
            fields: [],
            showInProfile: false
        }
        Object.keys(e[key]).forEach(field => {
            const fieldDetails: Field = {
                fieldName: field,
                values: []
            };
            const values = e[key][field];
            fieldDetails.values?.push(values)

            grpField.fields?.push(fieldDetails);
        })
        groupFields.push(grpField);
       })

       console.log(JSON.stringify(groupFields));
       const result = await saveProfile(groupFields).unwrap();
       console.log(result);
    }

    function handleOnSkip(e: any) {
        e.preventDefault();
        profileForm.nextStep();
    }

    function handleBackButtonClick(e: any){
        e.preventDefault();
        profileForm.prevStep();
    }

    return (
        <>
            <AlofaMobileHeader onClick={handleBackButtonClick} displayBackButton={!profileForm.isFirstStep}></AlofaMobileHeader>
            <Flex flexDirection={'column'} p={'1em'} width={'100%'} position='relative'>
                <Outlet/>
                {/*
                <Formiz connect={profileForm} onSubmit={handleFormSubmit}
                initialValues={init}
                >
                    <form noValidate onSubmit={profileForm.submitStep}>
                        <FormizStep name="nameStep">
                            <Name onSkip={handleOnSkip} showSkipButton={!profileForm.isLastStep}/>
                        </FormizStep>
                        <FormizStep name="abtYouStep">
                            <AboutYouForm onSkip={handleOnSkip} showSkipButton={!profileForm.isLastStep}/>
                        </FormizStep>
                        <FormizStep name="dobStep">
                            <DateOfBirthForm onSkip={handleOnSkip} showSkipButton={!profileForm.isLastStep}/>
                        </FormizStep>
                        <FormizStep name="genderStep">
                            <GenderSexualOrientation onSkip={handleOnSkip} showSkipButton={!profileForm.isLastStep}/>
                        </FormizStep>
                        <FormizStep name="ethnicityStep">
                            <Ethnicity onSkip={handleOnSkip} showSkipButton={!profileForm.isLastStep}/>
                        </FormizStep>
                        <FormizStep name="physicalStep">
                            <Physical onSkip={handleOnSkip} showSkipButton={!profileForm.isLastStep}/>
                        </FormizStep>
                        <FormizStep name="religionStep">
                            <Religion onSkip={handleOnSkip} showSkipButton={!profileForm.isLastStep}/>
                        </FormizStep>
                        <FormizStep name="locationStep">
                            <Location onSkip={handleOnSkip} showSkipButton={!profileForm.isLastStep}/>
                        </FormizStep>
                    </form>
                </Formiz>
    */}
            </Flex>
        </>
    )
}