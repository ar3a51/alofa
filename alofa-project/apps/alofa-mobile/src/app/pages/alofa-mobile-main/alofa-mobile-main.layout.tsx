import { Outlet, useNavigate } from 'react-router-dom';
import AlofaBottomNav from './components/alofa-bottom-nav/alofa-bottom-nav';
import AlofaHeader from './components/alofa-header/alofa-header';
import { Box } from '@chakra-ui/react';

export default function AlofaMobileLayout() {
  const navigate = useNavigate();

  const handleBackBtnClick = () => {
    navigate(-1);
  };

  const handleSettingClick = () => {
    navigate('#');
  };

  return (
    <Box as={'div'}
      style={{
        width: '100%',
        height: '100%',
        minHeight: '100dvh',
        paddingTop: '1rem',
        paddingBottom: '1rem',
        display: 'flex',
        flexDirection: 'column',
        boxSizing: 'border-box',
        justifyContent: 'space-between',
      }}
    >
      {/*<AlofaHeader backBtnOnClick={handleBackBtnClick}>
        <button aria-label="settings" onClick={handleSettingClick}>
          <img
            style={{ opacity: 0.7 }}
            src="/assets/icons/Navigation/Setting_line.png"
            alt="setting"
          />
        </button>
    </AlofaHeader>*/}
      <Outlet />
      <AlofaBottomNav />
    </Box>
  );
}
