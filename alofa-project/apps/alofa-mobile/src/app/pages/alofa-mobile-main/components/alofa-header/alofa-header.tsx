import { Box, Image, Center, IconButton } from '@chakra-ui/react';
import { ChevronLeftIcon } from '@chakra-ui/icons';
import { ReactNode } from 'react';

/* eslint-disable-next-line */
export interface AlofaHeaderProps {
  children: ReactNode;
  backBtnOnClick?: any;
}

/**
 * The main header for the user navigation
 *
 * @component
 * @param props - The components props
 * @param props.children - React nodes / icon buttons to be renders on the right side
 * @param props.backBtnOnClick - The optional onClick function for the back button. If none is provided, the back button is not rendered.
 * @returns A JSX element with the logo and conditional left/right icons
 */
export function AlofaHeader(props: AlofaHeaderProps) {
  return (
    <Box
      as="nav"
      width="100%"
      sx={{ display: 'flex', justifyContent: 'space-between' }}
      px={'20px'}
    >
      {props.backBtnOnClick && (
        <IconButton
          aria-label="back"
          variant="ghost"
          size="xs"
          onClick={props.backBtnOnClick}
          icon={<ChevronLeftIcon boxSize={8} />}
        />
      )}
      <Center>
        <Image
          h={'25px'}
          w={'72px'}
          src="/assets/icons/other/alofa-logo.svg"
          alt="alofa logo"
        />
      </Center>
      {props.children}
    </Box>
  );
}

export default AlofaHeader;
