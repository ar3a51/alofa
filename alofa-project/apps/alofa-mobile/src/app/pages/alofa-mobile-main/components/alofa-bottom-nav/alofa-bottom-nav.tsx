import styles from './alofa-bottom-nav.module.scss';
import { ReactNode } from 'react';
import { useLocation, useNavigate } from 'react-router-dom';
import { HStack, Image, Button, Text, Box, VStack } from '@chakra-ui/react';
import classNames from 'classnames';

interface NavButtonProps {
  icon: ReactNode;
  buttonText: string;
  onClick: any;
}

/**
 * @component
 * @param props - The component props
 * @param props.icon - The icon of the button
 * @param props.buttonText - The text of the button
 * @param props.onClick - The onclick handler for the button
 * @returns A navigation button with an icon
 */
function NavButton(props: NavButtonProps) {
  const location = useLocation();
  // getting currentPage to conditionally style the respected icon
  const currentPage = location.pathname;

  return (
    <Button
      className={styles['nav-button']}
      onClick={props.onClick}
      height="43px"
      display="flex"
      flexDirection="column"
      alignItems="center"
      justifyContent="center"
      variant="ghost"
      sx={{ flex: 1 }}
    >
      <VStack spacing={1}>
        <Box
          className={classNames({
            [styles['_selected']]: currentPage.includes(
              props.buttonText.toLowerCase()
            ),
          })}
          display="flex"
          justifyContent="center"
        >
          {props.icon}
        </Box>
        <Text fontSize="sm" sx={{ fontWeight: '400' }}>
          {props.buttonText}
        </Text>
      </VStack>
    </Button>
  );
}

/* eslint-disable-next-line */
interface AlofaBottomNavProps {}

/**
 * Renders the bottom navigation component for the user's main page
 * @component
 * @param props - The component props
 * @returns A JSX component for the user's bottom navigation.
 */
export function AlofaBottomNav(props: AlofaBottomNavProps) {
  const navigate = useNavigate();

  const handleDiscoveryClick = () => {
    navigate('#');
  };

  const handleMatchesClick = () => {
    navigate('#');
  };
  const handleMessagesClick = () => {
    navigate('#');
  };
  const handleProfileClick = () => {
    navigate('/main/user-profile');
  };

  return (
    <HStack
      justifyContent="space-between"
      width="100%"
      sx={{
        marginTop: 'auto',
        paddingTop: '1rem',
      }}
    >
      <NavButton
        icon={
          <Image
            src="/assets/icons/Navigation/Discover.png"
            alt="discovery button"
          />
        }
        onClick={handleDiscoveryClick}
        buttonText="Discovery"
      />
      <NavButton
        icon={
          <Image
            src="/assets/icons/Navigation/Matches.png"
            alt="discovery button"
          />
        }
        onClick={handleMatchesClick}
        buttonText="Matches"
      />
      <NavButton
        icon={
          <Image
            src="/assets/icons/Connect/Messages.png"
            alt="discovery button"
          />
        }
        onClick={handleMessagesClick}
        buttonText="Messages"
      />
      <NavButton
        icon={
          <Image
            src="/assets/icons/Navigation/Profile.png"
            alt="discovery button"
          />
        }
        onClick={handleProfileClick}
        buttonText="Profile"
      />
    </HStack>
  );
}

export default AlofaBottomNav;
