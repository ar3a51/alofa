import { render } from '@testing-library/react';

import AlofaBottomNav from './alofa-bottom-nav';

describe('AlofaBottomNav', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AlofaBottomNav />);
    expect(baseElement).toBeTruthy();
  });
});
