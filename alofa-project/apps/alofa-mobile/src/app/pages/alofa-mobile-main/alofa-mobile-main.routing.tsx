import React from 'react';
import { Route, Routes } from 'react-router-dom';
import LoadingFallback from '../../Loading-fallback';
import AlofaMobileLayout from './alofa-mobile-main.layout';
import UploadPhoto from './user-profile/alofa-upload-photo/upload-photo';

const ProfilePage = React.lazy(() => import('../Profile/alofa-mobile-profile'));
const MessengerPage = React.lazy(() => import('./messenger/messenger'));
const ChatPage = React.lazy(() => import('./messenger/ChatPage/ChatPage'));

export default function AlofaMobileMainRouting() {
    return (
        <Routes>
            <Route path="/" element={<AlofaMobileLayout/>}>
                <Route index element={
                    <React.Suspense fallback={<LoadingFallback/>}>
                    </React.Suspense>
                }/>

                <Route path="/user-profile" element={
                    <React.Suspense fallback={<LoadingFallback/>}>
                        <UserProfile></UserProfile>
                    </React.Suspense>
                }/>

                <Route path="/upload-photo" element={
                    <React.Suspense fallback={<LoadingFallback/>}>
                        <UploadPhoto></UploadPhoto>
                    </React.Suspense>
                }/>
            </Route>
        </Routes>
    )
}