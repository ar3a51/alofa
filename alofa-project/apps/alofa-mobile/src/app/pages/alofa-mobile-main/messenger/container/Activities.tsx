import { Box, Heading, Stack } from '@chakra-ui/react';
import ProfileActivity from '../components/ProfileActivity';
import { users } from '../utils/testArrays';

const Activities = () => {
  return (
    <Box>
      <Heading size="4xl" fontSize={20} mt="1.2rem" mb="1rem">
        Activities
      </Heading>
      <Stack direction="row" overflowX="scroll">
        {users.map((user) => (
          <ProfileActivity
            key={user.id}
            id={user.id}
            name={user.name}
            story={user.story}
            url={user.image}
          />
        ))}
      </Stack>
    </Box>
  );
};

export default Activities;
