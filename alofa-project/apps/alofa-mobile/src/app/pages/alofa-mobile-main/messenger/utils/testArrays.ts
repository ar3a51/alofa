// Arrays made for testing the components

export const chats = [
  {
    image:
      'https://t3.ftcdn.net/jpg/02/55/17/02/360_F_255170294_rQkbs2TgJYk2Oni3Wa1yJQbU7k48OnI5.jpg',
    user: 'Natasha',
    time: '2024-02-20 7:30',
    hasMessages: { hasMsg: true, numOfMsg: 3 },
    isTyping: true,
    lastMsg: { isMine: true, message: 'Hey how are you?' },
    story: true,
    id: 1,
  },
  {
    image:
      'https://t3.ftcdn.net/jpg/02/55/17/02/360_F_255170294_rQkbs2TgJYk2Oni3Wa1yJQbU7k48OnI5.jpg',
    user: 'Natasha',
    time: '2024-01-14 10:31',
    hasMessages: { hasMsg: false, numOfMsg: 0 },
    isTyping: false,
    lastMsg: {
      isMine: true,
      message: "What's up? do you wanna go to a party with me?",
    },
    story: false,
    id: 2,
  },
  {
    image:
      'https://t3.ftcdn.net/jpg/02/55/17/02/360_F_255170294_rQkbs2TgJYk2Oni3Wa1yJQbU7k48OnI5.jpg',
    user: 'Natasha',
    time: '2024-02-20 08:30',
    hasMessages: { hasMsg: true, numOfMsg: 3 },
    isTyping: false,
    lastMsg: {
      isMine: false,
      message: 'Hey how are you? what did you do last week',
    },
    story: true,
    id: 3,
  },
];

export const users = [
  {
    id: 1,
    name: 'Janna',
    image:
      'https://t3.ftcdn.net/jpg/02/55/17/02/360_F_255170294_rQkbs2TgJYk2Oni3Wa1yJQbU7k48OnI5.jpg',
    story: true,
  },
  {
    id: 2,
    name: 'Ana',
    image:
      'https://plus.unsplash.com/premium_photo-1664267832588-650cc54fcea6?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MXx8YmVhdXRpZnVsJTIwd29tZW58ZW58MHx8MHx8fDA=',
    story: true,
  },
  {
    id: 3,
    name: 'Paul',
    image:
      'https://img.freepik.com/premium-photo/rugged-manly-studio-shot-handsome-man-against-gray-background_590464-19912.jpg?size=626&ext=jpg',
    story: false,
  },
  {
    id: 4,
    name: 'John',
    image:
      'https://t4.ftcdn.net/jpg/01/15/85/23/360_F_115852367_E6iIYA8OxHDmRhjw7kOq4uYe4t440f14.jpg',
    story: false,
  },
  {
    id: 5,
    name: 'Mark',
    image:
      'https://burst.shopifycdn.com/photos/professional-man-portrait.jpg?width=1000&format=pjpg&exif=0&iptc=0',
    story: true,
  },
  {
    id: 6,
    name: 'Stephanie',
    image:
      'https://images.unsplash.com/photo-1634055739897-b6a98a80114a?q=80&w=1000&auto=format&fit=crop&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8MTh8fGJlYXV0aWZ1bCUyMHdvbWFufGVufDB8fDB8fHww',
    story: false,
  },
];

export default { chats, users };
