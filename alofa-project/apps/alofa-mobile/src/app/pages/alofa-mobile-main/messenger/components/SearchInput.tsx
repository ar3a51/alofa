import { SearchIcon } from '@chakra-ui/icons';
import { Input, InputGroup, InputLeftElement } from '@chakra-ui/react';

const SearchInput = () => {
  return (
    <InputGroup>
      <InputLeftElement pointerEvents="none">
        <SearchIcon color="gray.300" />
      </InputLeftElement>
      <Input
        size="md"
        type="tel"
        placeholder="Search"
        focusBorderColor="#E9405799"
      />
    </InputGroup>
  );
};

export default SearchInput;
