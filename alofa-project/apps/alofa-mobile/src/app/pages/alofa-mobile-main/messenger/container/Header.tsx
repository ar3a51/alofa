import { Heading, Flex, Box } from '@chakra-ui/react';
import { DropDown } from '../components/DropDownMenu';
import SearchInput from '../components/SearchInput';

export function Header({ setDesc }: any) {
  return (
    <Box>
      <Flex w="100%" align="center" justify="space-between" mb="1rem">
        <Heading size="2xl" fontSize={26} letterSpacing="-1px">
          Messages
        </Heading>
        <DropDown setDesc={setDesc} />
      </Flex>
      <SearchInput />
    </Box>
  );
}

export default Header;
