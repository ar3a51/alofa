import {
  Menu,
  MenuButton,
  MenuList,
  IconButton,
  MenuOptionGroup,
  MenuItemOption,
  MenuDivider,
} from '@chakra-ui/react';

import { HamburgerIcon } from '@chakra-ui/icons';

export function DropDown({ setDesc }: any) {
  return (
    <Menu>
      <MenuButton
        as={IconButton}
        border="1px"
        borderColor="gray.200"
        color="#E94057"
        bg="white"
        transition="all 0.2s"
        icon={<HamburgerIcon />}
      />
      <MenuList minWidth="240px">
        <MenuOptionGroup defaultValue="asc" title="Order" type="radio">
          <MenuItemOption value="rec" onClick={() => setDesc(true)}>
            Recent
          </MenuItemOption>
          <MenuItemOption value="lat" onClick={() => setDesc(false)}>
            Latest
          </MenuItemOption>
        </MenuOptionGroup>
      </MenuList>
    </Menu>
  );
}

export default DropDown;
