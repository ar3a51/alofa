import { useState } from 'react';
import { Box } from '@chakra-ui/react';

import styles from './messenger.module.scss';
import Header from './container/Header';
import Activities from './container/Activities';
import Messages from './container/Messages';

export function Messenger() {
  const [desc, setDesc] = useState(true);
  return (
    <Box className={styles['messenger']} py="15px" px="25px">
      <Header setDesc={setDesc} />
      <Activities />
      <Messages desc={desc} />
    </Box>
  );
}

export default Messenger;
