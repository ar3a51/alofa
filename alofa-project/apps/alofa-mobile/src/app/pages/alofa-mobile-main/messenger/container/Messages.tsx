import { Box, Flex, Heading } from '@chakra-ui/react';
import ContactChat from '../components/ContactChat';
import { chats } from '../utils/testArrays';

export function Messages({ desc }: any) {
  if (desc)
    chats.sort(
      (a, b) => new Date(b.time).getTime() - new Date(a.time).getTime()
    );
  else
    chats.sort(
      (a, b) => new Date(a.time).getTime() - new Date(b.time).getTime()
    );

  return (
    <Box mt="1rem">
      <Heading size="4xl" fontSize={20} mb="1rem">
        Messages
      </Heading>
      <Flex direction="column" gap="1rem">
        {chats.map((chat, i) => (
          <ContactChat
            key={chat.id}
            id={chat.id}
            image={chat.image}
            user={chat.user}
            time={chat.time}
            hasMessages={chat.hasMessages}
            isTyping={chat.isTyping}
            lastMsg={chat.lastMsg}
            story={chat.story}
          />
        ))}
      </Flex>
    </Box>
  );
}

export default Messages;
