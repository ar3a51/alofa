import { AlofaPhoto } from '@alofa/alofa-ui';
import { Box, Stack, Text } from '@chakra-ui/react';

interface ProfileActivityProps {
  url: string;
  name: string;
  story: boolean;
  id: number;
}

const ProfileActivity = ({ url, name, story, id }: ProfileActivityProps) => {
  return (
    <Stack
      id={`profile-activity-${id}`}
      direction="column"
      display="flex"
      flexDir="column"
      alignItems="center"
      mb="7px"
    >
      <Box
        boxSize="80px"
        rounded="full"
        padding="2px"
        border={story ? '2px solid #CB3949' : 'none'}
        mb="-0.5rem"
      >
        <AlofaPhoto isRounded={true} url={url} alt="Profile picture" />
      </Box>
      <Text textAlign="center" fontWeight={1000}>
        {name}
      </Text>
    </Stack>
  );
};

export default ProfileActivity;
