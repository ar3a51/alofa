export function getTimeDifference(time: string): string {
  const actualTime = new Date();
  const formattedTime = new Date(time);

  const timeDifferenceInMilliseconds =
    actualTime.getTime() - formattedTime.getTime();

  const millisecondsPerDay = 1000 * 60 * 60 * 24;
  const millisecondsPerHour = 1000 * 60 * 60;
  const millisecondsPerMinute = 1000 * 60;

  const days = Math.floor(timeDifferenceInMilliseconds / millisecondsPerDay);
  const remainingHours = Math.floor(
    (timeDifferenceInMilliseconds % millisecondsPerDay) / millisecondsPerHour
  );
  const remainingMinutes = Math.floor(
    (timeDifferenceInMilliseconds % millisecondsPerHour) / millisecondsPerMinute
  );

  let finalTime = '';

  if (days > 30) {
    finalTime = '30+ days';
  } else {
    if (days > 0) finalTime = `${days} days`;
    else if (remainingHours > 0) finalTime = `${remainingHours} hours`;
    else if (remainingMinutes > 0) finalTime = `${remainingMinutes} mins`;
    else finalTime = '1 min';
  }

  return finalTime;
}

export default { getTimeDifference };
