import { Box } from '@chakra-ui/react';
import { useParams } from 'react-router-dom';

const ChatPage = () => {
  const { id } = useParams();
  return <Box>ChatPage {id}</Box>;
};

export default ChatPage;
