import { AlofaPhoto } from '@alofa/alofa-ui';
import { Box, Flex, Heading, Text } from '@chakra-ui/react';
import { getTimeDifference } from '../utils/functions';
import { useNavigate } from 'react-router-dom';
import styles from '../messenger.module.scss';
import { chats } from '../utils/testArrays';

interface LastMsg {
  isMine: boolean;
  message: string;
}
interface HasMessages {
  hasMsg: boolean;
  numOfMsg: number;
}
interface ContactChatProps {
  id: number;
  image: string;
  user: string;
  time: string;
  hasMessages: HasMessages;
  isTyping: boolean;
  lastMsg: LastMsg;
  story: boolean;
}

function ContactChat({
  id,
  image,
  user,
  time,
  hasMessages,
  isTyping,
  lastMsg,
  story,
}: ContactChatProps) {
  const timeAgo = getTimeDifference(time);

  const navigate = useNavigate();

  const handleClick = () => {
    navigate(`/main/messenger/chat/${id}`);
  };

  return (
    <Flex align="center" gap="8px">
      <Box
        boxSize="60px"
        rounded="full"
        padding="2px"
        border={story ? '2px solid #CB3949' : 'none'}
      >
        <AlofaPhoto url={image} alt={`${user}-photo`} isRounded={true} />
      </Box>
      {/* User name and chat last message */}
      <Flex
        style={
          id !== chats[chats.length - 1].id
            ? { paddingBottom: '10px', borderBottom: '1px solid #ddd' }
            : {}
        }
        justify="space-between"
        align="center"
        flex={4}
        onClick={handleClick}
      >
        <Flex direction="column" justify="space-between" align="start" flex={3}>
          <Heading size="4xl" fontSize={15}>
            {user}
          </Heading>
          <Box>
            {isTyping ? (
              <Text fontSize={14}>Typing...</Text>
            ) : lastMsg.isMine ? (
              <Flex gap="5px">
                <Text fontSize={14} color={'#999'}>
                  You:
                </Text>
                <Text fontSize={14}>
                  {lastMsg.message.length > 19
                    ? `${lastMsg.message.slice(0, 19)}...`
                    : lastMsg.message}
                </Text>
              </Flex>
            ) : (
              <Text fontSize={14}>
                {lastMsg.message.length > 24
                  ? `${lastMsg.message.slice(0, 24)}...`
                  : lastMsg.message}
              </Text>
            )}
          </Box>
        </Flex>
        <Flex direction="column" align="end" flex="1">
          <Text fontSize={13}>{timeAgo}</Text>
          {hasMessages.hasMsg ? (
            <Box
              display="flex"
              justifyContent="center"
              alignItems="center"
              borderRadius="full"
              bgColor={'#CB3949'}
              padding={3}
              boxSize="24px"
            >
              <Text
                color={'#fff'}
                boxSize="fit-content"
                fontSize={12}
                textAlign="center"
              >
                {hasMessages.numOfMsg}
              </Text>
            </Box>
          ) : (
            <Text></Text>
          )}
        </Flex>
      </Flex>
    </Flex>
  );
}

export default ContactChat;
