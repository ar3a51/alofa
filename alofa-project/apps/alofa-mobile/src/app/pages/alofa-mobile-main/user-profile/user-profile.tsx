import { AlofaButton, AlofaPhoto } from '@alofa/alofa-ui';
import styles from './user-profile.module.scss';
import { useNavigate } from 'react-router-dom';
import { Box, Heading } from '@chakra-ui/react';
import AlofaHeader from '../components/alofa-header/alofa-header';

/* eslint-disable-next-line */
export interface UserProfileProps {}

const mockData = {
  displayName: 'Raul',
  age: 32,
  imageUrl:
    'https://plus.unsplash.com/premium_photo-1664540415069-bc45ce3e711e?w=300&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NXx8cG9ydHJhaXQlMjBtYW58ZW58MHx8MHx8fDA%3D',
};

export function UserProfile(props: UserProfileProps) {
  const navigate = useNavigate();

  const handleEditProfileClick = () => {
    // navigate('#');
    navigate('/main/upload-photo');
  };

  const handleJoinPremiumClick = () => {
    navigate('#');
  };

  const handleSafetyClick = () => {
    navigate('#');
  };

  return (
    <Box as="div" className={styles['container']}>
      <Box as="div" className={styles['board']}>
        <main className={styles['content']}>
          <Box as="div" className={styles['user_details']}>
            <Box as="div" className={styles['user_image']}>
              <AlofaPhoto
                url={mockData.imageUrl}
                alt={`${mockData.displayName}'s profile picture`}
                isRounded={true}
              />
            </Box>
            <Heading as="h2">{`${mockData.displayName}, ${mockData.age}`}</Heading>
          </Box>
          <Box as="div" className={styles['button_container']}>
            <AlofaButton onClick={handleEditProfileClick}>
              Edit Profile
            </AlofaButton>
            <AlofaButton isPrimary={false} onClick={handleJoinPremiumClick}>
              Join Premium
            </AlofaButton>
            <AlofaButton isPrimary={false} onClick={handleSafetyClick}>
              Safety
            </AlofaButton>
          </Box>
        </main>
      </Box>
    </Box>
  );
}

export default UserProfile;
