import React from 'react'
import { AlofaButton, AlofaPhoto, AlofaUploadPhoto } from '@alofa/alofa-ui';
import styles from '../user-profile.module.scss';
import styles2 from './upload-photo.module.scss';
import { useNavigate } from 'react-router-dom';
import { Box, Heading } from '@chakra-ui/react';

const mockData = {
    displayName: 'Raul',
    age: 32,
    imageUrl:
      'https://plus.unsplash.com/premium_photo-1664540415069-bc45ce3e711e?w=300&auto=format&fit=crop&q=60&ixlib=rb-4.0.3&ixid=M3wxMjA3fDB8MHxzZWFyY2h8NXx8cG9ydHJhaXQlMjBtYW58ZW58MHx8MHx8fDA%3D',
  };


const UploadPhoto = () => {
    const navigate = useNavigate();

    const handleSubmit = () => {
        // submit photos
        navigate('/main/user-profile');
    }

  return (
    <Box as="div" className={styles['container']}>
      <Box as="div" className={styles['board']}>
        <main className={styles['content']}>
          <Box as="div" className={styles['user_details']}>
            <Box as="div" className={styles['user_image']}>
              <AlofaPhoto
                url={mockData.imageUrl}
                alt={`${mockData.displayName}'s profile picture`}
                isRounded={true}
              />
            </Box>
            <Heading as="h2">{`${mockData.displayName}, ${mockData.age}`}</Heading>
          </Box>
          <Box as="div" className={styles2['photos'] }>
            <Box as="div" className={styles2['photos_container']}>
            
            <AlofaUploadPhoto 
                url={mockData.imageUrl}
                alt={`${mockData.displayName}'s profile picture`}
                isRounded={false}
            />
            <AlofaUploadPhoto 
                alt={`${mockData.displayName}'s profile picture`}
                isRounded={false}
            />
            <AlofaUploadPhoto 
                alt={`${mockData.displayName}'s profile picture`}
                isRounded={false}
            />
            <AlofaUploadPhoto 
                alt={`${mockData.displayName}'s profile picture`}
                isRounded={false}
            />
            <AlofaUploadPhoto 
                alt={`${mockData.displayName}'s profile picture`}
                isRounded={false}
            />
            <AlofaUploadPhoto 
                alt={`${mockData.displayName}'s profile picture`}
                isRounded={false}
            />
            </Box>
            <Box as='div' className={styles2['description']}>
                Add a video or picture to help better match you with your soul mate.
            </Box>
            <Box as='div' className={styles['button_container']}>
                <AlofaButton isPrimary={true} onClick={handleSubmit}>Submit</AlofaButton>
            </Box>
          </Box>
        </main>
      </Box>
    </Box>
  );
}

export default UploadPhoto;