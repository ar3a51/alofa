import styles from './alofa-mobile-login.module.scss';
import { AlofaButton, AlofaTextbox } from '@alofa/alofa-ui';
import { useAppDispatch } from '../../store.typed';
import { useState } from 'react';
import { clearStatus,  loginStateType, logout, setCredentials, statusStateType, updateStatus } from '@alofa/alofa-shared-lib'
import { useSelector } from 'react-redux';
import { statusSelector } from '../../shared-selectors/status.selector';
import { useLoginMutation } from './loginApiSlice';
import { useNavigate } from 'react-router-dom';
import { LoginDTO } from '@alofa/generated/authentication-end-point-types';
import {authenticate} from './authenticate';
import { CognitoUserSession } from 'amazon-cognito-identity-js';


/* eslint-disable-next-line */
export interface AlofaMobileLoginProps {}

export function AlofaMobileLogin(props: AlofaMobileLoginProps) {
  const dispatch = useAppDispatch();
  const [email, setEmail] = useState('');
  const [password, setPassword] = useState('');
   const status = useSelector(statusSelector);
  const [login, {isLoading, isError}] = useLoginMutation();
  const navigate = useNavigate();
   
  function handleInputChangeEmail(e: any) {
    setEmail(e.target.value)
    dispatch(clearStatus());
  }

  function handleInputChangePassword(e: any) {
    setPassword(e.target.value)
    dispatch(clearStatus());
  }

  async function handleSubmit (e:any) {
    e.preventDefault();

    const loginCred: LoginDTO = {emailAddress: email, password}

    try {
     
      const result: CognitoUserSession = (await authenticate(loginCred.emailAddress, loginCred.password)) as CognitoUserSession;
      dispatch(setCredentials({
        accessToken: result.getAccessToken().getJwtToken(),
        refreshToken: result.getRefreshToken().getToken()
      }));
      navigate('/main');

    } catch (err: any) {     
        dispatch(updateStatus({message: err.message, isError}));
    } finally {
      clearFields();
    }
  }

  function handleCancel(e:any): void{
    e.preventDefault();

    clearFields();
    dispatch(logout());
  }

  function clearFields():void {
    setEmail('');
    setPassword('');
  }


  
  return (
    <div className={styles['container']}>
      <img src='/assets/icons/other/alofa-logo.svg' alt='alofa logo'/>
      <form className={styles['login_form']} onSubmit={handleSubmit}>
        <fieldset className={styles['fieldset']}>
            <label htmlFor='txt_email'>Email</label>
            <AlofaTextbox type='email' id='txt_email' name='email'
              onChange={handleInputChangeEmail}
              value={email}
            ></AlofaTextbox>
        </fieldset>
        <fieldset className={styles['fieldset']}>
            <label htmlFor='txt_password'>Password</label>
            <AlofaTextbox type='password' id='txt_password' name='password'
              onChange={handleInputChangePassword}
              value={password}
            ></AlofaTextbox>
        </fieldset>
        <span className={styles['btn-submit']}>
          <AlofaButton>submit</AlofaButton>
        </span>
        <span className={styles['btn-cancel']}>
          <AlofaButton onClick={handleCancel} isPrimary={false}>Clear</AlofaButton>
        </span>
      </form>
      <span>{status.message}</span>
    </div>
  );
}

export default AlofaMobileLogin;
