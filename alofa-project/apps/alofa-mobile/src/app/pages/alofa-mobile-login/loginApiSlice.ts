import apiSlice from "../../query";

export const loginApiSlice = apiSlice.injectEndpoints({
    endpoints: builder => ({
        login: builder.mutation({
            query: credentials => ({
                url: '/api/authentication/login',
                method: 'POST',
                body: { ...credentials }
            })
        })
    })
})

 export const { useLoginMutation  } = loginApiSlice;