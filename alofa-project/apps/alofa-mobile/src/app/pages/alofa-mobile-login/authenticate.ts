import {AuthenticationDetails, CognitoUser} from 'amazon-cognito-identity-js';
import userPool from '../../user-pool';

export function authenticate(username: string, password: string) {
    return new Promise((resolve, reject)=> {
        const user = new CognitoUser({
            Username: username,
            Pool: userPool
        });

        const authDetails = new AuthenticationDetails({
            Username: username,
            Password: password
        });

        user.authenticateUser(authDetails, {
            onSuccess: (result) => {
                resolve(result);
            },
            onFailure: (err) => {
                console.log('err', err);
                reject(err);
            }
        })
    })
    //console.log(result);
}

export function logout() {
    const user = userPool.getCurrentUser();
    user?.signOut();
}