import { environment } from "@alofa/alofa-mobile-env";
import { CognitoUserPool } from 'amazon-cognito-identity-js';

const poolData: {UserPoolId: string, ClientId: string} = {
    UserPoolId: environment.authentication.userPoolId,
    ClientId: environment.authentication.clientId,
}

export default new CognitoUserPool(poolData);
