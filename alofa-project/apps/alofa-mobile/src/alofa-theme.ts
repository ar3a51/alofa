import { extendTheme } from "@chakra-ui/react"

const alofaTheme = extendTheme({
    fonts:{
      body: 'Open Sans',
      heading: 'Open Sans'
    },
    colors: {
      alofa: {
        primaryRed: '#CB3949',
        secondaryRed: '#E94057',
        grey: '#E5E5E5',
        lightGrey: '#aeaeae',
        darkGrey: '#7a7a7a'
      }
    }
});

export default alofaTheme;