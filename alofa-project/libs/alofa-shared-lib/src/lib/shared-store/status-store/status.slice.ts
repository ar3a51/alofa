import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { statusState, statusStateType } from "./status.state";

export const statusSlice = createSlice({
    name: 'status',
    initialState: statusState,
    reducers: {
        updateStatus(state, action: PayloadAction<statusStateType>){
           state = {
            isError: action.payload.isError,
            message: action.payload.message
           }
           return state;
        },
        clearStatus(state){
            state = {
                isError: false,
                message: ''
               }
               return state;
        }
    }
})

export const {updateStatus, clearStatus} = statusSlice.actions;