
export type loginStateType = {
    accessToken: string,
    refreshToken: string,
}


export const loginState: loginStateType = {
    accessToken: '',
    refreshToken: '',
}
