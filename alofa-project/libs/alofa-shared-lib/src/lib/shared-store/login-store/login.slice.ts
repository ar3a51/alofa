/* eslint-disable @typescript-eslint/no-unused-expressions */

import {createSlice, PayloadAction} from '@reduxjs/toolkit';
import { loginState, loginStateType } from './login.state';


export const authSlice = createSlice({
    name: 'auth',
    initialState: loginState,
    reducers:{
        logout(state){
           state = loginState

           return state;
        },
        setCredentials(state, action: PayloadAction<loginStateType>) {
            state = {
                ...action.payload
            };

            return state;
        }


    }
})

export const {logout, setCredentials} = authSlice.actions





