import { render } from '@testing-library/react';

import AlofaMobileToolbar from './alofa-mobile-toolbar';

describe('AlofaMobileToolbar', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AlofaMobileToolbar />);
    expect(baseElement).toBeTruthy();
  });
});
