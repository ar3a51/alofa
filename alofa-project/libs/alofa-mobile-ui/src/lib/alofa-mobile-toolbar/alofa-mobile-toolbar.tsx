import styles from './alofa-mobile-toolbar.module.scss';

/* eslint-disable-next-line */
export interface AlofaMobileToolbarProps {}

export function AlofaMobileToolbar(props: AlofaMobileToolbarProps) {
  return (
    <div className={styles['container']}>
      <h1>Welcome to AlofaMobileToolbar!</h1>
    </div>
  );
}

export default AlofaMobileToolbar;
