import styles from './alofa-mobile-ui.module.scss';

/* eslint-disable-next-line */
export interface AlofaMobileUiProps {}

export function AlofaMobileUi(props: AlofaMobileUiProps) {
  return (
    <div className={styles['container']}>
      <h1>Welcome to AlofaMobileUi!</h1>
    </div>
  );
}

export default AlofaMobileUi;
