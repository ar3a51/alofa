import { Box } from '@chakra-ui/react';
import styles from './alofa-mobile-header.module.scss';
import { ChevronLeftIcon } from '@chakra-ui/icons';

/* eslint-disable-next-line */
export interface AlofaMobileHeaderProps {
  onClick: any;
  displayBackButton?: boolean
}

export function AlofaMobileHeader(props: AlofaMobileHeaderProps) {
  const displayBackButton = props.displayBackButton === null ? true : props.displayBackButton;

  //console.log(`displayBAckButton: ${displayBackButton}`);
  return (
    <div className={styles['header']}>
       { displayBackButton && <ChevronLeftIcon onClick={props.onClick} boxSize={10} className={styles['header__back-button']}/> }
       <img className={styles['header__logo']} src='/assets/icons/other/alofa-logo.svg' alt='alofa logo'/>
    </div>
  );
}

export default AlofaMobileHeader;
