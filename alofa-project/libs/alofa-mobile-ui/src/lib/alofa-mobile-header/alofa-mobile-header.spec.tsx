import { render } from '@testing-library/react';

import AlofaMobileHeader from './alofa-mobile-header';

describe('AlofaMobileHeader', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AlofaMobileHeader />);
    expect(baseElement).toBeTruthy();
  });
});
