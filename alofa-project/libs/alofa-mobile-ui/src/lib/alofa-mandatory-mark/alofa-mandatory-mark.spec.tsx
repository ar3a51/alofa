import { render } from '@testing-library/react';

import AlofaMandatoryMark from './alofa-mandatory-mark';

describe('AlofaMandatoryMark', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AlofaMandatoryMark />);
    expect(baseElement).toBeTruthy();
  });
});
