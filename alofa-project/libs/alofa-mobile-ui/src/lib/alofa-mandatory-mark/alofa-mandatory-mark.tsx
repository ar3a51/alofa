import styles from './alofa-mandatory-mark.module.scss';

/* eslint-disable-next-line */
export interface AlofaMandatoryMarkProps {}

export function AlofaMandatoryMark(props: AlofaMandatoryMarkProps) {
  return (
    <span className={styles['mandatory-block']}>*Mandatory</span>
  );
}

export default AlofaMandatoryMark;
