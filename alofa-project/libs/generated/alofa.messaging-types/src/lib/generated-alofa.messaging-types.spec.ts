import { generatedAlofaMessagingTypes } from './generated-alofa.messaging-types';

describe('generatedAlofaMessagingTypes', () => {
  it('should work', () => {
    expect(generatedAlofaMessagingTypes()).toEqual(
      'generated-alofa.messaging-types'
    );
  });
});
