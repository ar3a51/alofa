# generated-alofa.messaging-types

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build generated-alofa.messaging-types` to build the library.

## Running unit tests

Run `nx test generated-alofa.messaging-types` to execute the unit tests via [Jest](https://jestjs.io).
