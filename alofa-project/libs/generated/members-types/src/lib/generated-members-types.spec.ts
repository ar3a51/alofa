import { generatedMembersTypes } from './generated-members-types';

describe('generatedMembersTypes', () => {
  it('should work', () => {
    expect(generatedMembersTypes()).toEqual('generated-members-types');
  });
});
