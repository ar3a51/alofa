import { Field } from './field';

export interface GroupField {
  groupName?: string;
  fields?: Field[];
  showInProfile: boolean;
}
