export interface Field {
  fieldName?: string;
  values?: string[];
}
