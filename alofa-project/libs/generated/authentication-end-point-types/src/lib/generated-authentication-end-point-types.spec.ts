import { generatedAuthenticationEndPointTypes } from './generated-authentication-end-point-types';

describe('generatedAuthenticationEndPointTypes', () => {
  it('should work', () => {
    expect(generatedAuthenticationEndPointTypes()).toEqual(
      'generated-authentication-end-point-types'
    );
  });
});
