# generated-authentication-end-point-types

This library was generated with [Nx](https://nx.dev).

## Building

Run `nx build generated-authentication-end-point-types` to build the library.

## Running unit tests

Run `nx test generated-authentication-end-point-types` to execute the unit tests via [Jest](https://jestjs.io).
