import { generatedAlofaPhotosTypes } from './generated-alofa.photos-types';

describe('generatedAlofaPhotosTypes', () => {
  it('should work', () => {
    expect(generatedAlofaPhotosTypes()).toEqual('generated-alofa.photos-types');
  });
});
