namespace member.contract.Dtos;
public class GroupField
{
    public string GroupName {set;get;}
    public Field[] Fields {set;get;}
    public bool ShowInProfile {set;get;}

}
