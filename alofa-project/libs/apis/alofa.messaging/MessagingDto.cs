namespace Alofa.Apis.AlofaMessaging;
public class MessagingDto
{
    public Guid MessageId {set;get;}
    public string SenderUsername {set;get;}
    public string ReceiverUsername {set;get;}
    public Guid? ParentMessageId { set; get; }
    public string MessageBody {set;get;}

    public DateTime MessageSentDate { set; get; }

    
}
