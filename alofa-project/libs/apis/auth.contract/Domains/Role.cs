﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Contract.Domains
{
    public class Role
    {
        public Guid RoleId { private set; get; }
        public string RoleName { private set; get; }

        public Role(Guid roleId, string roleName)
        {
            RoleId = roleId;
            RoleName = roleName;
        }
        public Role()
        {

        }
        public static Role Create(Guid roleId, string roleName)
        {
            return new Role { RoleId = roleId, RoleName = roleName };
        }
    }
}
