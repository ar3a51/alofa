﻿using Alofa.Auth.Contract.DTOs;
using BCrypt.Net;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Contract.Domains
{
    public class User
    {
        public string Password { private set; get; }
        public string EmailAddress { private set; get; }

        public User(string password, string emailAddress)
        {
            Password = password;
            EmailAddress = emailAddress;
        }
        public User()
        { }
        public static User Create ( AuthDTO auth) 
        {
            return new User
            {
                Password = BCrypt.Net.BCrypt.HashPassword(auth.Password),
                EmailAddress = auth.EmailAddress
            };    
        }

        public bool ValidatePassword(string password)
        {
            return BCrypt.Net.BCrypt.Verify(password, Password);
        }

    }
}
