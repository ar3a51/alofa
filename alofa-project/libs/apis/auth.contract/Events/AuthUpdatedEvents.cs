﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Alofa.Auth.Contract.Domains;
using Alofa.Common.BaseEvent;

namespace Alofa.Auth.Contract.Events
{
   public record AuthEmailUpdatedEvent(string Username, string NewEmail, DateTime UpdatedDate): BaseUpdateEvent(UpdatedDate);
   public record AuthFirstNameUpdatedEvent(string Username, string NewFirstName, DateTime UpdatedDate) : BaseUpdateEvent(UpdatedDate);
   public record AuthLastNameUpdatedEvent(string Username, string NewLastName, DateTime UpdatedDate) : BaseUpdateEvent(UpdatedDate);
   public record AuthPasswordUpdatedEvent(string Username, string NewPassword, DateTime UpdatedDate) : BaseUpdateEvent(UpdatedDate);
}
