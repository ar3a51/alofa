﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Contract.DTOs
{
    public class LoginDTO
    {
        public string EmailAddress { set; get; }
        public string Password { set; get; }
    }
}
