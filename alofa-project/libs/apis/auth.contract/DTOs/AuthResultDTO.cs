﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Contract.DTOs
{
    public class AuthResultDTO
    {
        public string Token { set; get; }
        public string Result { set; get; }
        public string ErrMessage { set; get; }
    }
}
