﻿using MediatR;
using Alofa.Common.BaseCommand;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Alofa.Auth.Contract.Commands
{
    public class UpdateUserEmailCommand: BaseUpdateCommand, IRequest
    {
        public string Username { set; get; }
        public string Email { set; get; }
    }

    public class UpdateFirstNameCommand: BaseUpdateCommand, IRequest
    {
        public string Username { set; get; }
        public string FirstName { set; get; }
    }
    public class UpdateLastNameCommand: BaseUpdateCommand, IRequest
    {
        public string Username { set; get; }
        public string LastName { set; get; }
    }

    public class UpdatePasswordCommand: BaseUpdateCommand, IRequest
    {
        public string Username { set; get; }
        public string Password { set; get; }
    }
}
