﻿using Alofa.Auth.Contract.DTOs;
using Alofa.Common.BaseCommand;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using MediatR;


namespace Alofa.Auth.Contract.Commands
{
    public class CreateUserCommand: BaseCreateCommand, IRequest<bool>
    {
        public AuthDTO AuthDto { get; set; }
    }
}
