import { Box } from '@chakra-ui/react';
import styles from './alofa-radio-button.module.scss';
import React from 'react';

/* eslint-disable-next-line */
export interface AlofaRadioButtonProps {
  id: string,
  name: string,
  label: string,
  value?: any,
  checked?: boolean,
  onChange?: (e: any) => void
}

export const AlofaRadioButton = React.forwardRef<HTMLInputElement, AlofaRadioButtonProps>((props: AlofaRadioButtonProps, ref) => {
  return (
    <Box as='div' 
      className={styles['radio-button']} 
      display={'flex'} alignItems={'center'}
      justifyContent={'center'}
      >
      <input ref={ref} type={'radio'} {...props} className={styles['radio-button__input']}/>
      <label
        className={styles['radio-button__field-label']} 
        htmlFor={props.id}>
        {props.label}
      </label>
    </Box>
  );
})

export default AlofaRadioButton;
