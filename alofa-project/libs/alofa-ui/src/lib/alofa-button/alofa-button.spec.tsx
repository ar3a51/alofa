import { render } from '@testing-library/react';

import AlofaButton from './alofa-button';

describe('AlofaButton', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AlofaButton />);
    expect(baseElement).toBeTruthy();
  });
});
