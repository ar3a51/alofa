import { render } from '@testing-library/react';

import AlofaPillButton from './alofa-pill-button';

describe('AlofaPillButton', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AlofaPillButton />);
    expect(baseElement).toBeTruthy();
  });
});
