import { ChangeEvent, ChangeEventHandler, useEffect, useState } from 'react';
import styles from './alofa-pill-button.module.scss';
import React from 'react';

/* eslint-disable-next-line */
export interface AlofaPillButtonProps {
  id: string,
  name: string,
  /* eslint-disable-next-line */
  value?: any,
  label?: string,
  onChange?: ChangeEventHandler<HTMLInputElement>,
  checked?: boolean
}

export const AlofaPillButton = React.forwardRef<HTMLInputElement, AlofaPillButtonProps>((props: AlofaPillButtonProps, ref) => {


  return (
    <div className={styles['pill-button']}>
      <input type="checkbox"
        ref={ref}
        className={styles['pill-button__input']} 
        {...props}
        onChange={props.onChange}
      />
       <label 
        className={styles['pill-button__label']} 
        htmlFor={props.id}>{props.label}</label>
    </div>
  );
})

export default AlofaPillButton;
