import { Box, Input } from '@chakra-ui/react';
import styles from './alofa-search-field.module.scss';
import { SearchIcon } from '@chakra-ui/icons';
import { type } from 'os';
import React from 'react';

/* eslint-disable-next-line */
export interface AlofaSearchFieldProps {
  id: string;
  name: string;
}

export const AlofaSearchField = React.forwardRef<HTMLInputElement, AlofaSearchFieldProps>((props: AlofaSearchFieldProps, ref) => {
  return (
   <Box as="div" className={styles['search-wrapper']}>
      <input ref={ref} className={styles['search-field']} type={'search'} {...props}/>
      <SearchIcon className={styles['search-icon']} fontSize={'1.6em'}/>
  </Box>
  );
})

export default AlofaSearchField;
