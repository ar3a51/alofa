import { useState, useRef } from 'react';
import { Box } from '@chakra-ui/react';

export interface AlofaUploadPhotoProps {
    url?: string;
    isRounded: boolean;
    alt: string;
}

export function AlofaUploadPhoto({ url: initialUrl, alt, isRounded }: AlofaUploadPhotoProps) {
    const [file, setFile] = useState<File | null>(null);
    const [previewUrl, setPreviewUrl] = useState<string | null>(initialUrl || null);
    const fileInputRef = useRef<HTMLInputElement>(null);

    const handleFileChange = (e: React.ChangeEvent<HTMLInputElement>) => {
        const selectedFile = e.target.files ? e.target.files[0] : null;
        if (selectedFile) {
            setFile(selectedFile);
            setPreviewUrl(URL.createObjectURL(selectedFile));
        }
    };

    const handleRemoveImage = () => {
        setFile(null);
        setPreviewUrl(null);
    };

    const handleButtonClick = () => {
        if (previewUrl) {
            handleRemoveImage();
        } else {
            fileInputRef.current?.click();
        }
    };

    return (
        <Box position="relative" boxSize="100%">
            {previewUrl ? (
                <Box
                    as="img"
                    src={previewUrl}
                    alt={alt || 'Image'}
                    boxSize="100%"
                    objectFit="cover"
                    borderRadius={isRounded ? '50%' : '3%'}
                />
            ) : (
                <Box
                    boxSize="100%"
                    bg="lightgray"
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    borderRadius={isRounded ? '50%' : '3%'}
                    color="white"
                >
                </Box>
            )}
            {isRounded ? null : (
                <Box
                    position="absolute"
                    bottom="0px"
                    right="0px"
                    width="30px"
                    height="30px"
                    borderRadius="100%"
                    bg={previewUrl ? 'white' : 'crimson'}
                    display="flex"
                    justifyContent="center"
                    alignItems="center"
                    color={previewUrl ? 'crimson' : 'white'}
                    fontSize="30px"
                    cursor="pointer"
                    onClick={handleButtonClick}
                >
                    {previewUrl ? '×' : '+'}
                </Box>
            )}
            <input
                type="file"
                accept="image/*"
                ref={fileInputRef}
                style={{ display: 'none' }}
                onChange={handleFileChange}
            />
        </Box>
    );
}
