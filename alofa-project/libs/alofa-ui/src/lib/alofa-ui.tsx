import styles from './alofa-ui.module.scss';

/* eslint-disable-next-line */
export interface AlofaUiProps {}

export function AlofaUi(props: AlofaUiProps) {
  return (
    <div className={styles['container']}>
      <h1>Welcome to AlofaUi!</h1>
    </div>
  );
}

export default AlofaUi;
