import { render } from '@testing-library/react';

import AlofaSlider from './alofa-slider';

describe('AlofaSlider', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AlofaSlider />);
    expect(baseElement).toBeTruthy();
  });
});
