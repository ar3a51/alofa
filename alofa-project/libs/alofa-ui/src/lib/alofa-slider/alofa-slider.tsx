import { useState } from 'react';

import styles from './alofa-slider.module.scss';
import {
  RangeSlider,
  RangeSliderTrack,
  RangeSliderFilledTrack,
  RangeSliderThumb,
} from '@chakra-ui/react';

/* eslint-disable-next-line */
export interface AlofaSliderProps {
  min?: number;
  max?: number;
  initial?: number[];
  onChange?: (values: number[]) => void;
}

export function AlofaSlider({
  min = 0,
  max = 100,
  initial = [0, 100],
  onChange,
}: AlofaSliderProps) {
  const [value, setValue] = useState<number[]>(initial);

  const handleChange = (newValues: number[]) => {
    setValue(newValues);
    if (onChange) {
      onChange(newValues);
    }
  };

  return (
    <>
      <RangeSlider
        w="100%"
        aria-label={['min', 'max']}
        min={min}
        max={max}
        defaultValue={initial}
        onChange={handleChange}
      >
        <RangeSliderTrack bg="gray.200" height="5px">
          <RangeSliderFilledTrack bg="red.500" />
        </RangeSliderTrack>
        <RangeSliderThumb
          className={styles['sliderThumb']}
          boxSize={6}
          index={0}
          bg="red.500"
          border="2px solid white"
        />
        <RangeSliderThumb
          className={styles['sliderThumb']}
          boxSize={6}
          index={1}
          bg="red.500"
          border="2px solid white"
        />
      </RangeSlider>
    </>
  );
}

export default AlofaSlider;
