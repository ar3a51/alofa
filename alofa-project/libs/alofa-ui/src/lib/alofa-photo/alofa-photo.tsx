import styles from './alofa-photo.module.scss';
import { Box } from '@chakra-ui/react';

/* eslint-disable-next-line */
export interface AlofaPhotoProps {
  url: string;
  isRounded: boolean;
  alt: string;
}

export function AlofaPhoto({ url, isRounded, alt }: AlofaPhotoProps) {
  return (
    <Box
      as="img"
      src={url}
      alt={alt}
      boxSize={'100%'}
      objectFit="cover"
      borderRadius={isRounded ? '50%' : '3%'}
    />
  );
}

export default AlofaPhoto;
