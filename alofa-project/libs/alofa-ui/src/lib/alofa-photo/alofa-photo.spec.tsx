import { render } from '@testing-library/react';

import AlofaPhoto from './alofa-photo';

describe('AlofaPhoto', () => {
  it('should render successfully', () => {
    const { baseElement } = render(<AlofaPhoto />);
    expect(baseElement).toBeTruthy();
  });
});
