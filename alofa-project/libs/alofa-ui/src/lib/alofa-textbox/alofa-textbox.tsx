import React, { ChangeEventHandler, useEffect } from 'react';
import './alofa-textbox.scss';
import classNames from 'classnames';

/* eslint-disable-next-line */
export interface AlofaTextboxProps {
  
  name: string,
  id: string,
  type: string,
  value?: string,
  onChange?: ChangeEventHandler<HTMLInputElement>,
  isvalidstate?: boolean,
  isreadonly?: number

}

export const AlofaTextbox = React.forwardRef<HTMLInputElement, AlofaTextboxProps>((props: any, ref)=>{
  
  const valid = props.isvalidstate == null ? true : props.isvalidstate;
  const isReadOnly = +props.isreadonly || false;

  /*Remove non-standard HTML attribute from props*/
  const nativeprops = (({isvalidstate, ...object})=> object)(props)
    
  return (
    <input ref={ref} className={classNames('txt', {'txt--invalid': !valid})}
    readOnly={isReadOnly}
    {...nativeprops}
   ></input>
  );
 
})

export default AlofaTextbox;
