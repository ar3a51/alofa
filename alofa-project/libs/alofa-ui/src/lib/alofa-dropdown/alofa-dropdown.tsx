import { Box } from '@chakra-ui/react';
import { ChangeEventHandler } from 'react';
import styles from './alofa-dropdown.module.scss';
import React from 'react';

/* eslint-disable-next-line */
export interface AlofaDropdownProps {
  id: string,
  name: string,
  listInfo: {
    data: Array<any>,
    idField: string,
    labelField: string,
    valueField: string
  },
  onChange?: ChangeEventHandler<HTMLSelectElement>,
  value?: any
  

}

export const AlofaDropdown = React.forwardRef<HTMLSelectElement, AlofaDropdownProps>((props: AlofaDropdownProps, ref) => {
  const nativeprops = (({listInfo, ...object})=>object)(props);
  return (
    <Box as='div' className={styles['dropdown-wrapper']}>
      <select ref={ref} className={styles['dropdown']} 
        onChange={props.onChange}
        value={props.value}
        {...nativeprops}
        >{
          props.listInfo && props.listInfo.data.map(val => <option
            key={val[props.listInfo.idField]} 
            value={val[props.listInfo.valueField]}
            disabled={val[props.listInfo.valueField] === ''}>
              { val[props.listInfo.labelField] }
            </option>)
        }</select>
    </Box>
  );
})

export default AlofaDropdown;
